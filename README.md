# Projects #

These are the more interesting projects i've done so far in C#.

## Library System ##

This project is the required project for finishing the SWIFT Academy C# course.
A simple WebForms application that uses simple web services to write in/read from a database. (database creation file is included)
The topic of the application is about managing a library. The application can add/edit/delete authors, books and users.
The application can also give out books to the users and display relevant information about which books are unavailable. The books can also be returned.
The application is translated in both bulgarian and english languages. It has statistics for how many books have been given out to users (for each year, sorted by month).

## ElectricityNorifications ##

This is a Windows Forms application that checks for specified keywords in the maintenance page of the official ENERGO-PRO website and indicates (with a small red/green
lightning) whether or not there will be maintenances. The application automatically check every 10 minutes (or more, but not more frequently than 10 minutes).
There are still some features i would like to add, but too much work for little benefit. And possibly some bugs to be fixed too.

## Coding Challenges ##

These are challenges from https://coderbyte.com/ . The challenges are simple - you are given one task and you must complete it.
I will include only the Hard (and possibly the Medium) difficulty challenges.
Each challenge's requirement is in its respective folder under the name TaskRequirements.txt. The text file is in english.

## Homework Projects ##

The projects in this folder are the homeworks we were given at the SWIFT Academy C# course.
None of them are really impressive (compared to the Library System).
All of the requirements for each homework will be located as a HomeworkRequirements.txt file in their respective folders. The text file is in bulgarian.
Notable homework tasks(tasks that are a bit more interesting):  
-Homework04\Arrays08 - filling up a two-dimentional array in a spiral fashion.  
-Homework08\Task00 - multiple inheritances for multiple classes.
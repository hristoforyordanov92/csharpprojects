﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ForkLibraryServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IForkoLibraryService" in both code and config file together.
    [ServiceContract]
    public interface IForkoLibraryService
    {
        //Authors
        [OperationContract]
        Author GetAuthor(int id);
        [OperationContract]
        List<Author> GetAllAuthors();
        [OperationContract]
        void CreateAuthor(Author author);
        [OperationContract]
        void UpdateAuthor(int id, Author author);
        [OperationContract]
        bool DeleteAuthor(Author author);
        [OperationContract]
        bool DeleteAuthorById(int id);

        //Book
        [OperationContract]
        Book GetBook(int id);
        [OperationContract]
        List<Book> GetAllBooks();
        [OperationContract]
        void CreateBook(Book book);
        [OperationContract]
        void UpdateBook(int id, Book book);
        [OperationContract]
        bool DeleteBook(Book book);
        [OperationContract]
        bool DeleteBookById(int id);

        //BooksPerMonth
        [OperationContract]
        List<BooksPerMonth> GetAllBooksPerMonth();
        [OperationContract]
        void CreateBooksPerMonth(BooksPerMonth booksPerMonth);

        //Checkout
        [OperationContract]
        Checkout GetCheckout(int id);
        [OperationContract]
        List<Checkout> GetAllCheckouts();
        [OperationContract]
        void CreateCheckout(Checkout checkout);
        [OperationContract]
        void UpdateCheckout(int id, Checkout checkout);
        [OperationContract]
        bool DeleteCheckout(Checkout checkout);
        [OperationContract]
        bool DeleteCheckoutById(int id);

        //City
        [OperationContract]
        City GetCity(int id);
        [OperationContract]
        List<City> GetAllCities();
        [OperationContract]
        void CreateCity(City city);
        [OperationContract]
        void UpdateCity(int id, City city);
        [OperationContract]
        bool DeleteCity(City city);
        [OperationContract]
        bool DeleteCityById(int id);

        //User
        [OperationContract]
        User GetUser(int id);
        [OperationContract]
        List<User> GetAllUsers();
        [OperationContract]
        void CreateUser(User user);
        [OperationContract]
        void UpdateUser(int id, User user);
        [OperationContract]
        bool DeleteUser(User user);
        [OperationContract]
        bool DeleteUserById(int id);
    }

    [DataContract]
    public class Author
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public byte Sex { get; set; }
        [DataMember]
        public DateTime BirthDate { get; set; }
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string UniqueNumber { get; set; }
        [DataMember]
        public int PageCount { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public int AuthorID { get; set; }
    }

    [DataContract]
    public class BooksPerMonth
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
    }

    [DataContract]
    public class Checkout
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public int BookID { get; set; }
        [DataMember]
        public int UserID { get; set; }
    }

    [DataContract]
    public class City
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string IDCard { get; set; }
        [DataMember]
        public string EGN { get; set; }
        [DataMember]
        public byte Sex { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string EMail { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int CityID { get; set; }
    }
}

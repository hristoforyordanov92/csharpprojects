﻿using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ForkLibraryServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ForkoLibraryService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test dal for testing this service, please select ForkoLibraryService.svc or ForkoLibraryService.svc.cs at the Solution Explorer and start debugging.
    public class ForkoLibraryService : IForkoLibraryService
    {
        //Authors
        public Author GetAuthor(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            Author author = Mapper.ToDataContract(dal.GetAuthor(id));
            return author;
        }
        public List<Author> GetAllAuthors()
        {
            DataAccessLayer dal = new DataAccessLayer();
            List<Author> allAuthors = new List<Author>();
            List<BusinessLibrary.Author> businessAuthors = dal.GetAllAuthors();
            foreach (var author in businessAuthors)
            {
                allAuthors.Add(Mapper.ToDataContract(author));
            }
            return allAuthors;
        }
        public void CreateAuthor(Author author)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.CreateAuthor(Mapper.ToBusiness(author));
        }
        public void UpdateAuthor(int id, Author author)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.UpdateAuthor(id, Mapper.ToBusiness(author));
        }
        public bool DeleteAuthor(Author author)
        {
            return DeleteAuthorById(author.ID);
        }
        public bool DeleteAuthorById(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            try
            {
                dal.DeleteAuthor(id);
                return true;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return false;
            }
        }

        //Books
        public Book GetBook(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            Book book = Mapper.ToDataContract(dal.GetBook(id));
            return book;
        }
        public List<Book> GetAllBooks()
        {
            DataAccessLayer dal = new DataAccessLayer();
            List<Book> allBooks = new List<Book>();
            List<BusinessLibrary.Book> businessBooks = dal.GetAllBooks();
            foreach (var book in businessBooks)
            {
                allBooks.Add(Mapper.ToDataContract(book));
            }
            return allBooks;
        }
        public void CreateBook(Book book)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.CreateBook(Mapper.ToBusiness(book));
        }
        public void UpdateBook(int id, Book book)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.UpdateBook(id, Mapper.ToBusiness(book));
        }
        public bool DeleteBook(Book book)
        {
            return DeleteBookById(book.ID);
        }
        public bool DeleteBookById(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            try
            {
                dal.DeleteBook(id);
                return true;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return false;
            }
        }

        //BooksPerMonth
        public List<BooksPerMonth> GetAllBooksPerMonth()
        {
            DataAccessLayer dal = new DataAccessLayer();
            List<BooksPerMonth> allBooksPerMonth = new List<BooksPerMonth>();
            List<BusinessLibrary.BooksPerMonth> businessBooksPerMonth = dal.GetAllBooksPerMonth();
            foreach (var bookPerMonth in businessBooksPerMonth)
            {
                allBooksPerMonth.Add(Mapper.ToDataContract(bookPerMonth));
            }
            return allBooksPerMonth;
        }
        public void CreateBooksPerMonth(BooksPerMonth booksPerMonth)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.CreateBookPerMonth(Mapper.ToBusiness(booksPerMonth));
        }

        //Checkout
        public Checkout GetCheckout(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            Checkout checkout = Mapper.ToDataContract(dal.GetCheckout(id));
            return checkout;
        }
        public List<Checkout> GetAllCheckouts()
        {
            DataAccessLayer dal = new DataAccessLayer();
            List<Checkout> allCheckouts = new List<Checkout>();
            List<BusinessLibrary.Checkout> businessCheckouts = dal.GetAllCheckout();
            foreach (var checkout in businessCheckouts)
            {
                allCheckouts.Add(Mapper.ToDataContract(checkout));
            }
            return allCheckouts;
        }
        public void CreateCheckout(Checkout checkout)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.CreateCheckout(Mapper.ToBusiness(checkout));
        }
        public void UpdateCheckout(int id, Checkout checkout)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.UpdateCheckout(id, Mapper.ToBusiness(checkout));
        }
        public bool DeleteCheckout(Checkout checkout)
        {
            return DeleteCheckoutById(checkout.ID);
        }
        public bool DeleteCheckoutById(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            try
            {
                dal.DeleteCheckout(id);
                return true;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return false;
            }
        }

        //City
        public City GetCity(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            City city = Mapper.ToDataContract(dal.GetCity(id));
            return city;
        }
        public List<City> GetAllCities()
        {
            DataAccessLayer dal = new DataAccessLayer();
            List<City> allCities = new List<City>();
            List<BusinessLibrary.City> businessCities = dal.GetAllCities();
            foreach (var city in businessCities)
            {
                allCities.Add(Mapper.ToDataContract(city));
            }
            return allCities;
        }
        public void CreateCity(City city)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.CreateCity(Mapper.ToBusiness(city));
        }
        public void UpdateCity(int id, City city)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.UpdateCity(id, Mapper.ToBusiness(city));
        }
        public bool DeleteCity(City city)
        {
            return DeleteCityById(city.ID);
        }
        public bool DeleteCityById(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            try
            {
                dal.DeleteCity(id);
                return true;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return false;
            }
        }

        //User
        public User GetUser(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            User user = Mapper.ToDataContract(dal.GetUser(id));
            return user;
        }
        public List<User> GetAllUsers()
        {
            DataAccessLayer dal = new DataAccessLayer();
            List<User> allUsers = new List<User>();
            List<BusinessLibrary.User> businessUsers = dal.GetAllUsers();
            foreach (var user in businessUsers)
            {
                allUsers.Add(Mapper.ToDataContract(user));
            }
            return allUsers;
        }
        public void CreateUser(User user)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.CreateUser(Mapper.ToBusiness(user));
        }
        public void UpdateUser(int id, User user)
        {
            DataAccessLayer dal = new DataAccessLayer();
            dal.UpdateUser(id, Mapper.ToBusiness(user));
        }
        public bool DeleteUser(User user)
        {
            return DeleteUserById(user.ID);
        }
        public bool DeleteUserById(int id)
        {
            DataAccessLayer dal = new DataAccessLayer();
            try
            {
                dal.DeleteUser(id);
                return true;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForkLibraryServices
{
    public class Mapper
    {
        public static Author ToDataContract(BusinessLibrary.Author businessAuthor)
        {
            Author dataContractAuthor = new Author
            {
                ID = businessAuthor.ID,
                FirstName = businessAuthor.FirstName,
                LastName = businessAuthor.LastName,
                Sex = businessAuthor.Sex,
                BirthDate = businessAuthor.BirthDate
            };
            return dataContractAuthor;
        }

        public static Book ToDataContract(BusinessLibrary.Book businessBook)
        {
            Book dataContractBook = new Book
            {
                ID = businessBook.ID,
                Title = businessBook.Title,
                UniqueNumber = businessBook.UniqueNumber,
                PageCount = businessBook.PageCount,
                PublishDate = businessBook.PublishDate,
                AuthorID = businessBook.AuthorID,
            };

            return dataContractBook;
        }

        public static City ToDataContract(BusinessLibrary.City businessCity)
        {
            City dataContractCity = new City
            {
                ID = businessCity.ID,
                Name = businessCity.Name
            };

            return dataContractCity;
        }

        public static User ToDataContract(BusinessLibrary.User businessUser)
        {
            User dataContractUser = new User
            {
                ID = businessUser.ID,
                FirstName = businessUser.FirstName,
                LastName = businessUser.LastName,
                IDCard = businessUser.IDCard,
                EGN = businessUser.EGN,
                Sex = businessUser.Sex,
                Phone = businessUser.Phone,
                EMail = businessUser.EMail,
                Address = businessUser.Address,
                CityID = businessUser.CityID
            };

            return dataContractUser;
        }

        public static Checkout ToDataContract(BusinessLibrary.Checkout businessCheckout)
        {
            Checkout dataContractCheckout = new Checkout
            {
                ID = businessCheckout.ID,
                FromDate = businessCheckout.FromDate,
                ToDate = businessCheckout.ToDate,
                BookID = businessCheckout.BookID,
                UserID = businessCheckout.UserID
            };

            return dataContractCheckout;
        }

        public static BooksPerMonth ToDataContract(BusinessLibrary.BooksPerMonth businessBooksPerMonth)
        {
            BooksPerMonth dataContractBooksPerMonth = new BooksPerMonth
            {
                Date = businessBooksPerMonth.Date
            };

            return dataContractBooksPerMonth;
        }

        
        public static BusinessLibrary.Author ToBusiness(Author dataContractAuthor)
        {
            BusinessLibrary.Author businessAuthor = new BusinessLibrary.Author
            {
                ID = dataContractAuthor.ID,
                FirstName = dataContractAuthor.FirstName,
                LastName = dataContractAuthor.LastName,
                Sex = dataContractAuthor.Sex,
                BirthDate = dataContractAuthor.BirthDate
            };

            return businessAuthor;
        }

        public static BusinessLibrary.Book ToBusiness(Book dataContractBook)
        {
            BusinessLibrary.Book businessBook = new BusinessLibrary.Book
            {
                ID = dataContractBook.ID,
                Title = dataContractBook.Title,
                UniqueNumber = dataContractBook.UniqueNumber,
                PageCount = dataContractBook.PageCount,
                PublishDate = dataContractBook.PublishDate,
                AuthorID = dataContractBook.AuthorID
            };

            return businessBook;
        }

        public static BusinessLibrary.City ToBusiness(City dataContractCity)
        {
            BusinessLibrary.City businessCity = new BusinessLibrary.City
            {
                ID = dataContractCity.ID,
                Name = dataContractCity.Name
            };

            return businessCity;
        }

        public static BusinessLibrary.User ToBusiness(User dataContractUser)
        {
            BusinessLibrary.User businessUser = new BusinessLibrary.User
            {
                ID = dataContractUser.ID,
                FirstName = dataContractUser.FirstName,
                LastName = dataContractUser.LastName,
                IDCard = dataContractUser.IDCard,
                EGN = dataContractUser.EGN,
                Sex = dataContractUser.Sex,
                Phone = dataContractUser.Phone,
                EMail = dataContractUser.EMail,
                Address = dataContractUser.Address,
                CityID = dataContractUser.CityID
            };

            return businessUser;
        }

        public static BusinessLibrary.Checkout ToBusiness(Checkout dataContractCheckout)
        {
            BusinessLibrary.Checkout businessCheckout = new BusinessLibrary.Checkout
            {
                ID = dataContractCheckout.ID,
                FromDate = dataContractCheckout.FromDate,
                ToDate = dataContractCheckout.ToDate,
                BookID = dataContractCheckout.BookID,
                UserID = dataContractCheckout.UserID
            };

            return businessCheckout;
        }

        public static BusinessLibrary.BooksPerMonth ToBusiness(BooksPerMonth dataContractBooksPerMonth)
        {
            BusinessLibrary.BooksPerMonth businessBooksPerMonth = new BusinessLibrary.BooksPerMonth
            {
                Date = dataContractBooksPerMonth.Date
            };

            return businessBooksPerMonth;
        }
    }
}
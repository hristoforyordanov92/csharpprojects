﻿using System.Collections.Generic;
using System.Linq;
using BusinessLibrary;

namespace LibraryDataAccessLayer
{
    public partial class DataAccessLayer
    {
        public List<BusinessLibrary.BooksPerMonth> GetAllBooksPerMonth()
        {
            List<LibraryEntity.BooksPerMonth> allEntityBooksPerMonth = database.BooksPerMonth.ToList();
            List<BooksPerMonth> allBooksPerMonth = new List<BooksPerMonth>();

            foreach (var bookPerMonth in allEntityBooksPerMonth)
            {
                allBooksPerMonth.Add(Mapper.ToBusiness(bookPerMonth));
            }

            return allBooksPerMonth;
        }

        public bool CreateBookPerMonth(BusinessLibrary.BooksPerMonth bookPerMonth)
        {
            database.BooksPerMonth.Add(Mapper.ToEntity(bookPerMonth));
            database.SaveChanges();
            return true;
        }

        //No other implementations are required
    }
}
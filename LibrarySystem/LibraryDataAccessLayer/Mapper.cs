﻿
namespace LibraryDataAccessLayer
{
    static class Mapper
    {
        //To entity objects
        public static LibraryEntity.Author ToEntity(BusinessLibrary.Author businessAuthor)
        {
            LibraryEntity.Author entityAuthor = new LibraryEntity.Author
            {
                ID = businessAuthor.ID,
                FirstName = businessAuthor.FirstName,
                LastName = businessAuthor.LastName,
                Sex = businessAuthor.Sex,
                BirthDate = businessAuthor.BirthDate
            };

            return entityAuthor;
        }

        public static LibraryEntity.Book ToEntity(BusinessLibrary.Book businessBook)
        {
            LibraryEntity.Book entityBook = new LibraryEntity.Book
            {
                ID = businessBook.ID,
                Title = businessBook.Title,
                UniqueNumber = businessBook.UniqueNumber,
                PageCount = businessBook.PageCount,
                PublishDate = businessBook.PublishDate,
                AuthorID = businessBook.AuthorID,
            };

            return entityBook;
        }

        public static LibraryEntity.City ToEntity(BusinessLibrary.City businessCity)
        {
            LibraryEntity.City entityCity = new LibraryEntity.City
            {
                ID = businessCity.ID,
                Name = businessCity.Name
            };

            return entityCity;
        }

        public static LibraryEntity.User ToEntity(BusinessLibrary.User businessUser)
        {
            LibraryEntity.User entityUser = new LibraryEntity.User
            {
                ID = businessUser.ID,
                FirstName = businessUser.FirstName,
                LastName = businessUser.LastName,
                IDCard = businessUser.IDCard,
                EGN = businessUser.EGN,
                Sex = businessUser.Sex,
                Phone = businessUser.Phone,
                EMail = businessUser.EMail,
                Address = businessUser.Address,
                CityID = businessUser.CityID
            };

            return entityUser;
        }

        public static LibraryEntity.Checkout ToEntity(BusinessLibrary.Checkout businessCheckout)
        {
            LibraryEntity.Checkout entityCheckout = new LibraryEntity.Checkout
            {
                ID = businessCheckout.ID,
                FromDate = businessCheckout.FromDate,
                ToDate = businessCheckout.ToDate,
                BookID = businessCheckout.BookID,
                UserID = businessCheckout.UserID
                //CheckedIn = businessCheckout.CheckedIn
            };

            return entityCheckout;
        }

        public static LibraryEntity.BooksPerMonth ToEntity(BusinessLibrary.BooksPerMonth businessBooksPerMonth)
        {
            LibraryEntity.BooksPerMonth entityBooksPerMonth = new LibraryEntity.BooksPerMonth
            {
                Date = businessBooksPerMonth.Date
            };

            return entityBooksPerMonth;
        }

        //To business objects
        public static BusinessLibrary.Author ToBusiness(LibraryEntity.Author entityAuthor)
        {
            BusinessLibrary.Author businessAuthor = new BusinessLibrary.Author
            {
                ID = entityAuthor.ID,
                FirstName = entityAuthor.FirstName,
                LastName = entityAuthor.LastName,
                Sex = entityAuthor.Sex,
                BirthDate = entityAuthor.BirthDate
            };

            return businessAuthor;
        }

        public static BusinessLibrary.Book ToBusiness(LibraryEntity.Book entityBook)
        {
            BusinessLibrary.Book businessBook = new BusinessLibrary.Book
            {
                ID = entityBook.ID,
                Title = entityBook.Title,
                UniqueNumber = entityBook.UniqueNumber,
                PageCount = entityBook.PageCount,
                PublishDate = entityBook.PublishDate,
                AuthorID = entityBook.AuthorID
            };

            return businessBook;
        }

        public static BusinessLibrary.City ToBusiness(LibraryEntity.City entityCity)
        {
            BusinessLibrary.City businessCity = new BusinessLibrary.City
            {
                ID = entityCity.ID,
                Name = entityCity.Name
            };

            return businessCity;
        }

        public static BusinessLibrary.User ToBusiness(LibraryEntity.User entityUser)
        {
            BusinessLibrary.User businessUser = new BusinessLibrary.User
            {
                ID = entityUser.ID,
                FirstName = entityUser.FirstName,
                LastName = entityUser.LastName,
                IDCard = entityUser.IDCard,
                EGN = entityUser.EGN,
                Sex = entityUser.Sex,
                Phone = entityUser.Phone,
                EMail = entityUser.EMail,
                Address = entityUser.Address,
                CityID = entityUser.CityID
            };

            return businessUser;
        }

        public static BusinessLibrary.Checkout ToBusiness(LibraryEntity.Checkout entityCheckout)
        {
            BusinessLibrary.Checkout businessCheckout = new BusinessLibrary.Checkout
            {
                ID = entityCheckout.ID,
                FromDate = entityCheckout.FromDate,
                ToDate = entityCheckout.ToDate,
                BookID = entityCheckout.BookID,
                UserID = entityCheckout.UserID
                //CheckedIn = entityCheckout.CheckedIn
            };

            return businessCheckout;
        }

        public static BusinessLibrary.BooksPerMonth ToBusiness(LibraryEntity.BooksPerMonth entityBooksPerMonth)
        {
            BusinessLibrary.BooksPerMonth businessBooksPerMonth = new BusinessLibrary.BooksPerMonth
            {
                Date = entityBooksPerMonth.Date
            };

            return businessBooksPerMonth;
        }
    }
}
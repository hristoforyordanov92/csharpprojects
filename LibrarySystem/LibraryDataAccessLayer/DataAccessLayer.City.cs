﻿using System.Collections.Generic;
using System.Linq;
using BusinessLibrary;

namespace LibraryDataAccessLayer
{
    public partial class DataAccessLayer
    {
        public City GetCity(int id)
        {
            var entityCity = database.City.FirstOrDefault(x => x.ID == id);
            if (entityCity != null)
            {
                return Mapper.ToBusiness(entityCity);
            }

            return null;
        }

        public List<City> GetAllCities()
        {
            List<LibraryEntity.City> allEntityCities = database.City.ToList();
            List<City> allCities = new List<City>();

            if (allEntityCities != null)
            {
                foreach (var city in allEntityCities)
                {
                    allCities.Add(Mapper.ToBusiness(city));
                }
            }

            return allCities;
        }

        public bool CreateCity(City city)
        {
            database.City.Add(Mapper.ToEntity(city));
            database.SaveChanges();

            return true;
        }

        public bool UpdateCity(int id, City newCity)
        {
            var oldCity = database.City.FirstOrDefault(x => x.ID == id);
            if (oldCity != null)
            {
                oldCity.Name = newCity.Name;
                database.SaveChanges();
                return true;
            }

            return false;
        }

        public bool DeleteCity(City city)
        {
            return DeleteCity(city.ID);
        }
        public bool DeleteCity(int id)
        {
            var cityToDelete = database.City.FirstOrDefault(x => x.ID == id);
            if (cityToDelete != null)
            {
                database.City.Remove(cityToDelete);
                database.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
﻿using BusinessLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryDataAccessLayer
{
    public partial class DataAccessLayer
    {
        public Checkout GetCheckout(int id)
        {
            LibraryEntity.Checkout checkout = new LibraryEntity.Checkout();
            checkout = database.Checkout.FirstOrDefault(x => x.ID == id);

            if (checkout != null)
            {
                return Mapper.ToBusiness(checkout);
            }
            return null;
        }

        public List<Checkout> GetAllCheckout()
        {
            List<LibraryEntity.Checkout> entityCheckouts = new List<LibraryEntity.Checkout>();
            entityCheckouts = database.Checkout.ToList();

            if (entityCheckouts != null)
            {
                List<Checkout> businessCheckouts = new List<Checkout>();
                foreach (var checkout in entityCheckouts)
                {
                    businessCheckouts.Add(Mapper.ToBusiness(checkout));
                }
                return businessCheckouts;
            }
            return null;
        }

        public bool CreateCheckout(Checkout checkout)
        {
            DataAccessLayer dal = new DataAccessLayer();
            database.Checkout.Add(Mapper.ToEntity(checkout));
            dal.CreateBookPerMonth(new BooksPerMonth { Date = new DateTime(checkout.FromDate.Year, checkout.FromDate.Month, checkout.FromDate.Day) });
            database.SaveChanges();
            return true;
        }

        public bool UpdateCheckout(int id, Checkout checkout)
        {
            var oldCheckout = database.Checkout.FirstOrDefault(x => x.ID == id);
            if (oldCheckout != null)
            {
                oldCheckout.FromDate = checkout.FromDate;
                oldCheckout.ToDate = checkout.ToDate;
                oldCheckout.BookID = checkout.BookID;
                oldCheckout.UserID = checkout.UserID;
                database.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteCheckout(int id)
        {
            var entityCheckout = database.Checkout.FirstOrDefault(x => x.ID == id);
            if (entityCheckout != null)
            {
                database.Checkout.Remove(entityCheckout);
                database.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
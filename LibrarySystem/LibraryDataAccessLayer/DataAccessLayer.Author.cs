﻿using System.Collections.Generic;
using System.Linq;
using BusinessLibrary;

namespace LibraryDataAccessLayer
{
    public partial class DataAccessLayer
    {
        public Author GetAuthor(int id)
        {
            var author = database.Author.FirstOrDefault(x => x.ID == id);
            if (author != null)
            {
                var newAuthor = Mapper.ToBusiness(author);
                return newAuthor;
            }
            return null;
        }

        public List<Author> GetAllAuthors()
        {
            List<LibraryEntity.Author> allEntityAuthors = database.Author.ToList();
            List<Author> allAuthors = new List<Author>();
            if (allEntityAuthors != null)
            {
                foreach (var author in allEntityAuthors)
                {
                    allAuthors.Add(Mapper.ToBusiness(author));
                }
                return allAuthors;
            }
            return null;
        }

        public void CreateAuthor(Author author)
        {
            database.Author.Add(Mapper.ToEntity(author));
            database.SaveChanges();
        }

        public bool UpdateAuthor(int id, Author newAuthor)
        {
            using (database)
            {
                var oldAuthor = database.Author.FirstOrDefault(x => x.ID == id);
                if (oldAuthor != null)
                {
                    oldAuthor.FirstName = newAuthor.FirstName;
                    oldAuthor.LastName = newAuthor.LastName;
                    oldAuthor.Sex = newAuthor.Sex;
                    oldAuthor.BirthDate = newAuthor.BirthDate;
                    database.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAuthor(Author author)
        {
            return DeleteAuthor(author.ID);
        }
        public bool DeleteAuthor(int id)
        {
            var authorToDelete = database.Author.FirstOrDefault(x => x.ID == id);
            if (authorToDelete != null)
            {
                database.Author.Remove(authorToDelete);
                database.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
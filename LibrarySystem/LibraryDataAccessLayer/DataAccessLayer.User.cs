﻿using System.Collections.Generic;
using System.Linq;
using BusinessLibrary;

namespace LibraryDataAccessLayer
{
    public partial class DataAccessLayer
    {
        public User GetUser(int id)
        {
            var user = database.User.FirstOrDefault(x => x.ID == id);
            if (user != null)
            {
                return Mapper.ToBusiness(user);
            }
            return null;
        }

        public List<User> GetAllUsers()
        {
            var allUsers = database.User.ToList();
            var returnedUsers = new List<User>();
            if (allUsers != null)
            {
                foreach (var user in allUsers)
                {
                    returnedUsers.Add(Mapper.ToBusiness(user));
                }
                return returnedUsers;
            }
            return null;
        }

        public bool CreateUser(User user)
        {
            var newUser = Mapper.ToEntity(user);
            database.User.Add(newUser);
            database.SaveChanges();
            return true;
        }

        public bool UpdateUser(int id, User user)
        {
            var oldUser = database.User.FirstOrDefault(x => x.ID == id);
            if (oldUser != null)
            {
                oldUser.FirstName = user.FirstName;
                oldUser.LastName = user.LastName;
                oldUser.IDCard = user.IDCard;
                oldUser.EGN = user.EGN;
                oldUser.Sex = user.Sex;
                oldUser.Phone = user.Phone;
                oldUser.EMail = user.EMail;
                oldUser.Address = user.Address;
                oldUser.CityID = user.CityID;
                database.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteUser(User user)
        {
            return DeleteUser(user.ID);
        }

        public bool DeleteUser(int id)
        {
            var oldUser = database.User.FirstOrDefault(x => x.ID == id);
            if (oldUser != null)
            {
                database.User.Remove(oldUser);
                database.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
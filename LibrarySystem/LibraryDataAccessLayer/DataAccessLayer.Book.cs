﻿using System.Collections.Generic;
using System.Linq;
using BusinessLibrary;

namespace LibraryDataAccessLayer
{
    public partial class DataAccessLayer
    {
        public Book GetBook(int id)
        {
            var book = database.Book.FirstOrDefault(x => x.ID == id);
            if (book != null)
            {
                return Mapper.ToBusiness(book);
            }
            return null;
        }
        public Book GetBookByUniqueNumber(string uniqueNumber)
        {
            var book = database.Book.FirstOrDefault(x => x.UniqueNumber.Equals(uniqueNumber));
            if (book != null)
            {
                return Mapper.ToBusiness(book);
            }
            return null;
        }

        public List<Book> GetAllBooks()
        {
            List<LibraryEntity.Book> allEntityBooks = database.Book.ToList();
            List<Book> allBooks = new List<Book>();

            foreach (var book in allEntityBooks)
            {
                allBooks.Add(Mapper.ToBusiness(book));
            }

            return allBooks;
        }

        public bool CreateBook(Book book)
        {
            database.Book.Add(Mapper.ToEntity(book));
            database.SaveChanges();
            return true;
        }

        public bool UpdateBook(int id, Book newBook)
        {
            var oldBook = database.Book.FirstOrDefault(x => x.ID == newBook.ID);
            if (oldBook != null)
            {
                oldBook.Title = newBook.Title;
                oldBook.UniqueNumber = newBook.UniqueNumber;
                oldBook.PageCount = newBook.PageCount;
                oldBook.PublishDate = newBook.PublishDate;
                oldBook.AuthorID = newBook.AuthorID;
                database.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteBook(Book book)
        {
            return DeleteBook(book.ID);
        }
        public bool DeleteBook(int id)
        {
            var bookToDelete = database.Book.FirstOrDefault(x => x.ID == id);
            if (bookToDelete != null)
            {
                database.Book.Remove(bookToDelete);
                database.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
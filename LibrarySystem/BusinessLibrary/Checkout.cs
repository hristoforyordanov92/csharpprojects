﻿using System;

namespace BusinessLibrary
{
    public class Checkout
    {
        public int ID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int BookID { get; set; }
        public int UserID { get; set; }
        //public bool CheckedIn { get; set; }
    }
}
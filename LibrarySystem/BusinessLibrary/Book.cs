﻿using System;

namespace BusinessLibrary
{
    public class Book
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string UniqueNumber { get; set; }
        public int PageCount { get; set; }
        public DateTime PublishDate { get; set; }
        public int AuthorID { get; set; }

        public override string ToString()
        {
            return $"{Title}";
        }
    }
}
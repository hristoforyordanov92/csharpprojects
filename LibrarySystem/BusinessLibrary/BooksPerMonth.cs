﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLibrary
{
    public class BooksPerMonth
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
    }
}

CREATE DATABASE [ForkoLibrary];
GO

USE [ForkoLibrary];
GO

CREATE TABLE [Author](
	[ID] int IDENTITY(1,1) NOT NULL,
	[FirstName] nvarchar(50) NOT NULL,
	[LastName] nvarchar(50) NOT NULL,
	[Sex] tinyint NOT NULL,
	[BirthDate] date NOT NULL,
	CONSTRAINT PK_Author PRIMARY KEY (ID)
);
GO

CREATE TABLE [Book](
	[ID] int IDENTITY(1,1) NOT NULL,
	[Title] nvarchar(200) NOT NULL,
	[UniqueNumber] nvarchar(13) NOT NULL,
	[PageCount] int NOT NULL,
	[PublishDate] date NOT NULL,
	[AuthorID] int NOT NULL,
	CONSTRAINT PK_Book PRIMARY KEY (ID),
	CONSTRAINT FK_Book_Author_ID FOREIGN KEY (AuthorID) REFERENCES [Author] (ID)
);
GO

CREATE TABLE [City](
	[ID] int IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	CONSTRAINT PK_City PRIMARY KEY (ID)
);
GO

CREATE TABLE [User](
	[ID] int IDENTITY(1,1) NOT NULL,
	[FirstName] nvarchar(50) NOT NULL,
	[LastName] nvarchar(50) NOT NULL,
	[IDCard] nvarchar(9) NOT NULL,
	[EGN] nvarchar(10) NOT NULL,
	[Sex] tinyint NOT NULL,
	[Phone] nvarchar(30) NOT NULL,
	[EMail] nvarchar(50) NOT NULL,
	[Address] nvarchar(100) NOT NULL,
	[CityID] int NOT NULL,
	CONSTRAINT PK_User PRIMARY KEY (ID),
	CONSTRAINT FK_User_City FOREIGN KEY (CityID) REFERENCES [City] (ID)
);
GO

CREATE TABLE [Checkout](
	[ID] int IDENTITY(1,1) NOT NULL,
	[FromDate] date NOT NULL,
	[ToDate] date NOT NULL,
	[BookID] int NOT NULL,
	[UserID] int NOT NULL,
	CONSTRAINT PK_Checkout PRIMARY KEY (ID),
	CONSTRAINT FK_Checkout_Book FOREIGN KEY (BookID) REFERENCES [Book] (ID),
	CONSTRAINT FK_Checkout_User FOREIGN KEY (UserID) REFERENCES [User] (ID)
);
GO

CREATE TABLE [BooksPerMonth](
	[ID] int IDENTITY(1,1) NOT NULL,
	[Date] date NOT NULL
	CONSTRAINT PK_BooksPerMonth PRIMARY KEY (ID)
);
GO
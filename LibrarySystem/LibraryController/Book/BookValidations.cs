﻿using LibraryController.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.Book
{
    class BookValidations
    {
        private static bool Title(ref string title, CustomField customField)
        {
            Validations.TrimEmptySpaces(ref title);

            if (!Validations.MinSymbols(title, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(title, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            return true;
        }
        private static bool UniqueNumber(ref string uniqueNumber, CustomField customField, List<ForkoLibraryServiceReference.Book> bookCollection, ForkoLibraryServiceReference.Book currentBook = null)
        {
            Validations.FilterEmptySpaces(ref uniqueNumber);

            if (!Validations.CheckIfNumber(uniqueNumber, customField.Name))
            {
                return false;
            }

            if (!Validations.MinSymbols(uniqueNumber, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(uniqueNumber, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            string uniqueNumberForLinq = uniqueNumber;

            if (currentBook != null)
            {
                if (bookCollection.Where(x => x.ID != currentBook.ID).FirstOrDefault(x => x.UniqueNumber.Equals(uniqueNumberForLinq)) != null)
                {
                    MessageBox.Show(MainForm.loc.uniqueNumberExists,
                                    MainForm.loc.warning,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                if (bookCollection.FirstOrDefault(x => x.UniqueNumber.Equals(uniqueNumberForLinq)) != null)
                {
                    MessageBox.Show(MainForm.loc.uniqueNumberExists,
                                    MainForm.loc.warning,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }

            return true;
        }
        private static bool PageCount(ref string pageCount, CustomField customField)
        {
            pageCount = pageCount.TrimStart(' ');
            pageCount = pageCount.TrimEnd(' ');

            if (!Validations.MinSymbols(pageCount, customField.MinSymbols, customField.Name))
            {
                return false;
            }
            if (!Validations.MaxSymbols(pageCount, customField.MaxSymbols, customField.Name))
            {
                return false;
            }
            if (!Validations.CheckIfNumber(pageCount, customField.Name))
            {
                return false;
            }
            if (!Validations.PositiveNumber(int.Parse(pageCount), customField.Name))
            {
                return false;
            }
            if(!Validations.MinNumber(int.Parse(pageCount), customField.Name, customField.MinNumber))
            {
                return false;
            }
            if (!Validations.MaxNumber(int.Parse(pageCount), customField.Name, customField.MaxNumber))
            {
                return false;
            }

            return true;
        }
        private static bool Date(DateTime date)
        {
            if (!Validations.FutureDate(date))
            {
                return false;
            }
            return true;
        }

        public static bool Validate(ref string title, ref string uniqueNumber, ref string pageCount, DateTime date,
            BookFields bookFields, List<ForkoLibraryServiceReference.Book> bookCollection, ForkoLibraryServiceReference.Book currentBook = null)
        {
            if (!Title(ref title, bookFields.title)) { return false; }
            if (!UniqueNumber(ref uniqueNumber, bookFields.uniqueNumber, bookCollection, currentBook)) { return false; }
            if (!PageCount(ref pageCount, bookFields.pageCount)) { return false; }
            if (!Date(date)) { return false; }

            return true;
        }
    }
}

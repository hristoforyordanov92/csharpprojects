﻿using LibraryController.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryController.Book
{
    class BookFields
    {
        public CustomField title;
        public CustomField uniqueNumber;
        public CustomField pageCount;

        public BookFields()
        {
            title = new CustomField(MainForm.loc.bookColumnTitle, 1, 200);
            uniqueNumber = new CustomField(MainForm.loc.bookColumnUniqueNumber, 10, 13);
            pageCount = new CustomField(MainForm.loc.bookColumnPageCount, 1, int.MaxValue.ToString().Length, 1, int.MaxValue);
        }
    }
}

﻿using LibraryController.CustomClasses;
using LibraryController.ForkoLibraryServiceReference;
using LibraryController.HelperClasses;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.Book
{
    public partial class UpdateBook : Form
    {
        private ForkoLibraryServiceClient dal;
        private ForkoLibraryServiceReference.Book currentBook;
        private List<ForkoLibraryServiceReference.Book> bookCollection;
        private BookFields bookFields;
        private CustomComboDate customComboDate;

        private string title = "";
        private string uniqueNumber;
        private string pageCount;
        private DateTime date;

        public UpdateBook(ForkoLibraryServiceReference.Book importedBook)
        {
            InitializeComponent();

            dal = new ForkoLibraryServiceClient();
            currentBook = importedBook;
            bookCollection = dal.GetAllBooks().ToList();
            bookFields = new BookFields();

            SetupAuthorsComboBox();
            customComboDate = new CustomComboDate(YearComboBox, MonthComboBox, DayComboBox, DateTime.Now.Year, 1000);
            //SetupPublishDateComboBoxes();
            //SubscribeYearAndMonthComboBoxes();
            LoadBookInformation();
            Localize();
        }

        private void Localize()
        {
            Text = MainForm.loc.createBookForm;
            toolTip1.SetToolTip(CreateBookButton, MainForm.loc.createBookForm_createButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            BookTitleLabel.Text = MainForm.loc.bookColumnTitle;
            BookAuthorLabel.Text = MainForm.loc.author;
            BookUniqueNumberLabel.Text = MainForm.loc.bookColumnUniqueNumber;
            BookPageCountLabel.Text = MainForm.loc.bookColumnPageCount;
            BookPublishedDateLabel.Text = MainForm.loc.publishDate;
        }
        private void SetupAuthorsComboBox()
        {
            var authors = Mapper.ToClientList(dal.GetAllAuthors().ToList());
            int index = authors.FindIndex(x => x.ID == currentBook.AuthorID);
            AuthorComboBox.DataSource = authors;
            AuthorComboBox.SelectedIndex = index;
        }
        private void SetupPublishDateComboBoxes()
        {
            SetupYearComboBox();
            SetupMonthComboBox();
            SetupDayComboBox(null, EventArgs.Empty);
        }
        private void SetupYearComboBox()
        {
            var maxYear = DateTime.Now.Year;
            var allYears = new List<int>();
            for (int i = maxYear; i >= 1500; i--)
            {
                YearComboBox.Items.Add(i);
            }
            YearComboBox.SelectedItem = YearComboBox.Items[0];
        }
        private void SetupMonthComboBox()
        {
            if (MonthComboBox.Items.Count > 0)
            {
                MonthComboBox.Items.Clear();
            }

            for (int i = 0; i < 12; i++)
            {
                MonthComboBox.Items.Add(MainForm.loc.months[i]);
            }
            MonthComboBox.SelectedIndex = 0;
        }
        private void SetupDayComboBox(object sender, EventArgs e)
        {
            int oldSelectedIndex = DayComboBox.SelectedIndex;
            int daysInMonth = DateTime.DaysInMonth((int)YearComboBox.SelectedItem, MonthComboBox.SelectedIndex + 1);
            int newSelectedIndex = 0;

            if (DayComboBox.Items.Count > 0)
            {
                if (oldSelectedIndex > daysInMonth - 1)
                {
                    newSelectedIndex = daysInMonth - 1;
                }
                else
                {
                    newSelectedIndex = oldSelectedIndex;
                }
                DayComboBox.Items.Clear();
            }

            for (int i = 1; i <= daysInMonth; i++)
            {
                DayComboBox.Items.Add(i);
            }

            DayComboBox.SelectedItem = DayComboBox.Items[newSelectedIndex];
        }
        private void SubscribeYearAndMonthComboBoxes()
        {
            YearComboBox.SelectedIndexChanged += SetupDayComboBox;
            MonthComboBox.SelectedIndexChanged += SetupDayComboBox;
        }
        private void LoadBookInformation()
        {
            TitleText.Text = currentBook.Title;
            UniqueNumberText.Text = currentBook.UniqueNumber;
            PageCountText.Text = currentBook.PageCount.ToString();
            YearComboBox.SelectedItem = currentBook.PublishDate.Year;
            MonthComboBox.SelectedIndex = currentBook.PublishDate.Month - 1;
            DayComboBox.SelectedItem = currentBook.PublishDate.Day;
        }

        private void UpdateBookButton_Click(object sender, EventArgs e)
        {
            title = TitleText.Text;
            uniqueNumber = UniqueNumberText.Text;
            pageCount = PageCountText.Text;
            date = new DateTime((int)YearComboBox.SelectedItem,
                                MonthComboBox.SelectedIndex + 1,
                                (int)DayComboBox.SelectedItem);

            if (BookValidations.Validate(ref title, ref uniqueNumber, ref pageCount, date, bookFields, bookCollection, currentBook))
            {
                currentBook.Title = title;
                currentBook.AuthorID = ((CustomClasses.Author)AuthorComboBox.SelectedItem).ID;
                currentBook.UniqueNumber = uniqueNumber;
                currentBook.PageCount = int.Parse(pageCount);
                currentBook.PublishDate = new DateTime((int)YearComboBox.SelectedItem,
                                                        MonthComboBox.SelectedIndex + 1,
                                                        (int)DayComboBox.SelectedItem);

                dal.UpdateBook(currentBook.ID, currentBook);
                Close();
            }
        }
    }
}

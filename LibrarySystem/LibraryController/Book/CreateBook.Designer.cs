﻿namespace LibraryController.Book
{
    partial class CreateBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateBook));
            this.TitleText = new System.Windows.Forms.TextBox();
            this.AuthorComboBox = new System.Windows.Forms.ComboBox();
            this.UniqueNumberText = new System.Windows.Forms.TextBox();
            this.PageCountText = new System.Windows.Forms.TextBox();
            this.CreateBookButton = new System.Windows.Forms.Button();
            this.BookTitleLabel = new System.Windows.Forms.Label();
            this.BookAuthorLabel = new System.Windows.Forms.Label();
            this.BookUniqueNumberLabel = new System.Windows.Forms.Label();
            this.BookPageCountLabel = new System.Windows.Forms.Label();
            this.BookPublishedDateLabel = new System.Windows.Forms.Label();
            this.DayComboBox = new System.Windows.Forms.ComboBox();
            this.MonthComboBox = new System.Windows.Forms.ComboBox();
            this.YearComboBox = new System.Windows.Forms.ComboBox();
            this.CancelButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // TitleText
            // 
            this.TitleText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TitleText.Location = new System.Drawing.Point(136, 15);
            this.TitleText.Margin = new System.Windows.Forms.Padding(2);
            this.TitleText.Name = "TitleText";
            this.TitleText.Size = new System.Drawing.Size(306, 23);
            this.TitleText.TabIndex = 0;
            // 
            // AuthorComboBox
            // 
            this.AuthorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AuthorComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AuthorComboBox.FormattingEnabled = true;
            this.AuthorComboBox.Location = new System.Drawing.Point(136, 42);
            this.AuthorComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.AuthorComboBox.Name = "AuthorComboBox";
            this.AuthorComboBox.Size = new System.Drawing.Size(306, 25);
            this.AuthorComboBox.TabIndex = 1;
            // 
            // UniqueNumberText
            // 
            this.UniqueNumberText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UniqueNumberText.Location = new System.Drawing.Point(259, 70);
            this.UniqueNumberText.Margin = new System.Windows.Forms.Padding(2);
            this.UniqueNumberText.Name = "UniqueNumberText";
            this.UniqueNumberText.Size = new System.Drawing.Size(183, 23);
            this.UniqueNumberText.TabIndex = 2;
            // 
            // PageCountText
            // 
            this.PageCountText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PageCountText.Location = new System.Drawing.Point(136, 97);
            this.PageCountText.Margin = new System.Windows.Forms.Padding(2);
            this.PageCountText.Name = "PageCountText";
            this.PageCountText.Size = new System.Drawing.Size(306, 23);
            this.PageCountText.TabIndex = 3;
            // 
            // CreateBookButton
            // 
            this.CreateBookButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateBookButton.Image = global::LibraryController.Properties.Resources.addBook;
            this.CreateBookButton.Location = new System.Drawing.Point(12, 169);
            this.CreateBookButton.Margin = new System.Windows.Forms.Padding(2);
            this.CreateBookButton.Name = "CreateBookButton";
            this.CreateBookButton.Size = new System.Drawing.Size(48, 48);
            this.CreateBookButton.TabIndex = 7;
            this.CreateBookButton.UseVisualStyleBackColor = true;
            this.CreateBookButton.Click += new System.EventHandler(this.CreateBookButton_Click);
            // 
            // BookTitleLabel
            // 
            this.BookTitleLabel.AutoSize = true;
            this.BookTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BookTitleLabel.Location = new System.Drawing.Point(9, 18);
            this.BookTitleLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookTitleLabel.Name = "BookTitleLabel";
            this.BookTitleLabel.Size = new System.Drawing.Size(35, 17);
            this.BookTitleLabel.TabIndex = 13;
            this.BookTitleLabel.Text = "Title";
            // 
            // BookAuthorLabel
            // 
            this.BookAuthorLabel.AutoSize = true;
            this.BookAuthorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BookAuthorLabel.Location = new System.Drawing.Point(9, 45);
            this.BookAuthorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookAuthorLabel.Name = "BookAuthorLabel";
            this.BookAuthorLabel.Size = new System.Drawing.Size(50, 17);
            this.BookAuthorLabel.TabIndex = 12;
            this.BookAuthorLabel.Text = "Author";
            // 
            // BookUniqueNumberLabel
            // 
            this.BookUniqueNumberLabel.AutoSize = true;
            this.BookUniqueNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BookUniqueNumberLabel.Location = new System.Drawing.Point(9, 72);
            this.BookUniqueNumberLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookUniqueNumberLabel.Name = "BookUniqueNumberLabel";
            this.BookUniqueNumberLabel.Size = new System.Drawing.Size(105, 17);
            this.BookUniqueNumberLabel.TabIndex = 11;
            this.BookUniqueNumberLabel.Text = "Unique number";
            // 
            // BookPageCountLabel
            // 
            this.BookPageCountLabel.AutoSize = true;
            this.BookPageCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BookPageCountLabel.Location = new System.Drawing.Point(9, 99);
            this.BookPageCountLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookPageCountLabel.Name = "BookPageCountLabel";
            this.BookPageCountLabel.Size = new System.Drawing.Size(80, 17);
            this.BookPageCountLabel.TabIndex = 10;
            this.BookPageCountLabel.Text = "Page count";
            // 
            // BookPublishedDateLabel
            // 
            this.BookPublishedDateLabel.AutoSize = true;
            this.BookPublishedDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BookPublishedDateLabel.Location = new System.Drawing.Point(9, 128);
            this.BookPublishedDateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.BookPublishedDateLabel.Name = "BookPublishedDateLabel";
            this.BookPublishedDateLabel.Size = new System.Drawing.Size(103, 17);
            this.BookPublishedDateLabel.TabIndex = 9;
            this.BookPublishedDateLabel.Text = "Date published";
            // 
            // DayComboBox
            // 
            this.DayComboBox.DropDownHeight = 200;
            this.DayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DayComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DayComboBox.FormattingEnabled = true;
            this.DayComboBox.IntegralHeight = false;
            this.DayComboBox.Location = new System.Drawing.Point(166, 126);
            this.DayComboBox.Name = "DayComboBox";
            this.DayComboBox.Size = new System.Drawing.Size(52, 24);
            this.DayComboBox.TabIndex = 4;
            // 
            // MonthComboBox
            // 
            this.MonthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MonthComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MonthComboBox.FormattingEnabled = true;
            this.MonthComboBox.Location = new System.Drawing.Point(224, 126);
            this.MonthComboBox.Name = "MonthComboBox";
            this.MonthComboBox.Size = new System.Drawing.Size(140, 24);
            this.MonthComboBox.TabIndex = 5;
            // 
            // YearComboBox
            // 
            this.YearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.YearComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YearComboBox.FormattingEnabled = true;
            this.YearComboBox.Location = new System.Drawing.Point(370, 126);
            this.YearComboBox.Name = "YearComboBox";
            this.YearComboBox.Size = new System.Drawing.Size(71, 24);
            this.YearComboBox.TabIndex = 6;
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CancelButton.Image = global::LibraryController.Properties.Resources.checkin;
            this.CancelButton.Location = new System.Drawing.Point(393, 169);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(48, 48);
            this.CancelButton.TabIndex = 8;
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CreateBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 228);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.DayComboBox);
            this.Controls.Add(this.MonthComboBox);
            this.Controls.Add(this.YearComboBox);
            this.Controls.Add(this.BookPublishedDateLabel);
            this.Controls.Add(this.BookPageCountLabel);
            this.Controls.Add(this.BookUniqueNumberLabel);
            this.Controls.Add(this.BookAuthorLabel);
            this.Controls.Add(this.BookTitleLabel);
            this.Controls.Add(this.CreateBookButton);
            this.Controls.Add(this.PageCountText);
            this.Controls.Add(this.UniqueNumberText);
            this.Controls.Add(this.AuthorComboBox);
            this.Controls.Add(this.TitleText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "CreateBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CreateBook";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TitleText;
        private System.Windows.Forms.ComboBox AuthorComboBox;
        private System.Windows.Forms.TextBox UniqueNumberText;
        private System.Windows.Forms.TextBox PageCountText;
        private System.Windows.Forms.Button CreateBookButton;
        private System.Windows.Forms.Label BookTitleLabel;
        private System.Windows.Forms.Label BookAuthorLabel;
        private System.Windows.Forms.Label BookUniqueNumberLabel;
        private System.Windows.Forms.Label BookPageCountLabel;
        private System.Windows.Forms.Label BookPublishedDateLabel;
        private System.Windows.Forms.ComboBox DayComboBox;
        private System.Windows.Forms.ComboBox MonthComboBox;
        private System.Windows.Forms.ComboBox YearComboBox;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
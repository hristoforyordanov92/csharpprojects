﻿using System;

namespace LibraryController.CustomClasses
{
    public class Author
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte Sex { get; set; }
        public DateTime BirthDate { get; set; }

        public string LocalizedSex
        {
            get
            {
                return MainForm.loc.sex[Sex];
            }
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
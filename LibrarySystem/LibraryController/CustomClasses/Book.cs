﻿using System;

namespace LibraryController.CustomClasses
{
    public class Book
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string UniqueNumber { get; set; }
        public int PageCount { get; set; }
        public DateTime PublishDate { get; set; }
        public int AuthorID { get; set; }

        public byte IsAvailable { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public string LocalizedIsAvailable
        {
            get
            {
                return MainForm.loc.isAvailable[IsAvailable];
            }
        }

        public override string ToString()
        {
            return $"{Title}";
        }
    }
}
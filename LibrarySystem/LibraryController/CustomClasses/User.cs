﻿
namespace LibraryController.CustomClasses
{
    public class User
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IDCard { get; set; }
        public string EGN { get; set; }
        public byte Sex { get; set; }
        public string Phone { get; set; }
        public string EMail { get; set; }
        public string Address { get; set; }
        public int CityID { get; set; }

        public string City { get; set; }
        public string LocalizedSex
        {
            get
            {
                return MainForm.loc.sex[Sex];
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryController.CustomClasses
{
    public class CustomField
    {
        public CustomField(string name, int minSymbols, int maxSymbols)
        {
            Name = name;
            MinSymbols = minSymbols;
            MaxSymbols = maxSymbols;
        }
        public CustomField(string name, int minSymbols, int maxSymbols, long minNumber, long maxNumber) : this(name, minSymbols, maxSymbols)
        {
            MinNumber = minNumber;
            MaxNumber = maxNumber;
        }

        public string Name { get; set; }
        public int MinSymbols { get; set; }
        public int MaxSymbols { get; set; }
        public long MinNumber { get; set; }
        public long MaxNumber { get; set; }
    }
}
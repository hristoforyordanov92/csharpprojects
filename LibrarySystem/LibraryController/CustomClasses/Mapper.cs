﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryController.CustomClasses
{
    public class Mapper
    {
        //To business
        public static BusinessLibrary.Author ToBusiness(CustomClasses.Author clientAuthor)
        {
            BusinessLibrary.Author businessAuthor = new BusinessLibrary.Author
            {
                ID = clientAuthor.ID,
                FirstName = clientAuthor.FirstName,
                LastName = clientAuthor.LastName,
                Sex = clientAuthor.Sex,
                BirthDate = clientAuthor.BirthDate
            };
            return businessAuthor;
        }
        public static BusinessLibrary.User ToBusiness(CustomClasses.User clientUser)
        {
            BusinessLibrary.User businessUser = new BusinessLibrary.User
            {
                ID = clientUser.ID,
                FirstName = clientUser.FirstName,
                LastName = clientUser.LastName,
                Sex = clientUser.Sex,
                Phone = clientUser.Phone,
                EMail = clientUser.EMail,
                EGN = clientUser.EGN,
                IDCard = clientUser.IDCard,
                Address = clientUser.Address,
                CityID = clientUser.CityID
            };
            return businessUser;
        }
        public static BusinessLibrary.Book ToBusiness(CustomClasses.Book clientBook)
        {
            BusinessLibrary.Book businessBook = new BusinessLibrary.Book
            {
                ID = clientBook.ID,
                Title = clientBook.Title,
                PageCount = clientBook.PageCount,
                UniqueNumber = clientBook.UniqueNumber,
                PublishDate = clientBook.PublishDate,
                AuthorID = clientBook.AuthorID
            };
            return businessBook;
        }

        //To client
        public static CustomClasses.Author ToClient(BusinessLibrary.Author businessAuthor)
        {
            CustomClasses.Author clientAuthor = new CustomClasses.Author
            {
                ID = businessAuthor.ID,
                FirstName = businessAuthor.FirstName,
                LastName = businessAuthor.LastName,
                Sex = businessAuthor.Sex,
                BirthDate = businessAuthor.BirthDate
            };
            return clientAuthor;
        }
        public static CustomClasses.User ToClient(BusinessLibrary.User businessUser)
        {
            CustomClasses.User clientUser = new CustomClasses.User
            {
                ID = businessUser.ID,
                FirstName = businessUser.FirstName,
                LastName = businessUser.LastName,
                Sex = businessUser.Sex,
                Phone = businessUser.Phone,
                EMail = businessUser.EMail,
                EGN = businessUser.EGN,
                IDCard = businessUser.IDCard,
                Address = businessUser.Address,
                CityID = businessUser.CityID
            };
            return clientUser;
        }
        public static CustomClasses.Book ToClient(BusinessLibrary.Book businessBook)
        {
            CustomClasses.Book clientBook = new CustomClasses.Book
            {
                ID = businessBook.ID,
                Title = businessBook.Title,
                PageCount = businessBook.PageCount,
                UniqueNumber = businessBook.UniqueNumber,
                PublishDate = businessBook.PublishDate,
                AuthorID = businessBook.AuthorID
            };
            return clientBook;
        }

        //To client from DataContract
        public static CustomClasses.City ToClient(ForkoLibraryServiceReference.City dataContractCity)
        {
            CustomClasses.City city = new CustomClasses.City
            {
                ID = dataContractCity.ID,
                Name = dataContractCity.Name
            };
            return city;
        }
        public static CustomClasses.Book ToClient(ForkoLibraryServiceReference.Book dataContractBook)
        {
            CustomClasses.Book clientBook = new CustomClasses.Book
            {
                ID = dataContractBook.ID,
                Title = dataContractBook.Title,
                PageCount = dataContractBook.PageCount,
                UniqueNumber = dataContractBook.UniqueNumber,
                PublishDate = dataContractBook.PublishDate,
                AuthorID = dataContractBook.AuthorID
            };
            return clientBook;
        }

        public static CustomClasses.Author ToClient(ForkoLibraryServiceReference.Author dataContractAuthor)
        {
            CustomClasses.Author clientAuthor = new CustomClasses.Author
            {
                ID = dataContractAuthor.ID,
                FirstName = dataContractAuthor.FirstName,
                LastName = dataContractAuthor.LastName,
                Sex = dataContractAuthor.Sex,
                BirthDate = dataContractAuthor.BirthDate
            };
            return clientAuthor;
        }
        public static List<CustomClasses.Author> ToClientList(List<ForkoLibraryServiceReference.Author> dataContractAuthorList)
        {
            List<CustomClasses.Author> customAuthors = new List<CustomClasses.Author>();
            foreach (var author in dataContractAuthorList)
            {
                customAuthors.Add(ToClient(author));
            }
            return customAuthors;
        }
    }
}

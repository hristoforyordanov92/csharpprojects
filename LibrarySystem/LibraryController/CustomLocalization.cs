﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryController
{
    //[Serializable]
    public class CustomLocalization
    {
        public string menuStripSettings = "Settings";
        public string menuStripLanguage = "Language";
        public string menuStripManager = "Manager";
        public string menuStripCities = "Cities";

        //Main form
        public string mainForm = "Library Browser";

        //Book related translation
        public string bookTab = "Books";
        public string createBookButton = "New book";
        public string updateBookButton = "Update book";
        public string deleteBookButton = "Delete book";
        public string refreshBookButton = "Refresh list";

        public string bookColumnTitle = "Title";
        public string bookColumnUniqueNumber = "Unique number (ISBN)";
        public string bookColumnPageCount = "Number of pages";
        public string bookColumnPublishDate = "Date published (dd-mm-yyyy)";
        public string bookColumnAvailable = "Availability";
        public string bookColumnFromDate = "Unavailable since (dd-mm-yyyy)";
        public string bookColumnToDate = "Unavailable until (dd-mm-yyyy)";

        public string[] isAvailable = { "Not available", "Available" };

        //Author related translation
        public string authorTab = "Authors";
        public string createAuthorButton = "New author";
        public string updateAuthorButton = "Update author";
        public string deleteAuthorButton = "Delete author";
        public string refreshAuthorButton = "Refresh list";

        public string authorColumnFirstName = "First name";
        public string authorColumnLastName = "Last name";
        public string columnSex = "Sex";
        public string authorColumnBirthDate = "Birth date (dd-mm-yyyy)";

        //User related translation
        public string userTab = "Users";
        public string createUserButton = "New user";
        public string updateUserButton = "Update user";
        public string deleteUserButton = "Delete user";
        public string refreshUserButton = "Refresh list";
        public string checkOutButton = "Check-Out";
        public string checkInButton = "Check-In";

        public string createUserForm = "New user";
        public string updateUserForm = "Updating user";

        public string userColumnFirstName = "First name";
        public string userColumnLastName = "Last name";
        public string userColumnIDCard = "ID card";
        public string userColumnEGN = "EGN";
        public string userColumnPhone = "Phone";
        public string userColumnEMail = "E-Mail";
        public string userColumnAddress = "Address";
        public string userColumnCity = "City";

        public string[] sex = { "Male", "Female" };

        //Statistics
        public string statistics = "Statistics";
        public string statYear = "The information is up to date for the following year:";
        public string statInformationLabel = "Number of checked-out books during the year";

        //Create author
        public string createAuthorForm = "New author";
        public string createAuthorForm_createButton = "Add";

        //Update author
        public string updateAuthorForm = "Updating author";
        public string updateAuthorForm_updateButton = "Update";
        public string updateAuthorForm_reseButton = "Reset";

        //Delete author
        public string unableToDeleteAuthor = "Unable to delete this author, because this author is currently being in use.";

        //Author validation
        public string invalidFirstName = "Invalid first name!";
        public string invalidLastName = "Invalid last name!";

        //Create book
        public string createBookForm = "New book";
        public string createBookForm_createButton = "Add";

        //Update book
        public string updateBookForm = "Updating book";
        public string updateBookForm_updateButton = "Update";
        public string updateBookForm_resetutton = "Reset";

        //Delete book
        public string unableToDeleteBook = "Unable to delete this book, because this book is currently being used!";

        //Book validation
        public string invalidTitle = "Invalid title!";
        public string invalidUniqueNumber = "Invalid unique number!";
        public string uniqueNumberExists = "Unique number already exists!";
        public string invalidPageCount = "Invalid page count!";
        public string pageCountInvalidFormat = "You have entered an invalid data to represent the page count.";

        //User
        public string idcardAlreadyExists = "A user with this ID card already exists!";
        public string egnAlreadyExists = "A user with this EGN already exists!";

        //Delete user
        public string unableToDeleteUser = "Unable to delete this user, because this user is currently being used!";

        public string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        public string birthDate = "Birth date";
        public string publishDate = "Date published";
        public string author = "Author";
        public string warning = "WARNING!";
        public string error = "Error!";
        public string areYouSureYouWantToDelete = "Are you sure you want to delete";
        public string processCanNotBeUndone = "This process can NOT be undone!";
        public string incorrectData = "Incorrect data!";

        //Checkout
        public string checkoutForm = "Checking-Out books";
        public string checkoutInformationLabel = "These books will be checked-out for:";
        public string checkoutButton = "Check-out";
        public string checkoutFromDateLabel = "Checking-out from";
        public string checkoutToDateLabel = "Expecting check-in by";
        public string checkoutBookListLabel = "Available books";
        public string checkinBookListLabel = "Taken books";

        //Checkin
        public string checkinForm = "Checking-In books";
        public string checkinInformationLabel = "These books will be checked-in for:";
        public string checkinButton = "Check-in";

        //City
        public string cityManagerForm = "City manager";
        public string cityManagerName = "Name";
        public string createCityButton = "Add city";
        public string updateCityButton = "Edit city";
        public string deleteCityButton = "Delete city";
        public string refreshCityButton = "Refresh list";
        public string createCityForm = "New city";
        public string editCityForm = "Edit city";

        //Random buttons
        public string cancelButton = "Cancel";

        public string theFieldNamed = "The field \"";
        public string invalidSymbols = "\" contains invalid symbols.";
        public string invalidDate = "You can not set a future date.";
        public string minSymbols = "\" must contain at least ";
        public string maxSymbols = "\" must contain at most ";
        public string symbols = " symbols.";
        public string nameSymbolsInfo = "\nSymbols allowed:\nLatin alphabet\nCyrilic alphabet\n- (dash)\n. (fullstop)";
        public string egnSymbolsInfo = "\nSymbols allowed:\nNumbers";
        public string emailRequiredSymbols = "\nSymbols required:\n@\n.";
        public string unableToDeleteCity = "Unable to delete this city. City is currently in use.";
        public string unableToCreateBookMissingAuthors = "Unable to create a new book. You must add authors first!";
        public string noCitySelected = "No city is selected!";
        public string canOnlyContainPositiveNumbers = "\" can only contain positive numbers!";
        public string smallestNumberShouldBe = "Smallest number for \"";
        public string highestNumberShouldBe = "Highest number for \"";
        public string canBe = "\" can be ";


        public static CustomLocalization GetEnglishLocalization()
        {
            return new CustomLocalization
            {

                ////MenuStrip
                //menuStripSettings = "Settings",
                //menuStripLanguage = "Language",

                ////Main form
                //mainForm = "Library Browser",

                ////Book related translation
                //bookTab = "Books",
                //createBookButton = "New book",
                //updateBookButton = "Update book",
                //deleteBookButton = "Delete book",
                //refreshBookButton = "Refresh list",

                //bookColumnTitle = "Title",
                //bookColumnUniqueNumber = "Serial number",
                //bookColumnPageCount = "Number of pages",
                //bookColumnPublishDate = "Date published (dd-mm-yyyy)",
                //bookColumnAvailable = "Availability",
                //bookColumnFromDate = "Unavailable since (dd-mm-yyyy)",
                //bookColumnToDate = "Unavailable until (dd-mm-yyyy)",

                //isAvailable = new string[] { "Not available", "Available" },

                ////Author related translation
                //authorTab = "Authors",
                //createAuthorButton = "New author",
                //updateAuthorButton = "Update author",
                //deleteAuthorButton = "Delete author",
                //refreshAuthorButton = "Refresh list",

                //authorColumnFirstName = "First name",
                //authorColumnLastName = "Last name",
                //columnSex = "Sex",
                //authorColumnBirthDate = "Birth date (dd-mm-yyyy)",

                ////User related translation
                //userTab = "Users",
                //createUserButton = "New user",
                //updateUserButton = "Update user",
                //deleteUserButton = "Delete user",
                //refreshUserButton = "Refresh list",
                //checkOutButton = "Check-Out",
                //checkInButton = "Check-In",

                //userColumnFirstName = "First name",
                //userColumnLastName = "Last name",
                //userColumnIDCard = "ID card",
                //userColumnEGN = "EGN",
                //userColumnPhone = "Phone",
                //userColumnEMail = "E-Mail",
                //userColumnAddress = "Address",
                //userColumnCity = "City",

                //sex = new string[] { "Male", "Female" }
            };
        }
        public static CustomLocalization GetBulgarianLocalization()
        {
            return new CustomLocalization
            {
                menuStripSettings = "Настройки",
                menuStripLanguage = "Език",
                menuStripManager = "Мениджър",
                menuStripCities = "Градове",

                //Main form
                mainForm = "Библиотечен Браузър",

                //Book related translation
                bookTab = "Книги",
                createBookButton = "Нова книга",
                updateBookButton = "Обнови книга",
                deleteBookButton = "Изтрий книга",
                refreshBookButton = "Обнови лист",

                bookColumnTitle = "Заглавие",
                bookColumnUniqueNumber = "Уникален номер (ISBN)",
                bookColumnPageCount = "Брой страници",
                bookColumnPublishDate = "Публикувана на (дд-мм-гггг)",
                bookColumnAvailable = "Наличност",
                bookColumnFromDate = "Липсва от (дд-мм-гггг)",
                bookColumnToDate = "Липсва до (дд-мм-гггг)",

                isAvailable = new string[] { "Не е налична", "Налична" },

                //Author related translation
                authorTab = "Автори",
                createAuthorButton = "Нов автор",
                updateAuthorButton = "Обнови автор",
                deleteAuthorButton = "Изтрий автор",
                refreshAuthorButton = "Обнови списък",

                authorColumnFirstName = "Име",
                authorColumnLastName = "Фамилия",
                columnSex = "Пол",
                authorColumnBirthDate = "Рождена дата (дд-мм-гггг)",

                //User related translation
                userTab = "Потребители",
                createUserButton = "Нов потребител",
                updateUserButton = "Обнови потребител",
                deleteUserButton = "Изтрий потребител",
                refreshUserButton = "Обновни списък",
                checkOutButton = "Отдай книга/и",
                checkInButton = "Приеми книга/и",

                createUserForm = "Нов потребител",
                updateUserForm = "Обновяване на потребител",

                userColumnFirstName = "Име",
                userColumnLastName = "Фамилия",
                userColumnIDCard = "Лична карта",
                userColumnEGN = "ЕГН",
                userColumnPhone = "Телефон",
                userColumnEMail = "E-Mail",
                userColumnAddress = "Адрес",
                userColumnCity = "Град",

                sex = new string[] { "Мъж", "Жена" },

                //Statistics
                statistics = "Статистика",
                statYear = "Информацията е актуална към следната година:",
                statInformationLabel = "Брой книги наети през годината",

                //Create author
                createAuthorForm = "Нов автор",
                createAuthorForm_createButton = "Добави",

                //Update author
                updateAuthorForm = "Обновяване на автор",
                updateAuthorForm_updateButton = "Обнови",
                updateAuthorForm_reseButton = "Занули",

                //Delete author
                unableToDeleteAuthor = "Невъзможно е изтриването на автора, защото този автор се използва от програмата.",

                //Author validation
                invalidFirstName = "Невалидно име!",
                invalidLastName = "Невалидна фамилия!",

                //Create book
                createBookForm = "Нова книга",
                createBookForm_createButton = "Добави",

                //Update book
                updateBookForm = "Обновяване на книга",
                updateBookForm_updateButton = "Обнови",
                updateBookForm_resetutton = "Занули",

                //Delete book
                unableToDeleteBook = "Невъзможно е изтриването на книгата, защото тази книга се използва от програмата.",

                //Book validation
                invalidTitle = "Невалидно заглавие!",
                invalidUniqueNumber = "Невалиден уникален номер (ISBN)!",
                uniqueNumberExists = "Уникален номер (ISBN) вече съществува!",
                invalidPageCount = "Невалиден брой страници!",
                pageCountInvalidFormat = "You have entered an invalid data to represent the page count.",

                //User
                idcardAlreadyExists = "Вече съществува потребител с този номер на личната карта!",
                egnAlreadyExists = "Вече съществува потребител с това ЕГН!",

                //Delete user
                unableToDeleteUser = "Невъзможно е изтриването на потребителя, защото този потребител се използва от програмата!",

                months = new string[] { "Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември" },
                birthDate = "Рождена дата",
                publishDate = "Дата на публикуване",
                author = "Автор",
                warning = "ВНИМАНИЕ!",
                error = "Грешка!",
                areYouSureYouWantToDelete = "Сигурни ли сте, че искате да изтриете",
                processCanNotBeUndone = "Този процес е необратим!",
                incorrectData = "Невалидни данни!",

                //Checkout
                checkoutForm = "Наемане на книги",
                checkoutInformationLabel = "Тези книги ще бъдат наети от:",
                checkoutButton = "Наеми книги",
                checkoutFromDateLabel = "Наемане от",
                checkoutToDateLabel = "Връщане до",
                checkoutBookListLabel = "Налични книги",
                checkinBookListLabel = "Наети книги",

                //Checkin
                checkinForm = "Връщане на книги",
                checkinInformationLabel = "Тези книги ще бъдат върнати от:",
                checkinButton = "Върни",

                //City
                cityManagerForm = "Мениджър на градовете",
                cityManagerName = "Име",
                createCityButton = "Добави град",
                updateCityButton = "Обнови град",
                deleteCityButton = "Изтрий град",
                refreshCityButton = "Обнови списък",
                createCityForm = "Нов град",
                editCityForm = "Обновяване",

                //Random buttons
                cancelButton = "Назад",

                theFieldNamed = "Полето \"",
                invalidSymbols = "\" съдържа невалидни символи.",
                invalidDate = "Не може да се задава бъдеща дата.",
                minSymbols = "\" трябва да съдържа поне ",
                maxSymbols = "\" трябва да съдържа най-много ",
                symbols = " символ/а.",
                nameSymbolsInfo = "\nПозволени символи:\nЛатинска азбука\nКирилица\n- (тире)\n. (точка)",
                egnSymbolsInfo = "\nПозволени символи:\nЦифри",
                emailRequiredSymbols = "\nНужни символи:\n@\n.",
                unableToDeleteCity = "Невъзможно е изтриването на града, защото този град се използва от програмата.",
                unableToCreateBookMissingAuthors = "Невъзможно създаване на нова книга. Първо добавете автори!",
                noCitySelected = "Няма избран град!",
                canOnlyContainPositiveNumbers = "\" може да съдържа само положителни числа!",
                smallestNumberShouldBe = "Най-малкото число за \"",
                highestNumberShouldBe = "Най-голямото число за \"",
                canBe = " може да е "
            };
        }
    }
}
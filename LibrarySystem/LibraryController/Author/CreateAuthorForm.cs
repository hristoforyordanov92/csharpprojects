﻿using BusinessLibrary;
using LibraryController.HelperClasses;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LibraryController.ForkoLibraryServiceReference;

namespace LibraryController.Author
{
    public partial class CreateAuthorForm : Form
    {
        private ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
        
        private AuthorFields authorFields;
        private CustomComboDate customComboDate;

        private string firstName;
        private string lastName;
        private DateTime date;

        public CreateAuthorForm()
        {
            InitializeComponent();

            authorFields = new AuthorFields();
            customComboDate = new CustomComboDate(YearComboBox, MonthComboBox, DayComboBox, DateTime.Now.Year, 1000);
            //customComboDate.SetSecondaryCustomComboDate()

            //Custom setups
            UIGod.FillUpComboBox(SexComboBox, MainForm.loc.sex);
            //customComboDate.Setup(DateTime.Now.Year, 1000);
            //SetupBirthDateComboBoxes();
            //SubscribeYearAndMonthComboBoxes();

            Localize();

            LastNameText.Focus();
            LastNameText.SelectAll();
        }

        private void Localize()
        {
            this.Text = MainForm.loc.createAuthorForm;
            toolTip1.SetToolTip(CreateAuthorButton, MainForm.loc.createAuthorForm_createButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            FirstNameLabel.Text = MainForm.loc.authorColumnFirstName;
            LastNameLabel.Text = MainForm.loc.authorColumnLastName;
            SexLabel.Text = MainForm.loc.columnSex;
            BirthDateLabel.Text = MainForm.loc.birthDate;
        }

        //private void SetupBirthDateComboBoxes()
        //{
        //    SetupYearComboBox();
        //    SetupMonthComboBox();
        //    SetupDayComboBox(null, EventArgs.Empty);
        //}
        //private void SetupYearComboBox()
        //{
        //    var maxYear = DateTime.Now.Year;
        //    var allYears = new List<int>();
        //    for (int i = maxYear; i >= 1500; i--)
        //    {
        //        YearComboBox.Items.Add(i);
        //    }
        //    YearComboBox.SelectedItem = YearComboBox.Items[0];
        //}
        //private void SetupMonthComboBox()
        //{
        //    if (MonthComboBox.Items.Count > 0)
        //    {
        //        MonthComboBox.Items.Clear();
        //    }

        //    for (int i = 0; i < 12; i++)
        //    {
        //        MonthComboBox.Items.Add(MainForm.loc.months[i]);
        //    }
        //    MonthComboBox.SelectedIndex = 0;
        //}
        //private void SetupDayComboBox(object sender, EventArgs e)
        //{
        //    int oldSelectedIndex = DayComboBox.SelectedIndex;
        //    int daysInMonth = DateTime.DaysInMonth((int)YearComboBox.SelectedItem, MonthComboBox.SelectedIndex + 1);
        //    int newSelectedIndex = 0;

        //    if (DayComboBox.Items.Count > 0)
        //    {
        //        if (oldSelectedIndex > daysInMonth - 1)
        //        {
        //            newSelectedIndex = daysInMonth - 1;
        //        }
        //        else
        //        {
        //            newSelectedIndex = oldSelectedIndex;
        //        }
        //        DayComboBox.Items.Clear();
        //    }

        //    for (int i = 1; i <= daysInMonth; i++)
        //    {
        //        DayComboBox.Items.Add(i);
        //    }

        //    DayComboBox.SelectedItem = DayComboBox.Items[newSelectedIndex];
        //}
        //private void SubscribeYearAndMonthComboBoxes()
        //{
        //    YearComboBox.SelectedIndexChanged += SetupDayComboBox;
        //    MonthComboBox.SelectedIndexChanged += SetupDayComboBox;
        //}

        private void CreateAuthorButton_Click(object sender, EventArgs e)
        {
            ForkoLibraryServiceReference.Author newAuthor = new ForkoLibraryServiceReference.Author();

            firstName = FirstNameText.Text;
            lastName = LastNameText.Text;
            date = new DateTime(
                        (int)YearComboBox.SelectedItem,
                        MonthComboBox.SelectedIndex + 1,
                        (int)DayComboBox.SelectedItem
                    );

            if (AuthorValidations.CreateAuthor(ref firstName, ref lastName, date, authorFields))
            {
                newAuthor.FirstName = firstName;
                newAuthor.LastName = lastName;
                newAuthor.Sex = (byte)SexComboBox.SelectedIndex;
                newAuthor.BirthDate = new DateTime(
                        (int)YearComboBox.SelectedItem,
                        MonthComboBox.SelectedIndex + 1,
                        (int)DayComboBox.SelectedItem
                    );
                dal.CreateAuthor(newAuthor);
                Close();
            }
        }

        private void TextBox_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void FirstNameText_Focus(object sender, EventArgs e)
        {
            FirstNameText.Focus();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
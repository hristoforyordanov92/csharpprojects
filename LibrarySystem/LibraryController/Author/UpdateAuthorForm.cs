﻿using BusinessLibrary;
using LibraryController.ForkoLibraryServiceReference;
using LibraryController.HelperClasses;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LibraryController.Author
{
    public partial class UpdateAuthorForm : Form
    {
        private ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
        private BusinessLibrary.Author currentAuthor;

        private AuthorFields authorFields;
        private CustomComboDate customComboDate;

        private string firstName;
        private string lastName;
        private DateTime date;

        public UpdateAuthorForm(BusinessLibrary.Author importedAuthor)
        {
            InitializeComponent();

            authorFields = new AuthorFields();

            currentAuthor = importedAuthor;

            //Custom setups
            UIGod.FillUpComboBox(SexComboBox, MainForm.loc.sex);
            customComboDate = new CustomComboDate(YearComboBox, MonthComboBox, DayComboBox, DateTime.Now.Year, 1000);
            SetupBirthDateComboBoxes();
            SubscribeYearAndMonthComboBoxes();
            FillUpData(null, EventArgs.Empty);
            Localize();

            //Initial selection
            FirstNameText.Focus();
            FirstNameText.SelectAll();
        }

        private void Localize()
        {
            this.Text = MainForm.loc.updateAuthorForm;
            toolTip1.SetToolTip(UpdateAuthorButton, MainForm.loc.updateAuthorForm_updateButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            toolTip1.SetToolTip(ResetButton, MainForm.loc.updateAuthorForm_reseButton);
            FirstNameLabel.Text = MainForm.loc.authorColumnFirstName;
            LastNameLabel.Text = MainForm.loc.authorColumnLastName;
            SexLabel.Text = MainForm.loc.columnSex;
            BirthDateLabel.Text = MainForm.loc.birthDate;

        }

        private void FillUpData(object sender, EventArgs e)
        {
            FirstNameText.Text = currentAuthor.FirstName;
            LastNameText.Text = currentAuthor.LastName;
            SexComboBox.SelectedIndex = currentAuthor.Sex;
            YearComboBox.SelectedItem = currentAuthor.BirthDate.Year;
            MonthComboBox.SelectedIndex = currentAuthor.BirthDate.Month - 1;
            DayComboBox.SelectedItem = currentAuthor.BirthDate.Day;
        }
        private void SetupBirthDateComboBoxes()
        {
            SetupYearComboBox();
            SetupMonthComboBox();
            SetupDayComboBox(null, EventArgs.Empty);
        }
        private void SetupYearComboBox()
        {
            var maxYear = DateTime.Now.Year;
            var allYears = new List<int>();
            for (int i = maxYear; i >= 1500; i--)
            {
                YearComboBox.Items.Add(i);
            }
            YearComboBox.SelectedItem = YearComboBox.Items[0];
        }
        private void SetupMonthComboBox()
        {
            if (MonthComboBox.Items.Count > 0)
            {
                MonthComboBox.Items.Clear();
            }

            for (int i = 0; i < 12; i++)
            {
                MonthComboBox.Items.Add(MainForm.loc.months[i]);
            }
            MonthComboBox.SelectedIndex = 0;
        }
        private void SetupDayComboBox(object sender, EventArgs e)
        {
            int oldSelectedIndex = DayComboBox.SelectedIndex;
            int daysInMonth = DateTime.DaysInMonth((int)YearComboBox.SelectedItem, MonthComboBox.SelectedIndex + 1);
            int newSelectedIndex = 0;

            if (DayComboBox.Items.Count > 0)
            {
                if (oldSelectedIndex > daysInMonth - 1)
                {
                    newSelectedIndex = daysInMonth - 1;
                }
                else
                {
                    newSelectedIndex = oldSelectedIndex;
                }
                DayComboBox.Items.Clear();
            }

            for (int i = 1; i <= daysInMonth; i++)
            {
                DayComboBox.Items.Add(i);
            }

            DayComboBox.SelectedItem = DayComboBox.Items[newSelectedIndex];
        }
        private void SubscribeYearAndMonthComboBoxes()
        {
            YearComboBox.SelectedIndexChanged += SetupDayComboBox;
            MonthComboBox.SelectedIndexChanged += SetupDayComboBox;
        }

        private void UpdateAuthorButton_Click(object sender, EventArgs e)
        {
            ForkoLibraryServiceReference.Author newAuthor = new ForkoLibraryServiceReference.Author();

            firstName = FirstNameText.Text;
            lastName = LastNameText.Text;

            date = new DateTime((int)YearComboBox.SelectedItem,
                                 MonthComboBox.SelectedIndex + 1,
                                 (int)DayComboBox.SelectedItem
                                 );

            if (AuthorValidations.UpdateAuthor(ref firstName, ref lastName, date, authorFields))
            {
                newAuthor.FirstName = firstName;
                newAuthor.LastName = lastName;
                newAuthor.Sex = (byte)SexComboBox.SelectedIndex;
                newAuthor.BirthDate =
                    new DateTime((int)YearComboBox.SelectedItem,
                                 MonthComboBox.SelectedIndex + 1,
                                 (int)DayComboBox.SelectedItem
                                 );
                dal.UpdateAuthor(currentAuthor.ID, newAuthor);
                Close();
            }
        }

        private void TextBox_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void FirstNameText_Focus(object sender, EventArgs e)
        {
            FirstNameText.Focus();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
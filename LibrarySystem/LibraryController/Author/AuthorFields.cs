﻿using LibraryController.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryController.Author
{
    public class AuthorFields
    {
        public CustomField firstName;
        public CustomField lastName;

        public AuthorFields()
        {
            firstName = new CustomField(MainForm.loc.authorColumnFirstName, 2, 50);
            lastName = new CustomField(MainForm.loc.authorColumnLastName, 2, 50);
        }
    }
}

﻿using LibraryController.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.Author
{
    class AuthorValidations
    {
        private static bool Name(ref string name, CustomField customField)
        {
            Validations.FilterEmptySpaces(ref name);

            char[] firstNameToChar = name.ToCharArray();
            foreach (var @char in firstNameToChar)
            {
                if (!Validations.AllowedNameCharset(@char))
                {
                    MessageBox.Show(MainForm.loc.theFieldNamed + customField.Name + MainForm.loc.invalidSymbols + MainForm.loc.nameSymbolsInfo, MainForm.loc.incorrectData, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            if (!Validations.MinSymbols(name, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(name, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            return true;
        }
        private static bool Date(DateTime date)
        {
            if (!Validations.FutureDate(date))
            {
                return false;
            }
            return true;
        }

        public static bool CreateAuthor(ref string firstName, ref string lastName, DateTime date, AuthorFields authorFields)
        {
            if (!Name(ref firstName, authorFields.firstName)) { return false; }
            if (!Name(ref lastName, authorFields.lastName)) { return false; }
            if (!Date(date)) { return false; }

            return true;
        }
        public static bool UpdateAuthor(ref string firstName, ref string lastName, DateTime date, AuthorFields authorFields)
        {
            return CreateAuthor(ref firstName, ref lastName, date, authorFields);
        }
    }
}

﻿namespace LibraryController.Author
{
    partial class UpdateAuthorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateAuthorForm));
            this.BirthDateLabel = new System.Windows.Forms.Label();
            this.SexLabel = new System.Windows.Forms.Label();
            this.SexComboBox = new System.Windows.Forms.ComboBox();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.DayComboBox = new System.Windows.Forms.ComboBox();
            this.FirstNameText = new System.Windows.Forms.TextBox();
            this.MonthComboBox = new System.Windows.Forms.ComboBox();
            this.LastNameText = new System.Windows.Forms.TextBox();
            this.YearComboBox = new System.Windows.Forms.ComboBox();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.CancelButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.UpdateAuthorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BirthDateLabel
            // 
            this.BirthDateLabel.AutoSize = true;
            this.BirthDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BirthDateLabel.Location = new System.Drawing.Point(12, 79);
            this.BirthDateLabel.Name = "BirthDateLabel";
            this.BirthDateLabel.Size = new System.Drawing.Size(64, 16);
            this.BirthDateLabel.TabIndex = 10;
            this.BirthDateLabel.Text = "Birth date";
            // 
            // SexLabel
            // 
            this.SexLabel.AutoSize = true;
            this.SexLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SexLabel.Location = new System.Drawing.Point(12, 119);
            this.SexLabel.Name = "SexLabel";
            this.SexLabel.Size = new System.Drawing.Size(31, 16);
            this.SexLabel.TabIndex = 9;
            this.SexLabel.Text = "Sex";
            // 
            // SexComboBox
            // 
            this.SexComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SexComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SexComboBox.FormattingEnabled = true;
            this.SexComboBox.Location = new System.Drawing.Point(331, 116);
            this.SexComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.SexComboBox.Name = "SexComboBox";
            this.SexComboBox.Size = new System.Drawing.Size(100, 24);
            this.SexComboBox.TabIndex = 5;
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstNameLabel.Location = new System.Drawing.Point(12, 14);
            this.FirstNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(70, 16);
            this.FirstNameLabel.TabIndex = 12;
            this.FirstNameLabel.Text = "First name";
            // 
            // DayComboBox
            // 
            this.DayComboBox.DropDownHeight = 200;
            this.DayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DayComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DayComboBox.FormattingEnabled = true;
            this.DayComboBox.IntegralHeight = false;
            this.DayComboBox.Location = new System.Drawing.Point(156, 76);
            this.DayComboBox.Name = "DayComboBox";
            this.DayComboBox.Size = new System.Drawing.Size(52, 24);
            this.DayComboBox.TabIndex = 2;
            // 
            // FirstNameText
            // 
            this.FirstNameText.BackColor = System.Drawing.SystemColors.Window;
            this.FirstNameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstNameText.Location = new System.Drawing.Point(156, 11);
            this.FirstNameText.Margin = new System.Windows.Forms.Padding(2);
            this.FirstNameText.MaxLength = 100;
            this.FirstNameText.Name = "FirstNameText";
            this.FirstNameText.Size = new System.Drawing.Size(275, 22);
            this.FirstNameText.TabIndex = 0;
            this.FirstNameText.Enter += new System.EventHandler(this.TextBox_Enter);
            // 
            // MonthComboBox
            // 
            this.MonthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MonthComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MonthComboBox.FormattingEnabled = true;
            this.MonthComboBox.Location = new System.Drawing.Point(214, 76);
            this.MonthComboBox.Name = "MonthComboBox";
            this.MonthComboBox.Size = new System.Drawing.Size(140, 24);
            this.MonthComboBox.TabIndex = 3;
            // 
            // LastNameText
            // 
            this.LastNameText.BackColor = System.Drawing.SystemColors.Window;
            this.LastNameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastNameText.Location = new System.Drawing.Point(156, 38);
            this.LastNameText.Margin = new System.Windows.Forms.Padding(2);
            this.LastNameText.MaxLength = 100;
            this.LastNameText.Name = "LastNameText";
            this.LastNameText.Size = new System.Drawing.Size(275, 22);
            this.LastNameText.TabIndex = 1;
            this.LastNameText.Enter += new System.EventHandler(this.TextBox_Enter);
            // 
            // YearComboBox
            // 
            this.YearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.YearComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YearComboBox.FormattingEnabled = true;
            this.YearComboBox.Location = new System.Drawing.Point(360, 76);
            this.YearComboBox.Name = "YearComboBox";
            this.YearComboBox.Size = new System.Drawing.Size(71, 24);
            this.YearComboBox.TabIndex = 4;
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastNameLabel.Location = new System.Drawing.Point(12, 41);
            this.LastNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(70, 16);
            this.LastNameLabel.TabIndex = 11;
            this.LastNameLabel.Text = "Last name";
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Image = global::LibraryController.Properties.Resources.checkin;
            this.CancelButton.Location = new System.Drawing.Point(383, 164);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(48, 48);
            this.CancelButton.TabIndex = 8;
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.Image = global::LibraryController.Properties.Resources.cancel_music;
            this.ResetButton.Location = new System.Drawing.Point(331, 164);
            this.ResetButton.Margin = new System.Windows.Forms.Padding(2);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(48, 48);
            this.ResetButton.TabIndex = 7;
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.FillUpData);
            // 
            // UpdateAuthorButton
            // 
            this.UpdateAuthorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateAuthorButton.Image = global::LibraryController.Properties.Resources.editEntry;
            this.UpdateAuthorButton.Location = new System.Drawing.Point(11, 164);
            this.UpdateAuthorButton.Margin = new System.Windows.Forms.Padding(2);
            this.UpdateAuthorButton.Name = "UpdateAuthorButton";
            this.UpdateAuthorButton.Size = new System.Drawing.Size(48, 48);
            this.UpdateAuthorButton.TabIndex = 6;
            this.UpdateAuthorButton.UseVisualStyleBackColor = true;
            this.UpdateAuthorButton.Click += new System.EventHandler(this.UpdateAuthorButton_Click);
            // 
            // UpdateAuthorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 226);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.BirthDateLabel);
            this.Controls.Add(this.SexLabel);
            this.Controls.Add(this.SexComboBox);
            this.Controls.Add(this.FirstNameLabel);
            this.Controls.Add(this.DayComboBox);
            this.Controls.Add(this.FirstNameText);
            this.Controls.Add(this.MonthComboBox);
            this.Controls.Add(this.LastNameText);
            this.Controls.Add(this.YearComboBox);
            this.Controls.Add(this.LastNameLabel);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.UpdateAuthorButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "UpdateAuthorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Author";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button UpdateAuthorButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Label BirthDateLabel;
        private System.Windows.Forms.Label SexLabel;
        private System.Windows.Forms.ComboBox SexComboBox;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.ComboBox DayComboBox;
        private System.Windows.Forms.TextBox FirstNameText;
        private System.Windows.Forms.ComboBox MonthComboBox;
        private System.Windows.Forms.TextBox LastNameText;
        private System.Windows.Forms.ComboBox YearComboBox;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
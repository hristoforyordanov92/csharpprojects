﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.HelperClasses
{
    public class CustomComboDate
    {
        public ComboBox Year { get; set; }
        public ComboBox Month { get; set; }
        public ComboBox Day { get; set; }
        private CustomComboDate secondaryCustomComboDate;

        public CustomComboDate(ComboBox year, ComboBox month, ComboBox day)
        {
            Year = year;
            Month = month;
            Day = day;
        }
        public CustomComboDate(ComboBox year, ComboBox month, ComboBox day, int fromYear, int toYear) : this(year, month, day)
        {
            Setup(fromYear, toYear);
        }
        public CustomComboDate(ComboBox year, ComboBox month, ComboBox day, int fromYear, int toYear, CustomComboDate secondaryCustomComboDate) : this(year, month, day, fromYear, toYear)
        {
            this.secondaryCustomComboDate = secondaryCustomComboDate;
        }

        public void Setup(int fromYear, int toYear)
        {
            Year.Items.Clear();
            Month.Items.Clear();
            Day.Items.Clear();

            int indexChanger = 1;
            if (fromYear > toYear)
            {
                indexChanger = -1;
            }

            for (int i = fromYear; i != toYear - 1; i += indexChanger)
            {
                Year.Items.Add(i);
            }
            Year.SelectedItem = Year.Items[0];

            for (int i = 0; i < 12; i++)
            {
                Month.Items.Add(MainForm.loc.months[i]);
            }
            Month.SelectedIndex = DateTime.Now.Month - 1;

            SetupDay();

            Year.SelectedIndexChanged += SetupDay;
            Month.SelectedIndexChanged += SetupDay;

            SetDate(DateTime.Now);
        }
        public void SetupDay()
        {
            int oldSelectedIndex = Day.SelectedIndex;
            int daysInMonth = DateTime.DaysInMonth((int)Year.SelectedItem, Month.SelectedIndex + 1);
            int newSelectedIndex = 0;

            if (Day.Items.Count > 0)
            {
                if (oldSelectedIndex > daysInMonth - 1)
                {
                    newSelectedIndex = daysInMonth - 1;
                }
                else
                {
                    newSelectedIndex = oldSelectedIndex;
                }
                Day.Items.Clear();
            }

            for (int i = 1; i <= daysInMonth; i++)
            {
                Day.Items.Add(i);
            }

            Day.SelectedItem = Day.Items[newSelectedIndex];
        }
        public void SetupDay(object sender, EventArgs e)
        {
            int oldSelectedIndex = Day.SelectedIndex;
            int daysInMonth = DateTime.DaysInMonth((int)Year.SelectedItem, Month.SelectedIndex + 1);
            int newSelectedIndex = 0;

            if (Day.Items.Count > 0)
            {
                if (oldSelectedIndex > daysInMonth - 1)
                {
                    newSelectedIndex = daysInMonth - 1;
                }
                else
                {
                    newSelectedIndex = oldSelectedIndex;
                }
                Day.Items.Clear();
            }

            for (int i = 1; i <= daysInMonth; i++)
            {
                Day.Items.Add(i);
            }

            Day.SelectedItem = Day.Items[newSelectedIndex];
        }
        public void SetDate(DateTime setToDate)
        {
            Year.SelectedItem = setToDate.Year;
            Month.SelectedIndex = setToDate.Month - 1;
            Day.SelectedItem = setToDate.Day;
            SetSecondaryDate(null, EventArgs.Empty);
        }
        public void SetDate(CustomComboDate customComboDate)
        {
            Year.SelectedItem = customComboDate.Year.SelectedItem;
            Month.SelectedIndex = customComboDate.Month.SelectedIndex;
            Day.SelectedItem = customComboDate.Day.SelectedItem;
            SetSecondaryDate(null, EventArgs.Empty);
        }
        private void SetSecondaryDate(object sender, EventArgs e)
        {
            if (secondaryCustomComboDate != null)
            {
                DateTime customDate = new DateTime((int)Year.SelectedItem,
                                                   Month.SelectedIndex + 1,
                                                   (int)Day.SelectedItem);
                DateTime secondaryDate = new DateTime((int)secondaryCustomComboDate.Year.SelectedItem,
                                                   secondaryCustomComboDate.Month.SelectedIndex + 1,
                                                   (int)secondaryCustomComboDate.Day.SelectedItem);
                if (customDate > secondaryDate)
                {
                    secondaryCustomComboDate.SetDate(this);
                }
            }
        }
        public void SetSecondaryCustomComboDate(CustomComboDate customComboDate)
        {
            secondaryCustomComboDate = customComboDate;
            Year.SelectedIndexChanged += SetSecondaryDate;
            Month.SelectedIndexChanged += SetSecondaryDate;
            Day.SelectedIndexChanged += SetSecondaryDate;
            secondaryCustomComboDate.Year.SelectedIndexChanged += SetSecondaryDate;
            secondaryCustomComboDate.Month.SelectedIndexChanged += SetSecondaryDate;
            secondaryCustomComboDate.Day.SelectedIndexChanged += SetSecondaryDate;
        }
        public void RemoveSecondaryCustomComboDate()
        {
            Year.SelectedIndexChanged -= SetSecondaryDate;
            Month.SelectedIndexChanged -= SetSecondaryDate;
            Day.SelectedIndexChanged -= SetSecondaryDate;
            secondaryCustomComboDate.Year.SelectedIndexChanged -= SetSecondaryDate;
            secondaryCustomComboDate.Month.SelectedIndexChanged -= SetSecondaryDate;
            secondaryCustomComboDate.Day.SelectedIndexChanged -= SetSecondaryDate;
            secondaryCustomComboDate = null;
        }
    }
}
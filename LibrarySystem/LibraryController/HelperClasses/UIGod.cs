﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController
{
    //This class is a UI God, because it will create UI elements. Just like God creates stuff...
    public static class UIGod
    {
        public static void FillUpComboBox(ComboBox comboBox, string[] items)
        {
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.Items.Clear();

            foreach (var item in items)
            {
                comboBox.Items.Add(item);
            }
            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedItem = comboBox.Items[0];
            }
        }
        public static void FillUpComboBox(ComboBox comboBox, CustomClasses.City[] items)
        {
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.Items.Clear();

            foreach (var item in items)
            {
                comboBox.Items.Add(item);
            }
            if (comboBox.Items.Count > 0)
            {
                comboBox.SelectedItem = comboBox.Items[0];
            }
        }
        
        public static void StylizeDataGridView(DataGridView dataGridView)
        {
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.RowHeadersVisible = false;
            dataGridView.AllowUserToResizeColumns = false;
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.BackgroundColor = Color.FromKnownColor(KnownColor.Control);
            dataGridView.GridColor = Color.FromKnownColor(KnownColor.ControlDark);
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Sunken;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
            dataGridView.Font = new Font(FontFamily.GenericSansSerif, 12f);
            dataGridView.AutoGenerateColumns = false;
            //dataGridView.Columns[0].SortMode = DataGridViewColumnSortMode.Automatic;

            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                column.CellTemplate.Style.Font = new Font(FontFamily.GenericSansSerif, 10f);
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                column.SortMode = DataGridViewColumnSortMode.Automatic;
            }
        }

        
    }
}
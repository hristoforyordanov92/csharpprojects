﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController
{
    public static class Validations
    {
        public static bool CheckIfNumber(string input, string fieldName)
        {
            if (!long.TryParse(input, out long result))
            {
                MessageBox.Show(MainForm.loc.theFieldNamed + fieldName + MainForm.loc.invalidSymbols + MainForm.loc.egnSymbolsInfo,
                                MainForm.loc.incorrectData,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
        public static void FilterEmptySpaces(ref string input)
        {
            char[] charInput = input.ToCharArray();
            input = "";
            foreach (var @char in charInput)
            {
                if (@char != ' ')
                {
                    input += @char;
                }
            }
        }
        public static void TrimEmptySpaces(ref string input)
        {
            input = input.TrimStart(' ');
            input = input.TrimEnd(' ');
        }
        public static bool MinSymbols(string input, int minSymbols, string fieldName)
        {
            if (input.Length < minSymbols || string.IsNullOrEmpty(input))
            {
                MessageBox.Show(MainForm.loc.theFieldNamed + fieldName + MainForm.loc.minSymbols + minSymbols.ToString() + MainForm.loc.symbols,
                                MainForm.loc.incorrectData,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public static bool MaxSymbols(string input, int maxSymbols, string fieldName)
        {
            if (input.Length > maxSymbols)
            {
                MessageBox.Show(MainForm.loc.theFieldNamed + fieldName + MainForm.loc.maxSymbols + maxSymbols.ToString() + MainForm.loc.symbols,
                                MainForm.loc.incorrectData,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public static bool CheckForRequiredSymbols(string input, string fieldName, char[] symbols, string requiredSymbols)
        {
            char[] charInput = input.ToCharArray();
            foreach (var symbol in symbols)
            {
                bool contains = false;
                foreach (var @char in charInput)
                {
                    if (@char == symbol)
                    {
                        contains = true;
                        break;
                    }
                }
                if (!contains)
                {
                    MessageBox.Show(MainForm.loc.theFieldNamed + fieldName + MainForm.loc.invalidSymbols + requiredSymbols,
                                        MainForm.loc.incorrectData.ToString(),
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }
        public static bool AllowedNameCharset(char charToTest)
        {
            if ((charToTest >= 'a' && charToTest <= 'z') ||
                (charToTest >= 'A' && charToTest <= 'Z') ||
                (charToTest >= 'а' && charToTest <= 'я') ||
                (charToTest >= 'А' && charToTest <= 'Я') ||
                (charToTest == '-') ||
                (charToTest == '.'))
            {
                return true;
            }

            return false;
        }
        public static bool FutureDate(DateTime date)
        {
            if (date > DateTime.Now)
            {
                MessageBox.Show(MainForm.loc.invalidDate,
                                MainForm.loc.incorrectData,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
        public static bool PositiveNumber(long input, string fieldName)
        {
            if (input <= 0)
            {
                MessageBox.Show(MainForm.loc.theFieldNamed + fieldName + MainForm.loc.canOnlyContainPositiveNumbers,
                                MainForm.loc.incorrectData,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
        public static bool MinNumber(long input, string fieldName, long minNumber)
        {
            if (input < minNumber)
            {
                MessageBox.Show(MainForm.loc.smallestNumberShouldBe + fieldName + MainForm.loc.canBe + minNumber + "!");
                return false;
            }

            return true;
        }
        public static bool MaxNumber(long input, string fieldName, long maxNumber)
        {
            if (input > maxNumber)
            {
                MessageBox.Show(MainForm.loc.highestNumberShouldBe + fieldName + MainForm.loc.canBe + maxNumber + "!");
                return false;
            }

            return true;
        }
    }
}
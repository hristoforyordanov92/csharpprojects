﻿using LibraryController.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryController.City
{
    public class CityFields
    {
        public CustomField name;

        public CityFields()
        {
            name = new CustomField(MainForm.loc.cityManagerName, 2, 50);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLibrary;
using LibraryController.ForkoLibraryServiceReference;

namespace LibraryController.City
{
    public partial class UpdateCity : Form
    {
        ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
        CustomClasses.City oldCity;

        private CityFields cityFields;

        private string name;

        public UpdateCity(CustomClasses.City oldCity)
        {
            InitializeComponent();
            this.oldCity = oldCity;

            cityFields = new CityFields();

            CityNameText.Text = oldCity.Name;
            Localize();
        }

        private void Localize()
        {
            Text = MainForm.loc.editCityForm;
            CityNameLabel.Text = MainForm.loc.cityManagerName;
            toolTip1.SetToolTip(UpdateCityButton, MainForm.loc.updateCityButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
        }

        private void UpdateCityButton_Click(object sender, EventArgs e)
        {
            name = CityNameText.Text;

            if (CityValidations.Validate(ref name, cityFields))
            {
                dal.UpdateCity(oldCity.ID, new ForkoLibraryServiceReference.City { Name = name });
                Close();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
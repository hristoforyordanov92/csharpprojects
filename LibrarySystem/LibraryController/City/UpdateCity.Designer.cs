﻿namespace LibraryController.City
{
    partial class UpdateCity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateCity));
            this.CityNameText = new System.Windows.Forms.TextBox();
            this.UpdateCityButton = new System.Windows.Forms.Button();
            this.CityNameLabel = new System.Windows.Forms.Label();
            this.CancelButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // CityNameText
            // 
            this.CityNameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CityNameText.Location = new System.Drawing.Point(99, 11);
            this.CityNameText.Margin = new System.Windows.Forms.Padding(2);
            this.CityNameText.Name = "CityNameText";
            this.CityNameText.Size = new System.Drawing.Size(152, 23);
            this.CityNameText.TabIndex = 0;
            // 
            // UpdateCityButton
            // 
            this.UpdateCityButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UpdateCityButton.Image = global::LibraryController.Properties.Resources.editEntry;
            this.UpdateCityButton.Location = new System.Drawing.Point(11, 61);
            this.UpdateCityButton.Margin = new System.Windows.Forms.Padding(2);
            this.UpdateCityButton.Name = "UpdateCityButton";
            this.UpdateCityButton.Size = new System.Drawing.Size(48, 48);
            this.UpdateCityButton.TabIndex = 1;
            this.UpdateCityButton.UseVisualStyleBackColor = true;
            this.UpdateCityButton.Click += new System.EventHandler(this.UpdateCityButton_Click);
            // 
            // CityNameLabel
            // 
            this.CityNameLabel.AutoSize = true;
            this.CityNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CityNameLabel.Location = new System.Drawing.Point(11, 14);
            this.CityNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CityNameLabel.Name = "CityNameLabel";
            this.CityNameLabel.Size = new System.Drawing.Size(45, 17);
            this.CityNameLabel.TabIndex = 3;
            this.CityNameLabel.Text = "Name";
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CancelButton.Image = global::LibraryController.Properties.Resources.checkin;
            this.CancelButton.Location = new System.Drawing.Point(203, 61);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(48, 48);
            this.CancelButton.TabIndex = 2;
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // UpdateCity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 120);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.CityNameLabel);
            this.Controls.Add(this.UpdateCityButton);
            this.Controls.Add(this.CityNameText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "UpdateCity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UpdateCity";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CityNameText;
        private System.Windows.Forms.Button UpdateCityButton;
        private System.Windows.Forms.Label CityNameLabel;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryDataAccessLayer;
using LibraryController.ForkoLibraryServiceReference;
using LibraryController.CustomClasses;

namespace LibraryController.City
{
    public partial class CityManager : Form
    {
        ForkoLibraryServiceClient dal;
        List<ForkoLibraryServiceReference.City> allCities;

        public CityManager()
        {
            dal = new ForkoLibraryServiceClient();
            allCities = dal.GetAllCities().ToList();
            InitializeComponent();
            SetupCityDataGridView();
            UIGod.StylizeDataGridView(CityDataGridView);
            LoadCityData();
            Localize();
        }

        private void Localize()
        {
            Text = MainForm.loc.cityManagerForm;
            CityDataGridView.Columns["CityName"].HeaderText = MainForm.loc.cityManagerName;
            toolTip1.SetToolTip(CreateCityButton, MainForm.loc.createCityButton);
            toolTip1.SetToolTip(UpdateCityButton, MainForm.loc.updateCityButton);
            toolTip1.SetToolTip(DeleteCityButton, MainForm.loc.deleteCityButton);
            toolTip1.SetToolTip(RefreshListButton, MainForm.loc.refreshCityButton);
        }

        private void SetupCityDataGridView()
        {
            //CityDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Sunken;
            //CityDataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
            //CityDataGridView.AutoGenerateColumns = false;
            //CityDataGridView.Columns.Clear();

            CityDataGridView.Columns.Add("CityName", "Name");
            CityDataGridView.Columns["CityName"].DataPropertyName = "Name";
            CityDataGridView.Columns["CityName"].CellTemplate.Style.Font = new Font(FontFamily.GenericSansSerif, 10f);
            //CityDataGridView.Columns["CityName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void LoadCityData()
        {
            int selectedIndex = 0;
            try
            {
                selectedIndex = CityDataGridView.SelectedRows[0].Index;
            }
            catch (ArgumentOutOfRangeException) { }

            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            var allCities = dal.GetAllCities().ToList();
            List<CustomClasses.City> allCitiesClient = new List<CustomClasses.City>();
            foreach (var city in allCities)
            {
                allCitiesClient.Add(Mapper.ToClient(city));
            }
            BindingList<CustomClasses.City> bindingCities = new BindingList<CustomClasses.City>(allCitiesClient);
            CityDataGridView.DataSource = bindingCities;

            CityDataGridView.ClearSelection();
            if (CityDataGridView.Rows.Count - 1 >= selectedIndex)
            {
                CityDataGridView.Rows[selectedIndex].Selected = true;
                CityDataGridView.CurrentCell = CityDataGridView.Rows[selectedIndex].Cells[0];
            }
            else if (CityDataGridView.Rows.Count > 0)
            {
                selectedIndex = CityDataGridView.Rows.Count - 1;
                CityDataGridView.Rows[selectedIndex].Selected = true;
                CityDataGridView.CurrentCell = CityDataGridView.Rows[selectedIndex].Cells[0];
            }
        }

        private void RefreshListButton_Click(object sender, EventArgs e)
        {
            LoadCityData();
        }

        private void CreateCityButton_Click(object sender, EventArgs e)
        {
            CreateCity createCity = new CreateCity();
            createCity.ShowDialog();
            LoadCityData();
        }

        private void UpdateCityButton_Click(object sender, EventArgs e)
        {
            if (CityDataGridView.SelectedRows.Count > 0)
            {
                UpdateCity updateCity = new UpdateCity((CustomClasses.City)CityDataGridView.SelectedRows[0].DataBoundItem);
                updateCity.ShowDialog();
            }
            LoadCityData();
        }

        private void DeleteCityButton_Click(object sender, EventArgs e)
        {
            if (CityDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
                var cityToDelete = dal.GetCity(((CustomClasses.City)CityDataGridView.SelectedRows[0].DataBoundItem).ID);
                var msg = MessageBox.Show(
                    $"{MainForm.loc.areYouSureYouWantToDelete}\n\"{cityToDelete.Name}\"?\n{MainForm.loc.processCanNotBeUndone}",
                    MainForm.loc.warning,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                    );

                if (msg == DialogResult.Yes)
                {
                    if (dal.DeleteCity(cityToDelete))
                    {
                        LoadCityData();
                    }
                    else
                    {
                        MessageBox.Show(MainForm.loc.unableToDeleteCity,
                            MainForm.loc.error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}

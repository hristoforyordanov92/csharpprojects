﻿using LibraryController.CustomClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.City
{
    public class CityValidations
    {
        private static bool Name(ref string name, CustomField customField)
        {
            Validations.TrimEmptySpaces(ref name);

            if (!Validations.MinSymbols(name, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(name, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            char[] nameToChar = name.ToCharArray();
            foreach (var @char in nameToChar)
            {
                if (!Validations.AllowedNameCharset(@char))
                {
                    MessageBox.Show(MainForm.loc.theFieldNamed + customField.Name + MainForm.loc.invalidSymbols + MainForm.loc.nameSymbolsInfo, MainForm.loc.incorrectData, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        public static bool Validate(ref string name, CityFields cityFields)
        {
            if (!Name(ref name, cityFields.name)) { return false; }

            return true;
        }
    }
}

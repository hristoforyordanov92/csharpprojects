﻿namespace LibraryController.City
{
    partial class CityManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CityManager));
            this.CityDataGridView = new System.Windows.Forms.DataGridView();
            this.CreateCityButton = new System.Windows.Forms.Button();
            this.UpdateCityButton = new System.Windows.Forms.Button();
            this.DeleteCityButton = new System.Windows.Forms.Button();
            this.RefreshListButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.CityDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // CityDataGridView
            // 
            this.CityDataGridView.AllowUserToAddRows = false;
            this.CityDataGridView.AllowUserToDeleteRows = false;
            this.CityDataGridView.AllowUserToResizeColumns = false;
            this.CityDataGridView.AllowUserToResizeRows = false;
            this.CityDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.CityDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.CityDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.CityDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CityDataGridView.GridColor = System.Drawing.Color.Black;
            this.CityDataGridView.Location = new System.Drawing.Point(13, 13);
            this.CityDataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.CityDataGridView.MultiSelect = false;
            this.CityDataGridView.Name = "CityDataGridView";
            this.CityDataGridView.ReadOnly = true;
            this.CityDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.CityDataGridView.RowHeadersVisible = false;
            this.CityDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.CityDataGridView.Size = new System.Drawing.Size(293, 344);
            this.CityDataGridView.StandardTab = true;
            this.CityDataGridView.TabIndex = 0;
            // 
            // CreateCityButton
            // 
            this.CreateCityButton.Image = global::LibraryController.Properties.Resources.addCity;
            this.CreateCityButton.Location = new System.Drawing.Point(13, 365);
            this.CreateCityButton.Margin = new System.Windows.Forms.Padding(4);
            this.CreateCityButton.Name = "CreateCityButton";
            this.CreateCityButton.Size = new System.Drawing.Size(48, 48);
            this.CreateCityButton.TabIndex = 1;
            this.CreateCityButton.UseVisualStyleBackColor = true;
            this.CreateCityButton.Click += new System.EventHandler(this.CreateCityButton_Click);
            // 
            // UpdateCityButton
            // 
            this.UpdateCityButton.Image = global::LibraryController.Properties.Resources.editEntry;
            this.UpdateCityButton.Location = new System.Drawing.Point(69, 365);
            this.UpdateCityButton.Margin = new System.Windows.Forms.Padding(4);
            this.UpdateCityButton.Name = "UpdateCityButton";
            this.UpdateCityButton.Size = new System.Drawing.Size(48, 48);
            this.UpdateCityButton.TabIndex = 2;
            this.UpdateCityButton.UseVisualStyleBackColor = true;
            this.UpdateCityButton.Click += new System.EventHandler(this.UpdateCityButton_Click);
            // 
            // DeleteCityButton
            // 
            this.DeleteCityButton.Image = global::LibraryController.Properties.Resources.deleteEntry;
            this.DeleteCityButton.Location = new System.Drawing.Point(258, 365);
            this.DeleteCityButton.Margin = new System.Windows.Forms.Padding(4);
            this.DeleteCityButton.Name = "DeleteCityButton";
            this.DeleteCityButton.Size = new System.Drawing.Size(48, 48);
            this.DeleteCityButton.TabIndex = 4;
            this.DeleteCityButton.UseVisualStyleBackColor = true;
            this.DeleteCityButton.Click += new System.EventHandler(this.DeleteCityButton_Click);
            // 
            // RefreshListButton
            // 
            this.RefreshListButton.Image = global::LibraryController.Properties.Resources.refreshList;
            this.RefreshListButton.Location = new System.Drawing.Point(125, 365);
            this.RefreshListButton.Margin = new System.Windows.Forms.Padding(4);
            this.RefreshListButton.Name = "RefreshListButton";
            this.RefreshListButton.Size = new System.Drawing.Size(48, 48);
            this.RefreshListButton.TabIndex = 3;
            this.RefreshListButton.UseVisualStyleBackColor = true;
            this.RefreshListButton.Click += new System.EventHandler(this.RefreshListButton_Click);
            // 
            // CityManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 426);
            this.Controls.Add(this.RefreshListButton);
            this.Controls.Add(this.DeleteCityButton);
            this.Controls.Add(this.UpdateCityButton);
            this.Controls.Add(this.CreateCityButton);
            this.Controls.Add(this.CityDataGridView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "CityManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CityManager";
            ((System.ComponentModel.ISupportInitialize)(this.CityDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView CityDataGridView;
        private System.Windows.Forms.Button CreateCityButton;
        private System.Windows.Forms.Button UpdateCityButton;
        private System.Windows.Forms.Button DeleteCityButton;
        private System.Windows.Forms.Button RefreshListButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
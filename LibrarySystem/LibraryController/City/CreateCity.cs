﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLibrary;
using LibraryController.ForkoLibraryServiceReference;

namespace LibraryController.City
{
    public partial class CreateCity : Form
    {
        ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
        ForkoLibraryServiceReference.City city = new ForkoLibraryServiceReference.City();

        private CityFields cityFields;

        private string name;

        public CreateCity()
        {
            InitializeComponent();

            cityFields = new CityFields();

            Localize();
        }

        private void Localize()
        {
            Text = MainForm.loc.createCityForm;
            toolTip1.SetToolTip(CreateCityButton, MainForm.loc.createCityButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            CityNameLabel.Text = MainForm.loc.cityManagerName;
        }

        private void CreateCityButton_Click(object sender, EventArgs e)
        {
            name = CityNameText.Text;

            if (CityValidations.Validate(ref name, cityFields))
            {
                city.Name = name;
                dal.CreateCity(city);
                Close();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
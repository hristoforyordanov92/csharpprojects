﻿using BusinessLibrary;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using LibraryController.ForkoLibraryServiceReference;

namespace LibraryController
{
    public partial class MainForm : Form
    {
        public static CustomLocalization loc = CustomLocalization.GetEnglishLocalization();

        public MainForm()
        {
            InitializeComponent();

            //Setup Data Grids
            SetupAuthorDataGridView();
            SetupBookDataGridView();
            SetupUserDataGridView();

            //Stylize DataGridViews
            UIGod.StylizeDataGridView(BookDataGridView);
            UIGod.StylizeDataGridView(AuthorDataGridView);
            UIGod.StylizeDataGridView(UserDataGridView);

            //Load data in Data Grids
            LoadAuthorData();
            LoadBookData();
            LoadUserData();

            SetupChart();
            SetupChartDateComboBox();
            LoadChartData();

            Localize();
        }

        private void TranslateToEnglish()
        {
            loc = CustomLocalization.GetEnglishLocalization();
            Localize();
        }
        private void TranslateToBulgarian()
        {
            loc = CustomLocalization.GetBulgarianLocalization();
            Localize();
        }
        private void Localize()
        {
            settingsToolStripMenuItem.Text = loc.menuStripSettings;
            languageToolStripMenuItem.Text = loc.menuStripLanguage;
            managerToolStripMenuItem.Text = loc.menuStripManager;
            citiesToolStripMenuItem.Text = loc.menuStripCities;

            this.Text = loc.mainForm;

            //Book
            BookTab.Text = loc.bookTab;

            toolTip.SetToolTip(CreateBookButton, loc.createBookButton);
            toolTip.SetToolTip(UpdateBookButton, loc.updateBookButton);
            toolTip.SetToolTip(DeleteBookButton, loc.deleteBookButton);
            toolTip.SetToolTip(RefreshBookButton, loc.refreshBookButton);

            //CreateBookButton. = loc.createBookButton;
            //UpdateBookButton.Text = loc.updateBookButton;
            //DeleteBookButton.Text = loc.deleteBookButton;
            //RefreshBookButton.Text = loc.refreshBookButton;

            BookDataGridView.Columns["BookTitle"].HeaderText = loc.bookColumnTitle;
            BookDataGridView.Columns["BookUniqueNumber"].HeaderText = loc.bookColumnUniqueNumber;
            BookDataGridView.Columns["BookPageCount"].HeaderText = loc.bookColumnPageCount;
            BookDataGridView.Columns["BookPublishDate"].HeaderText = loc.bookColumnPublishDate;
            BookDataGridView.Columns["BookIsAvailable"].HeaderText = loc.bookColumnAvailable;
            BookDataGridView.Columns["BookFromDate"].HeaderText = loc.bookColumnFromDate;
            BookDataGridView.Columns["BookToDate"].HeaderText = loc.bookColumnToDate;

            //Author
            AuthorTab.Text = loc.authorTab;

            toolTip.SetToolTip(CreateAuthorButton, loc.createAuthorButton);
            toolTip.SetToolTip(UpdateAuthorButton, loc.updateAuthorButton);
            toolTip.SetToolTip(DeleteAuthorButton, loc.deleteAuthorButton);
            toolTip.SetToolTip(RefreshAuthorButton, loc.refreshAuthorButton);

            //CreateAuthorButton.Text = loc.createAuthorButton;
            //UpdateAuthorButton.Text = loc.updateAuthorButton;
            //DeleteAuthorButton.Text = loc.deleteAuthorButton;
            //RefreshAuthorButton.Text = loc.refreshAuthorButton;

            AuthorDataGridView.Columns["AuthorFirstName"].HeaderText = loc.authorColumnFirstName;
            AuthorDataGridView.Columns["AuthorLastName"].HeaderText = loc.authorColumnLastName;
            AuthorDataGridView.Columns["AuthorLocalizedSex"].HeaderText = loc.columnSex;
            AuthorDataGridView.Columns["AuthorBirthDate"].HeaderText = loc.authorColumnBirthDate;

            //User
            UserTab.Text = loc.userTab;

            toolTip.SetToolTip(CreateUserButton, loc.createUserButton);
            toolTip.SetToolTip(UpdateUserButton, loc.updateUserButton);
            toolTip.SetToolTip(DeleteUserButton, loc.deleteUserButton);
            toolTip.SetToolTip(RefreshUserButton, loc.refreshUserButton);
            toolTip.SetToolTip(CheckOutButton, loc.checkOutButton);
            toolTip.SetToolTip(CheckInButton, loc.checkInButton);

            //CreateUserButton.Text = loc.createUserButton;
            //UpdateUserButton.Text = loc.updateUserButton;
            //DeleteUserButton.Text = loc.deleteUserButton;
            //RefreshUserButton.Text = loc.refreshUserButton;
            //CheckOutButton.Text = loc.checkOutButton;
            //CheckInButton.Text = loc.checkInButton;

            UserDataGridView.Columns["UserFirstName"].HeaderText = loc.userColumnFirstName;
            UserDataGridView.Columns["UserLastName"].HeaderText = loc.userColumnLastName;
            UserDataGridView.Columns["UserIDCard"].HeaderText = loc.userColumnIDCard;
            UserDataGridView.Columns["UserEGN"].HeaderText = loc.userColumnEGN;
            UserDataGridView.Columns["UserSex"].HeaderText = loc.columnSex;
            UserDataGridView.Columns["UserPhone"].HeaderText = loc.userColumnPhone;
            UserDataGridView.Columns["UserEMail"].HeaderText = loc.userColumnEMail;
            UserDataGridView.Columns["UserAddress"].HeaderText = loc.userColumnAddress;
            UserDataGridView.Columns["UserCity"].HeaderText = loc.userColumnCity;

            StatisticsTab.Text = loc.statistics;

            Chart.Series["Books"].LegendText = loc.statInformationLabel;
            for (int i = 0; i < Chart.Series["Books"].Points.Count; i++)
            {
                Chart.Series["Books"].Points[i].AxisLabel = $"{loc.months[i].Substring(0, 3)}";
            }
            ChartDateLabel.Text = loc.statYear;

            LoadBookData();
            LoadAuthorData();
            LoadUserData();
            LoadChartData();
        }

        //public static void LoadLocalization(string localizationPath)
        //{
        //    return;
        //}
        //public void Serialize()
        //{
        //    if (!Directory.Exists("Localizations"))
        //    {
        //        Directory.CreateDirectory("Localizations");
        //    }

        //    var serializedData = JsonConvert.SerializeObject(loc, Formatting.Indented);
        //    File.WriteAllText("Localizations\\defaultEnglish.loc", serializedData);
        //}
        //private void Deserialize()
        //{
        //    var reader = File.ReadAllText(fullFilePath);
        //    T returnedObject = (T)JsonConvert.DeserializeObject(reader, typeof(T));

        //    return returnedObject;
        //}

        //Book functionality

        private void SetupBookDataGridView()
        {
            BookDataGridView.Columns.Clear();
            BookDataGridView.AutoGenerateColumns = false;

            BookDataGridView.Columns.Add("BookTitle", "Title");
            BookDataGridView.Columns["BookTitle"].DataPropertyName = "Title";

            BookDataGridView.Columns.Add("BookUniqueNumber", "Serial number");
            BookDataGridView.Columns["BookUniqueNumber"].DataPropertyName = "UniqueNumber";

            BookDataGridView.Columns.Add("BookPageCount", "Number of pages");
            BookDataGridView.Columns["BookPageCount"].DataPropertyName = "PageCount";

            BookDataGridView.Columns.Add("BookPublishDate", "Date published");
            BookDataGridView.Columns["BookPublishDate"].DataPropertyName = "PublishDate";
            BookDataGridView.Columns["BookPublishDate"].CellTemplate.Style.Format = "dd/MM/yyyy";

            BookDataGridView.Columns.Add("BookIsAvailable", "Available");
            BookDataGridView.Columns["BookIsAvailable"].DataPropertyName = "LocalizedIsAvailable";

            BookDataGridView.Columns.Add("BookFromDate", "Unavailable since");
            BookDataGridView.Columns["BookFromDate"].DataPropertyName = "FromDate";
            BookDataGridView.Columns["BookFromDate"].CellTemplate.Style.Format = "dd/MM/yyyy";

            BookDataGridView.Columns.Add("BookToDate", "Unavailable until");
            BookDataGridView.Columns["BookToDate"].DataPropertyName = "ToDate";
            BookDataGridView.Columns["BookToDate"].CellTemplate.Style.Format = "dd/MM/yyyy";
        }
        private void LoadBookData()
        {
            int selectedIndex = 0;
            try
            {
                selectedIndex = BookDataGridView.SelectedRows[0].Index;
            }
            catch (ArgumentOutOfRangeException) { }

            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            List<ForkoLibraryServiceReference.Book> books = dal.GetAllBooks().ToList();
            List<ForkoLibraryServiceReference.Checkout> checkouts = dal.GetAllCheckouts().ToList();
            List<CustomClasses.Book> clientBooks = new List<CustomClasses.Book>();

            //var joinedBooks = books.GroupJoin(dal.GetAllCheckout(),
            //                                  b => b.ID,
            //                                  c => c.BookID,
            //                                  (b, c) => new { Book = b, Checkout = c.DefaultIfEmpty() });

            var joinedBooks = (from b in books
                               join c in checkouts
                               on b.ID equals c.BookID into newGroup
                               from groupItem in newGroup.DefaultIfEmpty()
                               select new { Book = b, Checkout = groupItem }).ToList();

            foreach (var item in joinedBooks)
            {
                var newBook = new CustomClasses.Book
                {
                    ID = item.Book.ID,
                    Title = item.Book.Title,
                    UniqueNumber = item.Book.UniqueNumber,
                    PageCount = item.Book.PageCount,
                    PublishDate = item.Book.PublishDate,
                    AuthorID = item.Book.AuthorID,
                };

                if (item.Checkout != null)
                {
                    newBook.IsAvailable = 0;
                    newBook.FromDate = item.Checkout.FromDate;
                    newBook.ToDate = item.Checkout.ToDate;
                }
                else
                {
                    newBook.IsAvailable = 1;
                    newBook.FromDate = null;
                    newBook.ToDate = null;
                }

                clientBooks.Add(newBook);
            }

            BookDataGridView.DataSource = new BindingList<CustomClasses.Book>(clientBooks.OrderBy(x => x.Title).ToList());

            BookDataGridView.ClearSelection();
            if (BookDataGridView.Rows.Count - 1 >= selectedIndex)
            {
                BookDataGridView.Rows[selectedIndex].Selected = true;
                BookDataGridView.CurrentCell = BookDataGridView.Rows[selectedIndex].Cells[1];
            }
            else if (BookDataGridView.Rows.Count > 0)
            {
                selectedIndex = BookDataGridView.Rows.Count - 1;
                BookDataGridView.Rows[selectedIndex].Selected = true;
                BookDataGridView.CurrentCell = BookDataGridView.Rows[selectedIndex].Cells[1];
            }
        }
        private void CreateBookButton_Click(object sender, EventArgs e)
        {
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            if (dal.GetAllAuthors().ToList().Count > 0)
            {
                Book.CreateBook createBookForm = new Book.CreateBook();
                createBookForm.ShowDialog();
            }
            else
            {
                MessageBox.Show(loc.unableToCreateBookMissingAuthors, loc.error, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LoadBookData();
        }
        private void UpdateBookButton_Click(object sender, EventArgs e)
        {
            if (BookDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
                var book = dal.GetBook(((CustomClasses.Book)BookDataGridView.SelectedRows[0].DataBoundItem).ID);
                Book.UpdateBook updateBookForm = new Book.UpdateBook(book);
                updateBookForm.ShowDialog();
            }
            LoadBookData();
        }
        private void DeleteBookButton_Click(object sender, EventArgs e)
        {
            if (BookDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();

                var bookToDelete = dal.GetBook(((CustomClasses.Book)BookDataGridView.SelectedRows[0].DataBoundItem).ID);
                var msg = MessageBox.Show(
                    $"{loc.areYouSureYouWantToDelete}\n\"{bookToDelete.Title}\"?\n{loc.processCanNotBeUndone}",
                    loc.warning,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                    );

                if (msg == DialogResult.Yes)
                {
                    if (dal.DeleteBook(bookToDelete))
                    {
                        LoadBookData();
                    }
                    else
                    {
                        MessageBox.Show(loc.unableToDeleteBook,
                            loc.error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void RefreshBookButton_Click(object sender, EventArgs e)
        {
            LoadBookData();
        }

        //Author functionality
        private void SetupAuthorDataGridView()
        {
            AuthorDataGridView.AutoGenerateColumns = false;
            AuthorDataGridView.Columns.Clear();

            AuthorDataGridView.Columns.Add("AuthorID", "ID");
            AuthorDataGridView.Columns["AuthorID"].DataPropertyName = "ID";
            AuthorDataGridView.Columns["AuthorID"].Visible = false;

            AuthorDataGridView.Columns.Add("AuthorFirstName", "FirstName");
            AuthorDataGridView.Columns["AuthorFirstName"].DataPropertyName = "FirstName";

            AuthorDataGridView.Columns.Add("AuthorLastName", "LastName");
            AuthorDataGridView.Columns["AuthorLastName"].DataPropertyName = "LastName";

            AuthorDataGridView.Columns.Add("AuthorSex", "Sex");
            AuthorDataGridView.Columns["AuthorSex"].DataPropertyName = "Sex";
            AuthorDataGridView.Columns["AuthorSex"].Visible = false;

            AuthorDataGridView.Columns.Add("AuthorLocalizedSex", "Sex");
            AuthorDataGridView.Columns["AuthorLocalizedSex"].DataPropertyName = "LocalizedSex";
            AuthorDataGridView.Columns["AuthorLocalizedSex"].FillWeight = 50;

            AuthorDataGridView.Columns.Add("AuthorBirthDate", "BirthDate");
            AuthorDataGridView.Columns["AuthorBirthDate"].DataPropertyName = "BirthDate";
            AuthorDataGridView.Columns["AuthorBirthDate"].CellTemplate.Style.Format = "dd/MM/yyyy";
        }
        private void LoadAuthorData()
        {
            int selectedIndex = 0;
            try
            {
                selectedIndex = AuthorDataGridView.SelectedRows[0].Index;
            }
            catch (ArgumentOutOfRangeException)
            {

            }

            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();

            //var authors = dal.GetAllAuthors();
            var authors = dal.GetAllAuthors();
            List<CustomClasses.Author> customAuthors = new List<CustomClasses.Author>();
            foreach (var author in authors)
            {
                customAuthors.Add(new CustomClasses.Author
                {
                    ID = author.ID,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                    Sex = author.Sex,
                    BirthDate = author.BirthDate
                });
            }

            BindingList<CustomClasses.Author> bindingAuthors = new BindingList<CustomClasses.Author>(customAuthors);

            AuthorDataGridView.DataSource = bindingAuthors;


            AuthorDataGridView.ClearSelection();
            if (AuthorDataGridView.Rows.Count - 1 >= selectedIndex)
            {
                AuthorDataGridView.Rows[selectedIndex].Selected = true;
                AuthorDataGridView.CurrentCell = AuthorDataGridView.Rows[selectedIndex].Cells[1];
            }
            else if (AuthorDataGridView.Rows.Count > 0)
            {
                selectedIndex = AuthorDataGridView.Rows.Count - 1;
                AuthorDataGridView.Rows[selectedIndex].Selected = true;
                AuthorDataGridView.CurrentCell = AuthorDataGridView.Rows[selectedIndex].Cells[1];
            }
        }
        private void CreateAuthorButton_Click(object sender, EventArgs e)
        {
            Author.CreateAuthorForm createAuthorForm = new Author.CreateAuthorForm();
            createAuthorForm.ShowDialog();
            LoadAuthorData();
        }
        private void UpdateAuthorButton_Click(object sender, EventArgs e)
        {
            if (AuthorDataGridView.SelectedRows.Count > 0)
            {
                var author = CustomClasses.Mapper.ToBusiness((CustomClasses.Author)AuthorDataGridView.SelectedRows[0].DataBoundItem);
                Author.UpdateAuthorForm updateAuthorForm = new Author.UpdateAuthorForm(author);
                updateAuthorForm.ShowDialog();
            }
            LoadAuthorData();
        }
        private void DeleteAuthorButton_Click(object sender, EventArgs e)
        {
            if (AuthorDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();



                var authorToDelete = dal.GetAuthor(((CustomClasses.Author)AuthorDataGridView.SelectedRows[0].DataBoundItem).ID);
                var msg = MessageBox.Show(
                    $"{loc.areYouSureYouWantToDelete}\n\"{authorToDelete.FirstName} {authorToDelete.LastName}\"?\n{loc.processCanNotBeUndone}",
                    loc.warning,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                    );

                if (msg == DialogResult.Yes)
                {
                    if (dal.DeleteAuthor(authorToDelete))
                    {
                        LoadAuthorData();
                    }
                    else
                    {
                        MessageBox.Show(loc.unableToDeleteAuthor,
                            loc.error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void RefreshAuthorButton_Click(object sender, EventArgs e)
        {
            LoadAuthorData();
        }

        //User funtionality
        private void SetupUserDataGridView()
        {
            UserDataGridView.AutoGenerateColumns = false;
            UserDataGridView.Columns.Clear();

            UserDataGridView.Columns.Add("UserID", "ID");
            UserDataGridView.Columns["UserID"].DataPropertyName = "ID";
            UserDataGridView.Columns["UserID"].Visible = false;

            UserDataGridView.Columns.Add("UserFirstName", "FirstName");
            UserDataGridView.Columns["UserFirstName"].DataPropertyName = "FirstName";

            UserDataGridView.Columns.Add("UserLastName", "LastName");
            UserDataGridView.Columns["UserLastName"].DataPropertyName = "LastName";

            UserDataGridView.Columns.Add("UserIDCard", "IDCard");
            UserDataGridView.Columns["UserIDCard"].DataPropertyName = "IDCard";
            UserDataGridView.Columns["UserIDCard"].FillWeight = 70;

            UserDataGridView.Columns.Add("UserEGN", "EGN");
            UserDataGridView.Columns["UserEGN"].DataPropertyName = "EGN";
            UserDataGridView.Columns["UserEGN"].FillWeight = 60;

            UserDataGridView.Columns.Add("UserSex", "Sex");
            UserDataGridView.Columns["UserSex"].DataPropertyName = "LocalizedSex";
            UserDataGridView.Columns["UserSex"].FillWeight = 50;

            UserDataGridView.Columns.Add("UserPhone", "Phone");
            UserDataGridView.Columns["UserPhone"].DataPropertyName = "Phone";

            UserDataGridView.Columns.Add("UserEMail", "EMail");
            UserDataGridView.Columns["UserEMail"].DataPropertyName = "EMail";

            UserDataGridView.Columns.Add("UserAddress", "Address");
            UserDataGridView.Columns["UserAddress"].DataPropertyName = "Address";

            UserDataGridView.Columns.Add("UserCity", "City");
            UserDataGridView.Columns["UserCity"].DataPropertyName = "City";
            UserDataGridView.Columns["UserCity"].FillWeight = 60;
        }
        private void LoadUserData()
        {
            int selectedIndex = 0;
            try
            {
                selectedIndex = UserDataGridView.SelectedRows[0].Index;
            }
            catch (ArgumentOutOfRangeException) { }

            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            var allUsers = (from user in dal.GetAllUsers()
                            join city in dal.GetAllCities()
                            on user.CityID equals city.ID
                            select new CustomClasses.User
                            {
                                ID = user.ID,
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                IDCard = user.IDCard,
                                EGN = user.EGN,
                                Phone = user.Phone,
                                Sex = user.Sex,
                                EMail = user.EMail,
                                Address = user.Address,
                                City = city.Name
                            }).ToList();

            BindingList<CustomClasses.User> bindingTakers = new BindingList<CustomClasses.User>(allUsers);
            UserDataGridView.DataSource = bindingTakers;

            UserDataGridView.ClearSelection();
            if (UserDataGridView.Rows.Count - 1 >= selectedIndex)
            {
                UserDataGridView.Rows[selectedIndex].Selected = true;
                UserDataGridView.CurrentCell = UserDataGridView.Rows[selectedIndex].Cells[1];
            }
            else if (UserDataGridView.Rows.Count > 0)
            {
                selectedIndex = UserDataGridView.Rows.Count - 1;
                UserDataGridView.Rows[selectedIndex].Selected = true;
                UserDataGridView.CurrentCell = UserDataGridView.Rows[selectedIndex].Cells[1];
            }
        }
        private void CreateUserButton_Click(object sender, EventArgs e)
        {
            User.CreateUserForm newForm = new User.CreateUserForm();
            newForm.ShowDialog();
            LoadUserData();
        }
        private void UpdateUserButton_Click(object sender, EventArgs e)
        {
            if (UserDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
                var user = dal.GetUser(((CustomClasses.User)UserDataGridView.SelectedRows[0].DataBoundItem).ID);
                User.UpdateUserForm newForm = new User.UpdateUserForm(user);
                newForm.ShowDialog();
            }
            LoadUserData();
        }
        private void DeleteUserButton_Click(object sender, EventArgs e)
        {
            if (UserDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();

                var userToDelete = dal.GetUser(((CustomClasses.User)UserDataGridView.SelectedRows[0].DataBoundItem).ID);
                var msg = MessageBox.Show(
                    $"{loc.areYouSureYouWantToDelete}\n\"{userToDelete.FirstName} {userToDelete.LastName}\"?\n{loc.processCanNotBeUndone}",
                    loc.warning,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                    );

                if (msg == DialogResult.Yes)
                {
                    if (dal.DeleteUser(userToDelete))
                    {
                        LoadUserData();
                    }
                    else
                    {
                        MessageBox.Show(loc.unableToDeleteUser,
                            loc.error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void RefreshUserButton_Click(object sender, EventArgs e)
        {
            LoadUserData();
        }

        //Checkout functionality
        private void CheckOutButton_Click(object sender, EventArgs e)
        {
            if (UserDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
                var user = dal.GetUser(((CustomClasses.User)UserDataGridView.SelectedRows[0].DataBoundItem).ID);
                CheckInOut.CheckOutForm newForm = new CheckInOut.CheckOutForm(user);
                newForm.ShowDialog();
            }
            LoadUserData();
        }
        private void CheckInButton_Click(object sender, EventArgs e)
        {
            if (UserDataGridView.SelectedRows.Count > 0)
            {
                ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
                var user = dal.GetUser(((CustomClasses.User)UserDataGridView.SelectedRows[0].DataBoundItem).ID);
                CheckInOut.CheckInForm newForm = new CheckInOut.CheckInForm(user);
                newForm.ShowDialog();
            }
            LoadUserData();
        }

        //ToolStripMenu functionality
        private void EnglishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TranslateToEnglish();
        }
        private void BulgarianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TranslateToBulgarian();
        }

        private void citiesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void citiesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LibraryController.City.CityManager cityManager = new City.CityManager();
            cityManager.ShowDialog();
        }

        private void managerToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void SetupChart()
        {
            Chart.Series.Clear();
            Chart.ChartAreas[0].AxisX.Interval = 1d;
            Chart.Series.Add("Books");
            Chart.Series["Books"].IsValueShownAsLabel = false;
            Chart.Series["Books"].IsVisibleInLegend = false;
            Chart.Series["Books"].YValuesPerPoint = 1;
            Chart.Series["Books"].IsXValueIndexed = true;
            Chart.Series["Books"].IsVisibleInLegend = true;
            Chart.Series["Books"].LegendText = loc.statInformationLabel;
            Chart.Legends.Add("Legend");
            Chart.Legends["Legend"].Font = new Font(FontFamily.GenericSansSerif, 10f);
            Chart.Legends["Legend"].TextWrapThreshold = 10;
            Chart.Series["Books"].Legend = "Legend";
            Chart.Series["Books"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            Chart.Series["Books"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            Chart.Series["Books"].CustomProperties = "PixelPointWidth=20, EmptyPointValue=Zero, PointWidth=1, LabelStyle=Top";

            Chart.Series["Books"].Points.Clear();
            for (int i = 0; i < loc.months.Length; i++)
            {
                Chart.Series["Books"].Points.Add(new DataPoint());
                Chart.Series["Books"].Points[i].AxisLabel = $"{loc.months[i].Substring(0, 3)}";
                Chart.Series["Books"].Points[i].Font = new Font(FontFamily.GenericSansSerif, 10f);
                Chart.Series["Books"].Points[i].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None;
                Chart.Series["Books"].Points[i].IsValueShownAsLabel = true;
            }
        }
        private void LoadChartData()
        {
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            var booksPerMonth = dal.GetAllBooksPerMonth().Where(x => x.Date.Year == (int)ChartDateComboBox.SelectedItem).ToList();
            int[] months = new int[12];
            foreach (var stat in booksPerMonth)
            {
                months[stat.Date.Month - 1]++;
            }

            if (booksPerMonth != null)
            {
                Chart.Series["Books"].Points.Clear();
                for (int i = 0; i < loc.months.Length; i++)
                {
                    Chart.Series["Books"].Points.AddXY(loc.months[i].Substring(0, 3), months[i]);
                    //Chart.Series["Books"].Points[i].AxisLabel = $"{loc.months[i].Substring(0, 3)}";
                    Chart.Series["Books"].Points[i].Font = new Font(FontFamily.GenericSansSerif, 10f);
                    Chart.Series["Books"].Points[i].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None;
                    Chart.Series["Books"].Points[i].IsValueShownAsLabel = true;
                }
            }

            //var checkouts = dal.GetAllCheckout(true).Where(x => x.FromDate.Year == (int)ChartDateComboBox.SelectedItem);

            Chart.Series["Books"].Points.ResumeUpdates();
            for (int i = 0; i < Chart.Series["Books"].Points.Count; i++)
            {
                //var books = checkouts.Where(x => x.FromDate.Month == i + 1).Count();
                Chart.Series["Books"].Points[i].SetValueY(months[i]);
            }
            //Chart.Series["Books"].Points.SuspendUpdates();
        }
        private void SetupChartDateComboBox()
        {
            ChartDateComboBox.Items.Clear();
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();

            ChartDateComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            var booksPerMonth = dal.GetAllBooksPerMonth();
            int minYear;
            int maxYear;
            try
            {
                minYear = booksPerMonth.Select(x => x.Date.Year).Min();
                maxYear = booksPerMonth.Select(x => x.Date.Year).Max();
            }
            catch (InvalidOperationException)
            {
                minYear = DateTime.Now.Year;
                maxYear = DateTime.Now.Year;
            }

            for (int i = minYear; i <= maxYear; i++)
            {
                ChartDateComboBox.Items.Add(i);
            }
            if (ChartDateComboBox.Items.Count == 0)
            {
                ChartDateComboBox.Items.Add(DateTime.Now.Year);
            }
            ChartDateComboBox.SelectedIndex = 0;
            ChartDateComboBox.SelectedIndexChanged += ChartDateComboBox_SelectedIndexChanged;
        }
        private void ChartDateComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadChartData();
        }

        private void StatisticsTab_Click(object sender, EventArgs e)
        {
            LoadChartData();
        }

        private void RefreshStatisticsButton_Click(object sender, EventArgs e)
        {
            LoadChartData();
        }

        private void RefreshEverything(object sender, EventArgs e)
        {
            SetupChartDateComboBox();
            LoadChartData();
        }
    }
}
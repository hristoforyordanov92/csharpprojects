﻿namespace LibraryController.User
{
    partial class UpdateUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateUserForm));
            this.UpdateTakerButton = new System.Windows.Forms.Button();
            this.CityComboBox = new System.Windows.Forms.ComboBox();
            this.SexComboBox = new System.Windows.Forms.ComboBox();
            this.CityLabel = new System.Windows.Forms.Label();
            this.AddressLabel = new System.Windows.Forms.Label();
            this.SexLabel = new System.Windows.Forms.Label();
            this.EMailLabel = new System.Windows.Forms.Label();
            this.PhoneNumberLabel = new System.Windows.Forms.Label();
            this.EGNLabel = new System.Windows.Forms.Label();
            this.IDCardLabel = new System.Windows.Forms.Label();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.AddressText = new System.Windows.Forms.TextBox();
            this.EMailText = new System.Windows.Forms.TextBox();
            this.PhoneNumberText = new System.Windows.Forms.TextBox();
            this.EGNText = new System.Windows.Forms.TextBox();
            this.IDCardText = new System.Windows.Forms.TextBox();
            this.LastNameText = new System.Windows.Forms.TextBox();
            this.FirstNameText = new System.Windows.Forms.TextBox();
            this.CancelButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // UpdateTakerButton
            // 
            this.UpdateTakerButton.Image = global::LibraryController.Properties.Resources.editEntry;
            this.UpdateTakerButton.Location = new System.Drawing.Point(13, 293);
            this.UpdateTakerButton.Margin = new System.Windows.Forms.Padding(4);
            this.UpdateTakerButton.Name = "UpdateTakerButton";
            this.UpdateTakerButton.Size = new System.Drawing.Size(48, 48);
            this.UpdateTakerButton.TabIndex = 9;
            this.UpdateTakerButton.UseVisualStyleBackColor = true;
            this.UpdateTakerButton.Click += new System.EventHandler(this.UpdateTakerButton_Click);
            // 
            // CityComboBox
            // 
            this.CityComboBox.FormattingEnabled = true;
            this.CityComboBox.Location = new System.Drawing.Point(227, 238);
            this.CityComboBox.Name = "CityComboBox";
            this.CityComboBox.Size = new System.Drawing.Size(183, 24);
            this.CityComboBox.TabIndex = 8;
            // 
            // SexComboBox
            // 
            this.SexComboBox.FormattingEnabled = true;
            this.SexComboBox.Location = new System.Drawing.Point(310, 124);
            this.SexComboBox.Name = "SexComboBox";
            this.SexComboBox.Size = new System.Drawing.Size(100, 24);
            this.SexComboBox.TabIndex = 4;
            // 
            // CityLabel
            // 
            this.CityLabel.AutoSize = true;
            this.CityLabel.Location = new System.Drawing.Point(16, 241);
            this.CityLabel.Name = "CityLabel";
            this.CityLabel.Size = new System.Drawing.Size(30, 16);
            this.CityLabel.TabIndex = 11;
            this.CityLabel.Text = "City";
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(16, 213);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(59, 16);
            this.AddressLabel.TabIndex = 12;
            this.AddressLabel.Text = "Address";
            // 
            // SexLabel
            // 
            this.SexLabel.AutoSize = true;
            this.SexLabel.Location = new System.Drawing.Point(16, 127);
            this.SexLabel.Name = "SexLabel";
            this.SexLabel.Size = new System.Drawing.Size(31, 16);
            this.SexLabel.TabIndex = 15;
            this.SexLabel.Text = "Sex";
            // 
            // EMailLabel
            // 
            this.EMailLabel.AutoSize = true;
            this.EMailLabel.Location = new System.Drawing.Point(16, 185);
            this.EMailLabel.Name = "EMailLabel";
            this.EMailLabel.Size = new System.Drawing.Size(46, 16);
            this.EMailLabel.TabIndex = 13;
            this.EMailLabel.Text = "E-mail";
            // 
            // PhoneNumberLabel
            // 
            this.PhoneNumberLabel.AutoSize = true;
            this.PhoneNumberLabel.Location = new System.Drawing.Point(16, 157);
            this.PhoneNumberLabel.Name = "PhoneNumberLabel";
            this.PhoneNumberLabel.Size = new System.Drawing.Size(95, 16);
            this.PhoneNumberLabel.TabIndex = 14;
            this.PhoneNumberLabel.Text = "Phone number";
            // 
            // EGNLabel
            // 
            this.EGNLabel.AutoSize = true;
            this.EGNLabel.Location = new System.Drawing.Point(16, 99);
            this.EGNLabel.Name = "EGNLabel";
            this.EGNLabel.Size = new System.Drawing.Size(37, 16);
            this.EGNLabel.TabIndex = 16;
            this.EGNLabel.Text = "EGN";
            // 
            // IDCardLabel
            // 
            this.IDCardLabel.AutoSize = true;
            this.IDCardLabel.Location = new System.Drawing.Point(16, 71);
            this.IDCardLabel.Name = "IDCardLabel";
            this.IDCardLabel.Size = new System.Drawing.Size(51, 16);
            this.IDCardLabel.TabIndex = 17;
            this.IDCardLabel.Text = "ID card";
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Location = new System.Drawing.Point(16, 43);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(70, 16);
            this.LastNameLabel.TabIndex = 18;
            this.LastNameLabel.Text = "Last name";
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Location = new System.Drawing.Point(16, 15);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(70, 16);
            this.FirstNameLabel.TabIndex = 19;
            this.FirstNameLabel.Text = "First name";
            // 
            // AddressText
            // 
            this.AddressText.Location = new System.Drawing.Point(144, 210);
            this.AddressText.Name = "AddressText";
            this.AddressText.Size = new System.Drawing.Size(266, 22);
            this.AddressText.TabIndex = 7;
            // 
            // EMailText
            // 
            this.EMailText.Location = new System.Drawing.Point(188, 182);
            this.EMailText.Name = "EMailText";
            this.EMailText.Size = new System.Drawing.Size(222, 22);
            this.EMailText.TabIndex = 6;
            // 
            // PhoneNumberText
            // 
            this.PhoneNumberText.Location = new System.Drawing.Point(188, 154);
            this.PhoneNumberText.Name = "PhoneNumberText";
            this.PhoneNumberText.Size = new System.Drawing.Size(222, 22);
            this.PhoneNumberText.TabIndex = 5;
            // 
            // EGNText
            // 
            this.EGNText.Location = new System.Drawing.Point(227, 96);
            this.EGNText.Name = "EGNText";
            this.EGNText.Size = new System.Drawing.Size(183, 22);
            this.EGNText.TabIndex = 3;
            // 
            // IDCardText
            // 
            this.IDCardText.Location = new System.Drawing.Point(227, 68);
            this.IDCardText.Name = "IDCardText";
            this.IDCardText.Size = new System.Drawing.Size(183, 22);
            this.IDCardText.TabIndex = 2;
            // 
            // LastNameText
            // 
            this.LastNameText.Location = new System.Drawing.Point(144, 40);
            this.LastNameText.Name = "LastNameText";
            this.LastNameText.Size = new System.Drawing.Size(266, 22);
            this.LastNameText.TabIndex = 1;
            // 
            // FirstNameText
            // 
            this.FirstNameText.Location = new System.Drawing.Point(144, 12);
            this.FirstNameText.Name = "FirstNameText";
            this.FirstNameText.Size = new System.Drawing.Size(266, 22);
            this.FirstNameText.TabIndex = 0;
            // 
            // CancelButton
            // 
            this.CancelButton.Image = global::LibraryController.Properties.Resources.checkin;
            this.CancelButton.Location = new System.Drawing.Point(362, 294);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(48, 48);
            this.CancelButton.TabIndex = 10;
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // UpdateUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 355);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.CityComboBox);
            this.Controls.Add(this.SexComboBox);
            this.Controls.Add(this.CityLabel);
            this.Controls.Add(this.AddressLabel);
            this.Controls.Add(this.SexLabel);
            this.Controls.Add(this.EMailLabel);
            this.Controls.Add(this.PhoneNumberLabel);
            this.Controls.Add(this.EGNLabel);
            this.Controls.Add(this.IDCardLabel);
            this.Controls.Add(this.LastNameLabel);
            this.Controls.Add(this.FirstNameLabel);
            this.Controls.Add(this.AddressText);
            this.Controls.Add(this.EMailText);
            this.Controls.Add(this.PhoneNumberText);
            this.Controls.Add(this.EGNText);
            this.Controls.Add(this.IDCardText);
            this.Controls.Add(this.LastNameText);
            this.Controls.Add(this.FirstNameText);
            this.Controls.Add(this.UpdateTakerButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UpdateUserForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Taker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button UpdateTakerButton;
        private System.Windows.Forms.ComboBox CityComboBox;
        private System.Windows.Forms.ComboBox SexComboBox;
        private System.Windows.Forms.Label CityLabel;
        private System.Windows.Forms.Label AddressLabel;
        private System.Windows.Forms.Label SexLabel;
        private System.Windows.Forms.Label EMailLabel;
        private System.Windows.Forms.Label PhoneNumberLabel;
        private System.Windows.Forms.Label EGNLabel;
        private System.Windows.Forms.Label IDCardLabel;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.TextBox AddressText;
        private System.Windows.Forms.TextBox EMailText;
        private System.Windows.Forms.TextBox PhoneNumberText;
        private System.Windows.Forms.TextBox EGNText;
        private System.Windows.Forms.TextBox IDCardText;
        private System.Windows.Forms.TextBox LastNameText;
        private System.Windows.Forms.TextBox FirstNameText;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
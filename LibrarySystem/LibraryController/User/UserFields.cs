﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryController.CustomClasses;

namespace LibraryController.User
{
    public class UserFields
    {
        public CustomField firstName;
        public CustomField lastName;
        public CustomField idcard;
        public CustomField egn;
        public CustomField phone;
        public CustomField email;
        public CustomField address;

        public UserFields()
        {
            firstName = new CustomField(MainForm.loc.userColumnFirstName, 2, 50);
            lastName = new CustomField(MainForm.loc.userColumnLastName, 2, 50);
            idcard = new CustomField(MainForm.loc.userColumnIDCard, 9, 9);
            egn = new CustomField(MainForm.loc.userColumnEGN, 10, 10);
            phone = new CustomField(MainForm.loc.userColumnPhone, 5, 50);
            email = new CustomField(MainForm.loc.userColumnEMail, 5, 50);
            address = new CustomField(MainForm.loc.userColumnAddress, 5, 100);
        }
    }
}
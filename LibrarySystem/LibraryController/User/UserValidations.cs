﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibraryController.CustomClasses;

namespace LibraryController.User
{
    public static class UserValidations
    {
        private static bool Name(ref string name, CustomField customField)
        {
            Validations.FilterEmptySpaces(ref name);

            char[] firstNameToChar = name.ToCharArray();
            foreach (var @char in firstNameToChar)
            {
                if (!Validations.AllowedNameCharset(@char))
                {
                    MessageBox.Show(MainForm.loc.theFieldNamed + customField.Name + MainForm.loc.invalidSymbols + MainForm.loc.nameSymbolsInfo, MainForm.loc.incorrectData, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            if (!Validations.MinSymbols(name, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(name, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            return true;
        }
        private static bool IDCard(ref string idcard, CustomField customField, List<ForkoLibraryServiceReference.User> userCollection, ForkoLibraryServiceReference.User currentUser = null)
        {
            Validations.FilterEmptySpaces(ref idcard);

            if (!Validations.CheckIfNumber(idcard, customField.Name))
            {
                return false;
            }

            if (!Validations.MinSymbols(idcard, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(idcard, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.PositiveNumber(long.Parse(idcard), customField.Name))
            {
                return false;
            }

            string idcardForLinq = idcard;

            if (currentUser != null)
            {
                if (userCollection.Where(x => x.ID != currentUser.ID).FirstOrDefault(x => x.IDCard.Equals(idcardForLinq)) != null)
                {
                    MessageBox.Show(MainForm.loc.idcardAlreadyExists,
                                    MainForm.loc.warning,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                if (userCollection.FirstOrDefault(x => x.IDCard.Equals(idcardForLinq)) != null)
                {
                    MessageBox.Show(MainForm.loc.idcardAlreadyExists,
                                    MainForm.loc.warning,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }

            return true;
        }
        private static bool EGN(ref string egn, CustomField customField, List<ForkoLibraryServiceReference.User> userCollection, ForkoLibraryServiceReference.User currentUser = null)
        {
            Validations.FilterEmptySpaces(ref egn);

            if (!Validations.CheckIfNumber(egn, customField.Name))
            {
                return false;
            }

            if (!Validations.MinSymbols(egn, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(egn, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.PositiveNumber(long.Parse(egn), customField.Name))
            {
                return false;
            }

            string egnForLinq = egn;

            if (currentUser != null)
            {
                if (userCollection.Where(x => x.ID != currentUser.ID).FirstOrDefault(x => x.EGN.Equals(egnForLinq)) != null)
                {
                    MessageBox.Show(MainForm.loc.egnAlreadyExists,
                                    MainForm.loc.warning,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                if (userCollection.FirstOrDefault(x => x.EGN.Equals(egnForLinq)) != null)
                {
                    MessageBox.Show(MainForm.loc.egnAlreadyExists,
                                    MainForm.loc.warning,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                    return false;
                }
            }

            return true;
        }
        private static bool Address(ref string address, CustomField customField)
        {
            address.TrimStart(' ');

            if (!Validations.MinSymbols(address, customField.MinSymbols, customField.Name))
            {
                return false;
            }
            if (!Validations.MaxSymbols(address, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            return true;
        }
        private static bool Phone(ref string phone, CustomField customField)
        {
            phone.TrimStart(' ');

            if (!Validations.CheckIfNumber(phone, customField.Name))
            {
                return false;
            }
            if (!Validations.MinSymbols(phone, customField.MinSymbols, customField.Name))
            {
                return false;
            }
            if (!Validations.MaxSymbols(phone, customField.MaxSymbols, customField.Name))
            {
                return false;
            }
            if (!Validations.PositiveNumber(long.Parse(phone), customField.Name))
            {
                return false;
            }

            return true;
        }
        private static bool EMail(ref string email, CustomField customField)
        {
            Validations.FilterEmptySpaces(ref email);

            if (!Validations.CheckForRequiredSymbols(email, customField.Name, new char[] { '@', '.' }, MainForm.loc.emailRequiredSymbols))
            {
                return false;
            }

            if (!Validations.MinSymbols(email, customField.MinSymbols, customField.Name))
            {
                return false;
            }

            if (!Validations.MaxSymbols(email, customField.MaxSymbols, customField.Name))
            {
                return false;
            }

            return true;
        }
        private static bool City(ComboBox cityComboBox)
        {
            if (cityComboBox.Items.Count < 1 || cityComboBox.SelectedItem == null)
            {
                MessageBox.Show(MainForm.loc.noCitySelected, MainForm.loc.error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        public static bool CreateUser(ref string firstName, ref string lastName, ref string idcard, ref string egn,
            ref string address, ref string phone, ref string email, ComboBox cityComboBox,
            UserFields userFields, List<ForkoLibraryServiceReference.User> userCollection)
        {
            if (!Name(ref firstName, userFields.firstName)) { return false; }
            if (!Name(ref lastName, userFields.lastName)) { return false; }
            if (!IDCard(ref idcard, userFields.idcard, userCollection)) { return false; }
            if (!EGN(ref egn, userFields.egn, userCollection)) { return false; }
            if (!Phone(ref phone, userFields.phone)) { return false; }
            if (!EMail(ref email, userFields.email)) { return false; }
            if (!Address(ref address, userFields.address)) { return false; }
            if (!City(cityComboBox)){ return false; }

            return true;
        }

        public static bool UpdateUser(ref string firstName, ref string lastName, ref string idcard, ref string egn,
            ref string address, ref string phone, ref string email, ComboBox cityComboBox,
            UserFields userFields, List<ForkoLibraryServiceReference.User> userCollection, ForkoLibraryServiceReference.User currentUser)
        {
            if (!Name(ref firstName, userFields.firstName)) { return false; }
            if (!Name(ref lastName, userFields.lastName)) { return false; }
            if (!IDCard(ref idcard, userFields.idcard, userCollection, currentUser)) { return false; }
            if (!EGN(ref egn, userFields.egn, userCollection, currentUser)) { return false; }
            if (!Phone(ref phone, userFields.phone)) { return false; }
            if (!EMail(ref email, userFields.email)) { return false; }
            if (!Address(ref address, userFields.address)) { return false; }
            if (!City(cityComboBox)) { return false; }

            return true;
        }
    }
}

﻿using BusinessLibrary;
using LibraryController.ForkoLibraryServiceReference;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.User
{
    public partial class UpdateUserForm : Form
    {
        private ForkoLibraryServiceClient dal;

        private ForkoLibraryServiceReference.User currentUser;
        private List<ForkoLibraryServiceReference.User> allUsers;
        private List<ForkoLibraryServiceReference.City> allCities;
        private List<CustomClasses.City> customAllCities = new List<CustomClasses.City>();

        private UserFields userFields;

        string firstName;
        string lastName;
        string idcard;
        string egn;
        string phone;
        string email;
        string address;

        public UpdateUserForm(ForkoLibraryServiceReference.User importedUser)
        {
            InitializeComponent();

            dal = new ForkoLibraryServiceClient();
            currentUser = importedUser;
            allUsers = dal.GetAllUsers().ToList();

            allCities = dal.GetAllCities().ToList();
            foreach (var city in allCities)
            {
                customAllCities.Add(CustomClasses.Mapper.ToClient(city));
            }


            userFields = new UserFields();

            UIGod.FillUpComboBox(SexComboBox, MainForm.loc.sex);
            UIGod.FillUpComboBox(CityComboBox, customAllCities.ToArray());

            FillUpInformation();
            Localize();
        }

        private void Localize()
        {
            Text = MainForm.loc.createUserForm;
            FirstNameLabel.Text = MainForm.loc.userColumnFirstName;
            LastNameLabel.Text = MainForm.loc.userColumnLastName;
            AddressLabel.Text = MainForm.loc.userColumnAddress;
            EGNLabel.Text = MainForm.loc.userColumnEGN;
            SexLabel.Text = MainForm.loc.columnSex;
            EMailLabel.Text = MainForm.loc.userColumnEMail;
            CityLabel.Text = MainForm.loc.userColumnCity;
            IDCardLabel.Text = MainForm.loc.userColumnIDCard;
            PhoneNumberLabel.Text = MainForm.loc.userColumnPhone;
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            toolTip1.SetToolTip(UpdateTakerButton, MainForm.loc.updateUserButton);
        }

        private void FillUpInformation()
        {
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();

            FirstNameText.Text = currentUser.FirstName;
            LastNameText.Text = currentUser.LastName;
            IDCardText.Text = currentUser.IDCard;
            EGNText.Text = currentUser.EGN;
            SexComboBox.SelectedIndex = currentUser.Sex;
            PhoneNumberText.Text = currentUser.Phone;
            EMailText.Text = currentUser.EMail;
            AddressText.Text = currentUser.Address;
            CityComboBox.SelectedIndex = allCities.FindIndex(x => x.ID == currentUser.CityID);
        }

        private void UpdateTakerButton_Click(object sender, EventArgs e)
        {
            firstName = FirstNameText.Text;
            lastName = LastNameText.Text;
            idcard = IDCardText.Text;
            egn = EGNText.Text;
            phone = PhoneNumberText.Text;
            email = EMailText.Text;
            address = AddressText.Text;

            if (UserValidations.UpdateUser(ref firstName, ref lastName, ref idcard, ref egn, 
                ref address, ref phone, ref email, CityComboBox, userFields, allUsers, currentUser))
            {
                ForkoLibraryServiceReference.User newUser = new ForkoLibraryServiceReference.User
                {
                    FirstName = firstName,
                    LastName = lastName,
                    IDCard = idcard,
                    EGN = egn,
                    Sex = (byte)SexComboBox.SelectedIndex,
                    Phone = phone,
                    EMail = email,
                    Address = address,
                    CityID = ((CustomClasses.City)CityComboBox.SelectedItem).ID
                };

                dal.UpdateUser(currentUser.ID, newUser);
                Close();
            }
        }
    }
}

﻿using BusinessLibrary;
using LibraryController.ForkoLibraryServiceReference;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.User
{
    public partial class CreateUserForm : Form
    {
        ForkoLibraryServiceClient dal;
        List<ForkoLibraryServiceReference.User> allUsers;
        List<CustomClasses.City> allCities;

        UserFields userFields;

        string firstName;
        string lastName;
        string idcard;
        string egn;
        string phone;
        string email;
        string address;

        public CreateUserForm()
        {
            InitializeComponent();

            dal = new ForkoLibraryServiceClient();
            userFields = new UserFields();
            allUsers = dal.GetAllUsers().ToList();

            allCities = new List<CustomClasses.City>();
            foreach (var city in dal.GetAllCities().ToList())
            {
                allCities.Add(CustomClasses.Mapper.ToClient(city));
            }

            UIGod.FillUpComboBox(SexComboBox, MainForm.loc.sex);
            UIGod.FillUpComboBox(CityComboBox, allCities.ToArray());

            Localize();
        }

        private void Localize()
        {
            Text = MainForm.loc.createUserForm;
            FirstNameLabel.Text = MainForm.loc.userColumnFirstName;
            LastNameLabel.Text = MainForm.loc.userColumnLastName;
            AddressLabel.Text = MainForm.loc.userColumnAddress;
            EGNLabel.Text = MainForm.loc.userColumnEGN;
            SexLabel.Text = MainForm.loc.columnSex;
            EMailLabel.Text = MainForm.loc.userColumnEMail;
            CityLabel.Text = MainForm.loc.userColumnCity;
            IDCardLabel.Text = MainForm.loc.userColumnIDCard;
            PhoneNumberLabel.Text = MainForm.loc.userColumnPhone;
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            toolTip1.SetToolTip(CreateTakerButton, MainForm.loc.createUserButton);
        }

        private void CreateUserButton_Click(object sender, EventArgs e)
        {
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();

            firstName = FirstNameText.Text;
            lastName = LastNameText.Text;
            idcard = IDCardText.Text;
            egn = EGNText.Text;
            phone = PhoneNumberText.Text;
            email = EMailText.Text;
            address = AddressText.Text;

            if (UserValidations.CreateUser(ref firstName, ref lastName, ref idcard,
                ref egn, ref address, ref phone, ref email, CityComboBox, userFields, allUsers))
            {
                ForkoLibraryServiceReference.User newUser = new ForkoLibraryServiceReference.User
                {
                    FirstName = firstName,
                    LastName = lastName,
                    IDCard = IDCardText.Text,
                    EGN = EGNText.Text,
                    Sex = (byte)SexComboBox.SelectedIndex,
                    Phone = PhoneNumberText.Text,
                    EMail = EMailText.Text,
                    Address = AddressText.Text,
                    CityID = ((CustomClasses.City)CityComboBox.SelectedItem).ID
                };

                dal.CreateUser(newUser);
                Close();
            }
        }

        private void NewCityButton_Click(object sender, EventArgs e)
        {
            City.CreateCity createCityForm = new City.CreateCity();
            createCityForm.ShowDialog();
            allCities = new List<CustomClasses.City>();
            foreach (var city in dal.GetAllCities().ToList())
            {
                allCities.Add(CustomClasses.Mapper.ToClient(city));
            }
            UIGod.FillUpComboBox(CityComboBox, allCities.ToArray());
        }
    }
}
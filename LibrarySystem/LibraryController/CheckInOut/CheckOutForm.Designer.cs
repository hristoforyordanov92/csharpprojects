﻿namespace LibraryController.CheckInOut
{
    partial class CheckOutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckOutForm));
            this.AvailableBooksCheckboxList = new System.Windows.Forms.CheckedListBox();
            this.CheckOutBooksButton = new System.Windows.Forms.Button();
            this.InformationLabel = new System.Windows.Forms.Label();
            this.FromDayComboBox = new System.Windows.Forms.ComboBox();
            this.FromMonthComboBox = new System.Windows.Forms.ComboBox();
            this.FromYearComboBox = new System.Windows.Forms.ComboBox();
            this.ToDayComboBox = new System.Windows.Forms.ComboBox();
            this.ToMonthComboBox = new System.Windows.Forms.ComboBox();
            this.ToYearComboBox = new System.Windows.Forms.ComboBox();
            this.ChosenUserLabel = new System.Windows.Forms.Label();
            this.FromDateLabel = new System.Windows.Forms.Label();
            this.ToDateLabel = new System.Windows.Forms.Label();
            this.BookListLabel = new System.Windows.Forms.Label();
            this.CancelButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // AvailableBooksCheckboxList
            // 
            this.AvailableBooksCheckboxList.CheckOnClick = true;
            this.AvailableBooksCheckboxList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AvailableBooksCheckboxList.FormattingEnabled = true;
            this.AvailableBooksCheckboxList.Location = new System.Drawing.Point(16, 83);
            this.AvailableBooksCheckboxList.Name = "AvailableBooksCheckboxList";
            this.AvailableBooksCheckboxList.Size = new System.Drawing.Size(473, 327);
            this.AvailableBooksCheckboxList.TabIndex = 3;
            // 
            // CheckOutBooksButton
            // 
            this.CheckOutBooksButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckOutBooksButton.Image = global::LibraryController.Properties.Resources.checkout;
            this.CheckOutBooksButton.Location = new System.Drawing.Point(16, 493);
            this.CheckOutBooksButton.Name = "CheckOutBooksButton";
            this.CheckOutBooksButton.Size = new System.Drawing.Size(48, 48);
            this.CheckOutBooksButton.TabIndex = 10;
            this.CheckOutBooksButton.UseVisualStyleBackColor = true;
            this.CheckOutBooksButton.Click += new System.EventHandler(this.CheckOutBooksButton_Click);
            // 
            // InformationLabel
            // 
            this.InformationLabel.AutoSize = true;
            this.InformationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InformationLabel.Location = new System.Drawing.Point(12, 9);
            this.InformationLabel.Name = "InformationLabel";
            this.InformationLabel.Size = new System.Drawing.Size(103, 16);
            this.InformationLabel.TabIndex = 0;
            this.InformationLabel.Text = "informationlabel";
            // 
            // FromDayComboBox
            // 
            this.FromDayComboBox.DropDownHeight = 200;
            this.FromDayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FromDayComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromDayComboBox.FormattingEnabled = true;
            this.FromDayComboBox.IntegralHeight = false;
            this.FromDayComboBox.Location = new System.Drawing.Point(214, 416);
            this.FromDayComboBox.Name = "FromDayComboBox";
            this.FromDayComboBox.Size = new System.Drawing.Size(52, 24);
            this.FromDayComboBox.TabIndex = 4;
            // 
            // FromMonthComboBox
            // 
            this.FromMonthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FromMonthComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromMonthComboBox.FormattingEnabled = true;
            this.FromMonthComboBox.Location = new System.Drawing.Point(272, 416);
            this.FromMonthComboBox.Name = "FromMonthComboBox";
            this.FromMonthComboBox.Size = new System.Drawing.Size(140, 24);
            this.FromMonthComboBox.TabIndex = 5;
            // 
            // FromYearComboBox
            // 
            this.FromYearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FromYearComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromYearComboBox.FormattingEnabled = true;
            this.FromYearComboBox.Location = new System.Drawing.Point(418, 416);
            this.FromYearComboBox.Name = "FromYearComboBox";
            this.FromYearComboBox.Size = new System.Drawing.Size(71, 24);
            this.FromYearComboBox.TabIndex = 6;
            // 
            // ToDayComboBox
            // 
            this.ToDayComboBox.DropDownHeight = 200;
            this.ToDayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ToDayComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToDayComboBox.FormattingEnabled = true;
            this.ToDayComboBox.IntegralHeight = false;
            this.ToDayComboBox.Location = new System.Drawing.Point(214, 446);
            this.ToDayComboBox.Name = "ToDayComboBox";
            this.ToDayComboBox.Size = new System.Drawing.Size(52, 24);
            this.ToDayComboBox.TabIndex = 7;
            // 
            // ToMonthComboBox
            // 
            this.ToMonthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ToMonthComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToMonthComboBox.FormattingEnabled = true;
            this.ToMonthComboBox.Location = new System.Drawing.Point(272, 446);
            this.ToMonthComboBox.Name = "ToMonthComboBox";
            this.ToMonthComboBox.Size = new System.Drawing.Size(140, 24);
            this.ToMonthComboBox.TabIndex = 8;
            // 
            // ToYearComboBox
            // 
            this.ToYearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ToYearComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToYearComboBox.FormattingEnabled = true;
            this.ToYearComboBox.Location = new System.Drawing.Point(418, 446);
            this.ToYearComboBox.Name = "ToYearComboBox";
            this.ToYearComboBox.Size = new System.Drawing.Size(71, 24);
            this.ToYearComboBox.TabIndex = 9;
            // 
            // ChosenUserLabel
            // 
            this.ChosenUserLabel.AutoSize = true;
            this.ChosenUserLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChosenUserLabel.Location = new System.Drawing.Point(12, 25);
            this.ChosenUserLabel.Name = "ChosenUserLabel";
            this.ChosenUserLabel.Size = new System.Drawing.Size(105, 20);
            this.ChosenUserLabel.TabIndex = 1;
            this.ChosenUserLabel.Text = "chosenUser";
            // 
            // FromDateLabel
            // 
            this.FromDateLabel.AutoSize = true;
            this.FromDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromDateLabel.Location = new System.Drawing.Point(13, 419);
            this.FromDateLabel.Name = "FromDateLabel";
            this.FromDateLabel.Size = new System.Drawing.Size(63, 16);
            this.FromDateLabel.TabIndex = 12;
            this.FromDateLabel.Text = "fromDate";
            // 
            // ToDateLabel
            // 
            this.ToDateLabel.AutoSize = true;
            this.ToDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToDateLabel.Location = new System.Drawing.Point(13, 449);
            this.ToDateLabel.Name = "ToDateLabel";
            this.ToDateLabel.Size = new System.Drawing.Size(48, 16);
            this.ToDateLabel.TabIndex = 13;
            this.ToDateLabel.Text = "toDate";
            // 
            // BookListLabel
            // 
            this.BookListLabel.AutoSize = true;
            this.BookListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BookListLabel.Location = new System.Drawing.Point(12, 64);
            this.BookListLabel.Name = "BookListLabel";
            this.BookListLabel.Size = new System.Drawing.Size(73, 16);
            this.BookListLabel.TabIndex = 2;
            this.BookListLabel.Text = "listofbooks";
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Image = global::LibraryController.Properties.Resources.checkin;
            this.CancelButton.Location = new System.Drawing.Point(441, 493);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(48, 48);
            this.CancelButton.TabIndex = 11;
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckOutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 559);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.BookListLabel);
            this.Controls.Add(this.ToDateLabel);
            this.Controls.Add(this.FromDateLabel);
            this.Controls.Add(this.ChosenUserLabel);
            this.Controls.Add(this.ToDayComboBox);
            this.Controls.Add(this.ToMonthComboBox);
            this.Controls.Add(this.ToYearComboBox);
            this.Controls.Add(this.FromDayComboBox);
            this.Controls.Add(this.FromMonthComboBox);
            this.Controls.Add(this.FromYearComboBox);
            this.Controls.Add(this.InformationLabel);
            this.Controls.Add(this.CheckOutBooksButton);
            this.Controls.Add(this.AvailableBooksCheckboxList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CheckOutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CheckOutForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox AvailableBooksCheckboxList;
        private System.Windows.Forms.Button CheckOutBooksButton;
        private System.Windows.Forms.Label InformationLabel;
        private System.Windows.Forms.ComboBox FromDayComboBox;
        private System.Windows.Forms.ComboBox FromMonthComboBox;
        private System.Windows.Forms.ComboBox FromYearComboBox;
        private System.Windows.Forms.ComboBox ToDayComboBox;
        private System.Windows.Forms.ComboBox ToMonthComboBox;
        private System.Windows.Forms.ComboBox ToYearComboBox;
        private System.Windows.Forms.Label ChosenUserLabel;
        private System.Windows.Forms.Label FromDateLabel;
        private System.Windows.Forms.Label ToDateLabel;
        private System.Windows.Forms.Label BookListLabel;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
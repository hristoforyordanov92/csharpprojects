﻿using LibraryController.CustomClasses;
using LibraryController.ForkoLibraryServiceReference;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.CheckInOut
{
    public partial class CheckInForm : Form
    {
        private ForkoLibraryServiceReference.User chosenUser;

        public CheckInForm(ForkoLibraryServiceReference.User chosenUser)
        {
            InitializeComponent();
            this.chosenUser = chosenUser;
            LoadAvailableBooksCheckboxListData();
            Localize();
        }

        private void Localize()
        {
            this.Text = MainForm.loc.checkinForm;
            InformationLabel.Text = MainForm.loc.checkinInformationLabel;
            ChosenUserLabel.Text = $"{chosenUser.FirstName} {chosenUser.LastName}";
            //CheckInBooksButton.Text = MainForm.loc.checkinButton;
            BookListLabel.Text = MainForm.loc.checkinBookListLabel;
            toolTip1.SetToolTip(CheckInBooksButton, MainForm.loc.checkinButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
        }

        private void LoadAvailableBooksCheckboxListData()
        {
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            var availableBooks = dal.GetAllBooks().Join(dal.GetAllCheckouts(), b => b.ID, c => c.BookID,
                (b, c) => new
                {
                    Book = b,
                    Checkout = c
                }).Where(x => x.Checkout.UserID == chosenUser.ID);

            List<CustomClasses.Book> listBooks = new List<CustomClasses.Book>();
            foreach (var item in availableBooks)
            {
                listBooks.Add(Mapper.ToClient(item.Book));
            }
            object[] objectCollection = listBooks.OrderBy(x => x.Title).ToArray();
            AvailableBooksCheckboxList.Items.AddRange(objectCollection);
        }

        private void CheckOutBooksButton_Click(object sender, EventArgs e)
        {
            var chosenBooks = AvailableBooksCheckboxList.CheckedItems;
            var chosenBooksAsBook = new List<CustomClasses.Book>();
            foreach (var chosenBook in chosenBooks)
            {
                chosenBooksAsBook.Add((CustomClasses.Book)chosenBook);
            }
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            var checkouts = dal.GetAllCheckouts().Where(x => x.UserID == chosenUser.ID);
            foreach (var book in chosenBooksAsBook)
            {
                dal.DeleteCheckoutById(checkouts.FirstOrDefault(x => x.BookID == book.ID).ID);
            }

            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
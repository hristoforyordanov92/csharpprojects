﻿namespace LibraryController.CheckInOut
{
    partial class CheckInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckInForm));
            this.BookListLabel = new System.Windows.Forms.Label();
            this.ChosenUserLabel = new System.Windows.Forms.Label();
            this.InformationLabel = new System.Windows.Forms.Label();
            this.AvailableBooksCheckboxList = new System.Windows.Forms.CheckedListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.CheckInBooksButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BookListLabel
            // 
            this.BookListLabel.AutoSize = true;
            this.BookListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BookListLabel.Location = new System.Drawing.Point(12, 74);
            this.BookListLabel.Name = "BookListLabel";
            this.BookListLabel.Size = new System.Drawing.Size(73, 16);
            this.BookListLabel.TabIndex = 2;
            this.BookListLabel.Text = "listofbooks";
            // 
            // ChosenUserLabel
            // 
            this.ChosenUserLabel.AutoSize = true;
            this.ChosenUserLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChosenUserLabel.Location = new System.Drawing.Point(12, 25);
            this.ChosenUserLabel.Name = "ChosenUserLabel";
            this.ChosenUserLabel.Size = new System.Drawing.Size(105, 20);
            this.ChosenUserLabel.TabIndex = 1;
            this.ChosenUserLabel.Text = "chosenUser";
            // 
            // InformationLabel
            // 
            this.InformationLabel.AutoSize = true;
            this.InformationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InformationLabel.Location = new System.Drawing.Point(12, 9);
            this.InformationLabel.Name = "InformationLabel";
            this.InformationLabel.Size = new System.Drawing.Size(103, 16);
            this.InformationLabel.TabIndex = 0;
            this.InformationLabel.Text = "informationlabel";
            // 
            // AvailableBooksCheckboxList
            // 
            this.AvailableBooksCheckboxList.CheckOnClick = true;
            this.AvailableBooksCheckboxList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AvailableBooksCheckboxList.FormattingEnabled = true;
            this.AvailableBooksCheckboxList.Location = new System.Drawing.Point(15, 93);
            this.AvailableBooksCheckboxList.Name = "AvailableBooksCheckboxList";
            this.AvailableBooksCheckboxList.Size = new System.Drawing.Size(477, 327);
            this.AvailableBooksCheckboxList.TabIndex = 3;
            // 
            // CheckInBooksButton
            // 
            this.CheckInBooksButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckInBooksButton.Image = global::LibraryController.Properties.Resources.box;
            this.CheckInBooksButton.Location = new System.Drawing.Point(15, 432);
            this.CheckInBooksButton.Name = "CheckInBooksButton";
            this.CheckInBooksButton.Size = new System.Drawing.Size(48, 48);
            this.CheckInBooksButton.TabIndex = 4;
            this.CheckInBooksButton.UseVisualStyleBackColor = true;
            this.CheckInBooksButton.Click += new System.EventHandler(this.CheckOutBooksButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Image = global::LibraryController.Properties.Resources.checkin;
            this.CancelButton.Location = new System.Drawing.Point(444, 432);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(48, 48);
            this.CancelButton.TabIndex = 5;
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // CheckInForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 495);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.BookListLabel);
            this.Controls.Add(this.ChosenUserLabel);
            this.Controls.Add(this.InformationLabel);
            this.Controls.Add(this.AvailableBooksCheckboxList);
            this.Controls.Add(this.CheckInBooksButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CheckInForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CheckInForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button CheckInBooksButton;
        private System.Windows.Forms.Label BookListLabel;
        private System.Windows.Forms.Label ChosenUserLabel;
        private System.Windows.Forms.Label InformationLabel;
        private System.Windows.Forms.CheckedListBox AvailableBooksCheckboxList;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button CancelButton;
    }
}
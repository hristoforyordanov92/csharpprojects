﻿using LibraryController.CustomClasses;
using LibraryController.ForkoLibraryServiceReference;
using LibraryController.HelperClasses;
using LibraryDataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryController.CheckInOut
{
    public partial class CheckOutForm : Form
    {
        private ForkoLibraryServiceReference.User chosenTaker;
        private CustomComboDate fromDate;
        private CustomComboDate toDate;

        public CheckOutForm(ForkoLibraryServiceReference.User chosenTaker)
        {
            InitializeComponent();
            LoadAvailableBooksCheckboxListData();
            this.chosenTaker = chosenTaker;

            fromDate = new CustomComboDate(FromYearComboBox, FromMonthComboBox, FromDayComboBox, DateTime.Now.Year, DateTime.Now.Year + 10);
            toDate = new CustomComboDate(ToYearComboBox, ToMonthComboBox, ToDayComboBox, DateTime.Now.Year, DateTime.Now.Year + 10);
            fromDate.SetSecondaryCustomComboDate(toDate);

            Localize();
        }

        private void Localize()
        {
            this.Text = MainForm.loc.checkoutForm;
            InformationLabel.Text = MainForm.loc.checkoutInformationLabel;
            ChosenUserLabel.Text = $"{chosenTaker.FirstName} {chosenTaker.LastName}";
            toolTip1.SetToolTip(CheckOutBooksButton, MainForm.loc.checkoutButton);
            toolTip1.SetToolTip(CancelButton, MainForm.loc.cancelButton);
            FromDateLabel.Text = MainForm.loc.checkoutFromDateLabel;
            ToDateLabel.Text = MainForm.loc.checkoutToDateLabel;
            BookListLabel.Text = MainForm.loc.checkoutBookListLabel;
        }

        //private void SetupToDateComboBoxes()
        //{
        //    SetupToYearComboBox();
        //    SetupToMonthComboBox();
        //    SetupToDayComboBox(null, EventArgs.Empty);
        //}
        //private void SetupToYearComboBox()
        //{
        //    var currentYear = DateTime.Now.Year;
        //    for (int i = 0; i < 50; i++)
        //    {
        //        ToYearComboBox.Items.Add(currentYear + i);
        //    }
        //    ToYearComboBox.SelectedItem = ToYearComboBox.Items[0];
        //}
        //private void SetupToMonthComboBox()
        //{
        //    if (ToMonthComboBox.Items.Count > 0)
        //    {
        //        ToMonthComboBox.Items.Clear();
        //    }

        //    for (int i = 0; i < 12; i++)
        //    {
        //        ToMonthComboBox.Items.Add(MainForm.loc.months[i]);
        //    }
        //    ToMonthComboBox.SelectedIndex = 0;
        //}
        //private void SetupToDayComboBox(object sender, EventArgs e)
        //{
        //    int oldSelectedIndex = ToDayComboBox.SelectedIndex;
        //    int daysInMonth = DateTime.DaysInMonth((int)ToYearComboBox.SelectedItem, ToMonthComboBox.SelectedIndex + 1);
        //    int newSelectedIndex = 0;

        //    if (ToDayComboBox.Items.Count > 0)
        //    {
        //        if (oldSelectedIndex > daysInMonth - 1)
        //        {
        //            newSelectedIndex = daysInMonth - 1;
        //        }
        //        else
        //        {
        //            newSelectedIndex = oldSelectedIndex;
        //        }
        //        ToDayComboBox.Items.Clear();
        //    }

        //    for (int i = 1; i <= daysInMonth; i++)
        //    {
        //        ToDayComboBox.Items.Add(i);
        //    }

        //    ToDayComboBox.SelectedItem = ToDayComboBox.Items[newSelectedIndex];
        //}
        //private void SubscribeToYearAndMonthComboBoxes()
        //{
        //    ToYearComboBox.SelectedIndexChanged += SetupToDayComboBox;
        //    ToMonthComboBox.SelectedIndexChanged += SetupToDayComboBox;
        //    ToYearComboBox.SelectedIndexChanged += CorrectToDateComboBox;
        //    ToMonthComboBox.SelectedIndexChanged += CorrectToDateComboBox;
        //    ToDayComboBox.SelectedIndexChanged += CorrectToDateComboBox;
        //}

        //private void SetupFromDateComboBoxes()
        //{
        //    SetupFromYearComboBox();
        //    SetupFromMonthComboBox();
        //    SetupFromDayComboBox(null, EventArgs.Empty);
        //}
        //private void SetupFromYearComboBox()
        //{
        //    var currentYear = DateTime.Now.Year;
        //    for (int i = 0; i < 50; i++)
        //    {
        //        FromYearComboBox.Items.Add(currentYear + i);
        //    }
        //    FromYearComboBox.SelectedItem = FromYearComboBox.Items[0];
        //}
        //private void SetupFromMonthComboBox()
        //{
        //    if (FromMonthComboBox.Items.Count > 0)
        //    {
        //        FromMonthComboBox.Items.Clear();
        //    }

        //    for (int i = 0; i < 12; i++)
        //    {
        //        FromMonthComboBox.Items.Add(MainForm.loc.months[i]);
        //    }
        //    FromMonthComboBox.SelectedIndex = 0;
        //}
        //private void SetupFromDayComboBox(object sender, EventArgs e)
        //{
        //    int oldSelectedIndex = FromDayComboBox.SelectedIndex;
        //    int daysInMonth = DateTime.DaysInMonth((int)FromYearComboBox.SelectedItem, FromMonthComboBox.SelectedIndex + 1);
        //    int newSelectedIndex = 0;

        //    if (FromDayComboBox.Items.Count > 0)
        //    {
        //        if (oldSelectedIndex > daysInMonth - 1)
        //        {
        //            newSelectedIndex = daysInMonth - 1;
        //        }
        //        else
        //        {
        //            newSelectedIndex = oldSelectedIndex;
        //        }
        //        FromDayComboBox.Items.Clear();
        //    }

        //    for (int i = 1; i <= daysInMonth; i++)
        //    {
        //        FromDayComboBox.Items.Add(i);
        //    }

        //    FromDayComboBox.SelectedItem = FromDayComboBox.Items[newSelectedIndex];
        //}
        //private void SubscribeFromYearAndMonthComboBoxes()
        //{
        //    FromYearComboBox.SelectedIndexChanged += SetupFromDayComboBox;
        //    FromMonthComboBox.SelectedIndexChanged += SetupFromDayComboBox;
        //    FromYearComboBox.SelectedIndexChanged += CorrectToDateComboBox;
        //    FromMonthComboBox.SelectedIndexChanged += CorrectToDateComboBox;
        //    FromDayComboBox.SelectedIndexChanged += CorrectToDateComboBox;
        //}

        private void CorrectToDateComboBox(object sender, EventArgs e)
        {
            DateTime fromDate = new DateTime((int)FromYearComboBox.SelectedItem,
                                            FromMonthComboBox.SelectedIndex + 1,
                                            (int)FromDayComboBox.SelectedItem);
            DateTime toDate = new DateTime((int)ToYearComboBox.SelectedItem,
                                            ToMonthComboBox.SelectedIndex + 1,
                                            (int)ToDayComboBox.SelectedItem);
            if (fromDate > toDate)
            {
                ToYearComboBox.SelectedIndex = FromYearComboBox.SelectedIndex;
                ToMonthComboBox.SelectedIndex = FromMonthComboBox.SelectedIndex;
                ToDayComboBox.SelectedIndex = FromDayComboBox.SelectedIndex;
            }
        }

        private void LoadAvailableBooksCheckboxListData()
        {
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            var availableBooks = dal.GetAllBooks().GroupJoin(dal.GetAllCheckouts(), b => b.ID, c => c.BookID,
                (b, c) => new
                {
                    Book = b,
                    Checkout = c
                });

            List<CustomClasses.Book> listBooks = new List<CustomClasses.Book>();
            foreach (var item in availableBooks)
            {
                if (item.Checkout.Count() == 0)
                {
                    listBooks.Add(Mapper.ToClient(item.Book));
                }
            }
            object[] objectCollection = listBooks.OrderBy(x => x.Title).ToArray();
            AvailableBooksCheckboxList.Items.AddRange(objectCollection);
        }

        private void CheckOutBooksButton_Click(object sender, EventArgs e)
        {
            var chosenBooks = AvailableBooksCheckboxList.CheckedItems;
            var chosenBooksAsBook = new List<CustomClasses.Book>();
            foreach (var chosenBook in chosenBooks)
            {
                chosenBooksAsBook.Add((CustomClasses.Book)chosenBook);
            }
            ForkoLibraryServiceClient dal = new ForkoLibraryServiceClient();
            DateTime fromDate = new DateTime((int)FromYearComboBox.SelectedItem,
                                            FromMonthComboBox.SelectedIndex + 1,
                                            (int)FromDayComboBox.SelectedItem);
            DateTime toDate = new DateTime((int)ToYearComboBox.SelectedItem,
                                            ToMonthComboBox.SelectedIndex + 1,
                                            (int)ToDayComboBox.SelectedItem);

            foreach (var book in chosenBooksAsBook)
            {
                dal.CreateCheckout(new ForkoLibraryServiceReference.Checkout()
                {
                    FromDate = fromDate,
                    ToDate = toDate,
                    BookID = book.ID,
                    UserID = chosenTaker.ID
                });
            }

            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
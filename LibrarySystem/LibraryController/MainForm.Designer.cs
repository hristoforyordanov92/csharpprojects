﻿namespace LibraryController
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(22222D, 0D);
            this.TabControl = new System.Windows.Forms.TabControl();
            this.BookTab = new System.Windows.Forms.TabPage();
            this.RefreshBookButton = new System.Windows.Forms.Button();
            this.DeleteBookButton = new System.Windows.Forms.Button();
            this.UpdateBookButton = new System.Windows.Forms.Button();
            this.CreateBookButton = new System.Windows.Forms.Button();
            this.BookDataGridView = new System.Windows.Forms.DataGridView();
            this.AuthorTab = new System.Windows.Forms.TabPage();
            this.RefreshAuthorButton = new System.Windows.Forms.Button();
            this.DeleteAuthorButton = new System.Windows.Forms.Button();
            this.UpdateAuthorButton = new System.Windows.Forms.Button();
            this.CreateAuthorButton = new System.Windows.Forms.Button();
            this.AuthorDataGridView = new System.Windows.Forms.DataGridView();
            this.UserTab = new System.Windows.Forms.TabPage();
            this.RefreshUserButton = new System.Windows.Forms.Button();
            this.CheckInButton = new System.Windows.Forms.Button();
            this.CheckOutButton = new System.Windows.Forms.Button();
            this.DeleteUserButton = new System.Windows.Forms.Button();
            this.CreateUserButton = new System.Windows.Forms.Button();
            this.UpdateUserButton = new System.Windows.Forms.Button();
            this.UserDataGridView = new System.Windows.Forms.DataGridView();
            this.StatisticsTab = new System.Windows.Forms.TabPage();
            this.ChartDateLabel = new System.Windows.Forms.Label();
            this.ChartDateComboBox = new System.Windows.Forms.ComboBox();
            this.Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.AuthorID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorBirthDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookAuthor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookAuthorID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookUniqueNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookPageCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookDatePublished = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BookIsTaken = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.managerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.citiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.българскиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.TabControl.SuspendLayout();
            this.BookTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BookDataGridView)).BeginInit();
            this.AuthorTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorDataGridView)).BeginInit();
            this.UserTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserDataGridView)).BeginInit();
            this.StatisticsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            resources.ApplyResources(this.TabControl, "TabControl");
            this.TabControl.Controls.Add(this.BookTab);
            this.TabControl.Controls.Add(this.AuthorTab);
            this.TabControl.Controls.Add(this.UserTab);
            this.TabControl.Controls.Add(this.StatisticsTab);
            this.TabControl.Multiline = true;
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            // 
            // BookTab
            // 
            this.BookTab.BackColor = System.Drawing.SystemColors.Control;
            this.BookTab.Controls.Add(this.RefreshBookButton);
            this.BookTab.Controls.Add(this.DeleteBookButton);
            this.BookTab.Controls.Add(this.UpdateBookButton);
            this.BookTab.Controls.Add(this.CreateBookButton);
            this.BookTab.Controls.Add(this.BookDataGridView);
            resources.ApplyResources(this.BookTab, "BookTab");
            this.BookTab.Name = "BookTab";
            this.BookTab.Enter += new System.EventHandler(this.RefreshBookButton_Click);
            // 
            // RefreshBookButton
            // 
            this.RefreshBookButton.Image = global::LibraryController.Properties.Resources.refreshList;
            resources.ApplyResources(this.RefreshBookButton, "RefreshBookButton");
            this.RefreshBookButton.Name = "RefreshBookButton";
            this.RefreshBookButton.UseVisualStyleBackColor = true;
            this.RefreshBookButton.Click += new System.EventHandler(this.RefreshBookButton_Click);
            // 
            // DeleteBookButton
            // 
            this.DeleteBookButton.Image = global::LibraryController.Properties.Resources.deleteEntry;
            resources.ApplyResources(this.DeleteBookButton, "DeleteBookButton");
            this.DeleteBookButton.Name = "DeleteBookButton";
            this.DeleteBookButton.UseVisualStyleBackColor = true;
            this.DeleteBookButton.Click += new System.EventHandler(this.DeleteBookButton_Click);
            // 
            // UpdateBookButton
            // 
            this.UpdateBookButton.Image = global::LibraryController.Properties.Resources.editEntry;
            resources.ApplyResources(this.UpdateBookButton, "UpdateBookButton");
            this.UpdateBookButton.Name = "UpdateBookButton";
            this.UpdateBookButton.UseVisualStyleBackColor = true;
            this.UpdateBookButton.Click += new System.EventHandler(this.UpdateBookButton_Click);
            // 
            // CreateBookButton
            // 
            this.CreateBookButton.Image = global::LibraryController.Properties.Resources.addBook;
            resources.ApplyResources(this.CreateBookButton, "CreateBookButton");
            this.CreateBookButton.Name = "CreateBookButton";
            this.CreateBookButton.UseVisualStyleBackColor = true;
            this.CreateBookButton.Click += new System.EventHandler(this.CreateBookButton_Click);
            // 
            // BookDataGridView
            // 
            this.BookDataGridView.AllowUserToAddRows = false;
            this.BookDataGridView.AllowUserToDeleteRows = false;
            this.BookDataGridView.AllowUserToResizeRows = false;
            this.BookDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.BookDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.BookDataGridView, "BookDataGridView");
            this.BookDataGridView.MultiSelect = false;
            this.BookDataGridView.Name = "BookDataGridView";
            this.BookDataGridView.ReadOnly = true;
            this.BookDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.BookDataGridView.RowTemplate.Height = 24;
            this.BookDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // AuthorTab
            // 
            this.AuthorTab.Controls.Add(this.RefreshAuthorButton);
            this.AuthorTab.Controls.Add(this.DeleteAuthorButton);
            this.AuthorTab.Controls.Add(this.UpdateAuthorButton);
            this.AuthorTab.Controls.Add(this.CreateAuthorButton);
            this.AuthorTab.Controls.Add(this.AuthorDataGridView);
            resources.ApplyResources(this.AuthorTab, "AuthorTab");
            this.AuthorTab.Name = "AuthorTab";
            this.AuthorTab.UseVisualStyleBackColor = true;
            // 
            // RefreshAuthorButton
            // 
            this.RefreshAuthorButton.Image = global::LibraryController.Properties.Resources.refreshList;
            resources.ApplyResources(this.RefreshAuthorButton, "RefreshAuthorButton");
            this.RefreshAuthorButton.Name = "RefreshAuthorButton";
            this.RefreshAuthorButton.UseVisualStyleBackColor = true;
            this.RefreshAuthorButton.Click += new System.EventHandler(this.RefreshAuthorButton_Click);
            // 
            // DeleteAuthorButton
            // 
            this.DeleteAuthorButton.Image = global::LibraryController.Properties.Resources.deleteEntry;
            resources.ApplyResources(this.DeleteAuthorButton, "DeleteAuthorButton");
            this.DeleteAuthorButton.Name = "DeleteAuthorButton";
            this.DeleteAuthorButton.UseVisualStyleBackColor = true;
            this.DeleteAuthorButton.Click += new System.EventHandler(this.DeleteAuthorButton_Click);
            // 
            // UpdateAuthorButton
            // 
            this.UpdateAuthorButton.Image = global::LibraryController.Properties.Resources.editEntry;
            resources.ApplyResources(this.UpdateAuthorButton, "UpdateAuthorButton");
            this.UpdateAuthorButton.Name = "UpdateAuthorButton";
            this.UpdateAuthorButton.UseVisualStyleBackColor = true;
            this.UpdateAuthorButton.Click += new System.EventHandler(this.UpdateAuthorButton_Click);
            // 
            // CreateAuthorButton
            // 
            this.CreateAuthorButton.Image = global::LibraryController.Properties.Resources.addAuthor;
            resources.ApplyResources(this.CreateAuthorButton, "CreateAuthorButton");
            this.CreateAuthorButton.Name = "CreateAuthorButton";
            this.CreateAuthorButton.UseVisualStyleBackColor = true;
            this.CreateAuthorButton.Click += new System.EventHandler(this.CreateAuthorButton_Click);
            // 
            // AuthorDataGridView
            // 
            this.AuthorDataGridView.AllowUserToAddRows = false;
            this.AuthorDataGridView.AllowUserToDeleteRows = false;
            this.AuthorDataGridView.AllowUserToResizeColumns = false;
            this.AuthorDataGridView.AllowUserToResizeRows = false;
            this.AuthorDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.AuthorDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.AuthorDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.AuthorDataGridView, "AuthorDataGridView");
            this.AuthorDataGridView.MultiSelect = false;
            this.AuthorDataGridView.Name = "AuthorDataGridView";
            this.AuthorDataGridView.ReadOnly = true;
            this.AuthorDataGridView.RowTemplate.Height = 24;
            this.AuthorDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // UserTab
            // 
            this.UserTab.Controls.Add(this.RefreshUserButton);
            this.UserTab.Controls.Add(this.CheckInButton);
            this.UserTab.Controls.Add(this.CheckOutButton);
            this.UserTab.Controls.Add(this.DeleteUserButton);
            this.UserTab.Controls.Add(this.CreateUserButton);
            this.UserTab.Controls.Add(this.UpdateUserButton);
            this.UserTab.Controls.Add(this.UserDataGridView);
            resources.ApplyResources(this.UserTab, "UserTab");
            this.UserTab.Name = "UserTab";
            this.UserTab.UseVisualStyleBackColor = true;
            // 
            // RefreshUserButton
            // 
            this.RefreshUserButton.Image = global::LibraryController.Properties.Resources.refreshList;
            resources.ApplyResources(this.RefreshUserButton, "RefreshUserButton");
            this.RefreshUserButton.Name = "RefreshUserButton";
            this.RefreshUserButton.UseVisualStyleBackColor = true;
            this.RefreshUserButton.Click += new System.EventHandler(this.RefreshUserButton_Click);
            // 
            // CheckInButton
            // 
            this.CheckInButton.Image = global::LibraryController.Properties.Resources.box;
            resources.ApplyResources(this.CheckInButton, "CheckInButton");
            this.CheckInButton.Name = "CheckInButton";
            this.CheckInButton.UseVisualStyleBackColor = true;
            this.CheckInButton.Click += new System.EventHandler(this.CheckInButton_Click);
            // 
            // CheckOutButton
            // 
            this.CheckOutButton.Image = global::LibraryController.Properties.Resources.checkout;
            resources.ApplyResources(this.CheckOutButton, "CheckOutButton");
            this.CheckOutButton.Name = "CheckOutButton";
            this.CheckOutButton.UseVisualStyleBackColor = true;
            this.CheckOutButton.Click += new System.EventHandler(this.CheckOutButton_Click);
            // 
            // DeleteUserButton
            // 
            this.DeleteUserButton.Image = global::LibraryController.Properties.Resources.deleteEntry;
            resources.ApplyResources(this.DeleteUserButton, "DeleteUserButton");
            this.DeleteUserButton.Name = "DeleteUserButton";
            this.DeleteUserButton.UseVisualStyleBackColor = true;
            this.DeleteUserButton.Click += new System.EventHandler(this.DeleteUserButton_Click);
            // 
            // CreateUserButton
            // 
            this.CreateUserButton.Image = global::LibraryController.Properties.Resources.addUser;
            resources.ApplyResources(this.CreateUserButton, "CreateUserButton");
            this.CreateUserButton.Name = "CreateUserButton";
            this.CreateUserButton.UseVisualStyleBackColor = true;
            this.CreateUserButton.Click += new System.EventHandler(this.CreateUserButton_Click);
            // 
            // UpdateUserButton
            // 
            this.UpdateUserButton.Image = global::LibraryController.Properties.Resources.editEntry;
            resources.ApplyResources(this.UpdateUserButton, "UpdateUserButton");
            this.UpdateUserButton.Name = "UpdateUserButton";
            this.UpdateUserButton.UseVisualStyleBackColor = true;
            this.UpdateUserButton.Click += new System.EventHandler(this.UpdateUserButton_Click);
            // 
            // UserDataGridView
            // 
            this.UserDataGridView.AllowUserToAddRows = false;
            this.UserDataGridView.AllowUserToDeleteRows = false;
            this.UserDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.UserDataGridView, "UserDataGridView");
            this.UserDataGridView.MultiSelect = false;
            this.UserDataGridView.Name = "UserDataGridView";
            this.UserDataGridView.ReadOnly = true;
            this.UserDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // StatisticsTab
            // 
            this.StatisticsTab.Controls.Add(this.ChartDateLabel);
            this.StatisticsTab.Controls.Add(this.ChartDateComboBox);
            this.StatisticsTab.Controls.Add(this.Chart);
            resources.ApplyResources(this.StatisticsTab, "StatisticsTab");
            this.StatisticsTab.Name = "StatisticsTab";
            this.StatisticsTab.UseVisualStyleBackColor = true;
            this.StatisticsTab.Click += new System.EventHandler(this.StatisticsTab_Click);
            this.StatisticsTab.Enter += new System.EventHandler(this.RefreshEverything);
            // 
            // ChartDateLabel
            // 
            resources.ApplyResources(this.ChartDateLabel, "ChartDateLabel");
            this.ChartDateLabel.Name = "ChartDateLabel";
            // 
            // ChartDateComboBox
            // 
            this.ChartDateComboBox.FormattingEnabled = true;
            resources.ApplyResources(this.ChartDateComboBox, "ChartDateComboBox");
            this.ChartDateComboBox.Name = "ChartDateComboBox";
            // 
            // Chart
            // 
            this.Chart.BackColor = System.Drawing.SystemColors.Control;
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.Chart.ChartAreas.Add(chartArea1);
            legend1.AutoFitMinFontSize = 10;
            legend1.Name = "Legend1";
            this.Chart.Legends.Add(legend1);
            resources.ApplyResources(this.Chart, "Chart");
            this.Chart.Name = "Chart";
            series1.ChartArea = "ChartArea1";
            series1.CustomProperties = "PixelPointWidth=20, DrawSideBySide=True, EmptyPointValue=Zero, PointWidth=1, Labe" +
    "lStyle=Bottom";
            series1.EmptyPointStyle.IsValueShownAsLabel = true;
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.Points.Add(dataPoint5);
            series1.Points.Add(dataPoint6);
            series1.Points.Add(dataPoint7);
            series1.Points.Add(dataPoint8);
            series1.Points.Add(dataPoint9);
            series1.Points.Add(dataPoint10);
            series1.Points.Add(dataPoint11);
            series1.Points.Add(dataPoint12);
            this.Chart.Series.Add(series1);
            // 
            // AuthorID
            // 
            this.AuthorID.DataPropertyName = "ID";
            resources.ApplyResources(this.AuthorID, "AuthorID");
            this.AuthorID.Name = "AuthorID";
            this.AuthorID.ReadOnly = true;
            // 
            // AuthorFirstName
            // 
            this.AuthorFirstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AuthorFirstName.DataPropertyName = "FirstName";
            resources.ApplyResources(this.AuthorFirstName, "AuthorFirstName");
            this.AuthorFirstName.Name = "AuthorFirstName";
            this.AuthorFirstName.ReadOnly = true;
            // 
            // AuthorLastName
            // 
            this.AuthorLastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AuthorLastName.DataPropertyName = "LastName";
            resources.ApplyResources(this.AuthorLastName, "AuthorLastName");
            this.AuthorLastName.Name = "AuthorLastName";
            this.AuthorLastName.ReadOnly = true;
            // 
            // AuthorSex
            // 
            this.AuthorSex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AuthorSex.DataPropertyName = "Sex";
            resources.ApplyResources(this.AuthorSex, "AuthorSex");
            this.AuthorSex.Name = "AuthorSex";
            this.AuthorSex.ReadOnly = true;
            // 
            // AuthorBirthDate
            // 
            this.AuthorBirthDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AuthorBirthDate.DataPropertyName = "BirthDate";
            resources.ApplyResources(this.AuthorBirthDate, "AuthorBirthDate");
            this.AuthorBirthDate.Name = "AuthorBirthDate";
            this.AuthorBirthDate.ReadOnly = true;
            // 
            // BookID
            // 
            this.BookID.DataPropertyName = "ID";
            resources.ApplyResources(this.BookID, "BookID");
            this.BookID.Name = "BookID";
            this.BookID.ReadOnly = true;
            // 
            // BookTitle
            // 
            this.BookTitle.DataPropertyName = "Title";
            resources.ApplyResources(this.BookTitle, "BookTitle");
            this.BookTitle.Name = "BookTitle";
            this.BookTitle.ReadOnly = true;
            // 
            // BookAuthor
            // 
            this.BookAuthor.DataPropertyName = "Author";
            resources.ApplyResources(this.BookAuthor, "BookAuthor");
            this.BookAuthor.Name = "BookAuthor";
            this.BookAuthor.ReadOnly = true;
            // 
            // BookAuthorID
            // 
            this.BookAuthorID.DataPropertyName = "AuthorID";
            resources.ApplyResources(this.BookAuthorID, "BookAuthorID");
            this.BookAuthorID.Name = "BookAuthorID";
            this.BookAuthorID.ReadOnly = true;
            // 
            // BookUniqueNumber
            // 
            this.BookUniqueNumber.DataPropertyName = "UniqueNumber";
            resources.ApplyResources(this.BookUniqueNumber, "BookUniqueNumber");
            this.BookUniqueNumber.Name = "BookUniqueNumber";
            this.BookUniqueNumber.ReadOnly = true;
            // 
            // BookPageCount
            // 
            this.BookPageCount.DataPropertyName = "PageCount";
            resources.ApplyResources(this.BookPageCount, "BookPageCount");
            this.BookPageCount.Name = "BookPageCount";
            this.BookPageCount.ReadOnly = true;
            // 
            // BookDatePublished
            // 
            this.BookDatePublished.DataPropertyName = "DateCreated";
            resources.ApplyResources(this.BookDatePublished, "BookDatePublished");
            this.BookDatePublished.Name = "BookDatePublished";
            this.BookDatePublished.ReadOnly = true;
            // 
            // BookIsTaken
            // 
            this.BookIsTaken.DataPropertyName = "IsTaken";
            resources.ApplyResources(this.BookIsTaken, "BookIsTaken");
            this.BookIsTaken.Name = "BookIsTaken";
            this.BookIsTaken.ReadOnly = true;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.managerToolStripMenuItem,
            this.settingsToolStripMenuItem});
            resources.ApplyResources(this.menuStrip, "menuStrip");
            this.menuStrip.Name = "menuStrip";
            // 
            // managerToolStripMenuItem
            // 
            this.managerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.citiesToolStripMenuItem});
            this.managerToolStripMenuItem.Name = "managerToolStripMenuItem";
            resources.ApplyResources(this.managerToolStripMenuItem, "managerToolStripMenuItem");
            this.managerToolStripMenuItem.Click += new System.EventHandler(this.managerToolStripMenuItem1_Click);
            // 
            // citiesToolStripMenuItem
            // 
            this.citiesToolStripMenuItem.Name = "citiesToolStripMenuItem";
            resources.ApplyResources(this.citiesToolStripMenuItem, "citiesToolStripMenuItem");
            this.citiesToolStripMenuItem.Click += new System.EventHandler(this.citiesToolStripMenuItem1_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageToolStripMenuItem});
            resources.ApplyResources(this.settingsToolStripMenuItem, "settingsToolStripMenuItem");
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishToolStripMenuItem,
            this.българскиToolStripMenuItem});
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            resources.ApplyResources(this.languageToolStripMenuItem, "languageToolStripMenuItem");
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            resources.ApplyResources(this.englishToolStripMenuItem, "englishToolStripMenuItem");
            this.englishToolStripMenuItem.Click += new System.EventHandler(this.EnglishToolStripMenuItem_Click);
            // 
            // българскиToolStripMenuItem
            // 
            this.българскиToolStripMenuItem.Name = "българскиToolStripMenuItem";
            resources.ApplyResources(this.българскиToolStripMenuItem, "българскиToolStripMenuItem");
            this.българскиToolStripMenuItem.Click += new System.EventHandler(this.BulgarianToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.TabControl.ResumeLayout(false);
            this.BookTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BookDataGridView)).EndInit();
            this.AuthorTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AuthorDataGridView)).EndInit();
            this.UserTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UserDataGridView)).EndInit();
            this.StatisticsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage BookTab;
        private System.Windows.Forms.TabPage AuthorTab;
        private System.Windows.Forms.TabPage UserTab;
        private System.Windows.Forms.Button DeleteAuthorButton;
        private System.Windows.Forms.Button UpdateAuthorButton;
        private System.Windows.Forms.Button CreateAuthorButton;
        private System.Windows.Forms.DataGridView AuthorDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorBirthDate;
        private System.Windows.Forms.Button RefreshAuthorButton;
        private System.Windows.Forms.DataGridView BookDataGridView;
        private System.Windows.Forms.Button RefreshBookButton;
        private System.Windows.Forms.Button DeleteBookButton;
        private System.Windows.Forms.Button UpdateBookButton;
        private System.Windows.Forms.Button CreateBookButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookAuthor;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookAuthorID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookUniqueNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookPageCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookDatePublished;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookIsTaken;
        private System.Windows.Forms.DataGridView UserDataGridView;
        private System.Windows.Forms.Button DeleteUserButton;
        private System.Windows.Forms.Button CreateUserButton;
        private System.Windows.Forms.Button UpdateUserButton;
        private System.Windows.Forms.Button CheckInButton;
        private System.Windows.Forms.Button CheckOutButton;
        private System.Windows.Forms.Button RefreshUserButton;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem българскиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem managerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem citiesToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TabPage StatisticsTab;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart;
        private System.Windows.Forms.ComboBox ChartDateComboBox;
        private System.Windows.Forms.Label ChartDateLabel;
    }
}


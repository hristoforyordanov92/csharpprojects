﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is ConsoleApplication02.");
            Console.WriteLine("Current date is " + DateTime.Now.Date.ToShortDateString() + "\n");

            Console.Write("Enter first and last name: ");
            string name = Console.ReadLine();
            Console.Write("Enter age: ");
            string age = Console.ReadLine();
            Console.Write("Enter gender/sex: ");
            string sex = Console.ReadLine();
            Console.Write("Enter eye color: ");
            string eyes = Console.ReadLine();

            Console.WriteLine("\nYour name is " + name + ". You are " + age + " years old, " + sex + ", with " + eyes + " eyes.\n");
            //Just testing it
            Console.Beep();
            Console.Read();
        }
    }
}

SELECT * FROM [User];

SELECT * FROM [User]
WHERE FirstName = 'Ivan';

SELECT * FROM [User]
WHERE BirthDate > '1990-01-01';

UPDATE [User]
SET BirthDate = '1995-05-21'
WHERE ID = 1;

SELECT * FROM [BankAccount];

SELECT * FROM [BankAccount]
WHERE UserID = 1;

UPDATE [BankAccount]
SET Amount = 5000
WHERE ID = 1;

DELETE FROM [BankAccount]
WHERE ID = 1;

DELETE FROM [BankAccount]
WHERE UserID = 1;

DELETE FROM [User]
WHERE ID = 1;
﻿using System;

class Book
{
    private string title;
    private string author;
    private short published;
    private short numberOfPages;

    public string Title
    {
        get { return title; }
        set { title = value; }
    }
    public string Author
    {
        get { return author; }
        set { author = value; }
    }
    public short Published
    {
        get { return published; }
        set { published = value; }
    }
    public short NumberOfPages
    {
        get { return numberOfPages; }
        set { numberOfPages = value; }
    }

    public void Print(short pageNumber)
    {
        Console.WriteLine($"The page number {pageNumber} will be printed out."); //Is this what I'm supposed to do?
    }
}
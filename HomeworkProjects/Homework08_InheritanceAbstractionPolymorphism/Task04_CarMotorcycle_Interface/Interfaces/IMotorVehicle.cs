﻿interface IMotorVehicle
{
    string Name { get; set; }
    short ManifactureYear { get; set; }
    string RegistrationNumber { get; set; }
    string Engine { get; set; } //I don't know what "двигател" needs me to add to this interface, so this is what i did.

    void Drive(int speedKmH);
    short GetYears();
}
﻿using System;

class Motorcycle : IMotorVehicle
{
    //IMotorVehicle implementation
    public string Name { get; set; }
    public short ManifactureYear { get; set; }
    public string RegistrationNumber { get; set; }
    public string Engine { get; set; }

    public void Drive(int speedKmH)
    {
        Console.WriteLine($"{Name} is driving with {speedKmH} km/h.");
    }

    public short GetYears()
    {
        Console.WriteLine($"The motorcycle is {DateTime.Now.Year - ManifactureYear} years old.");
        return (short)(DateTime.Now.Year - ManifactureYear);
    }
}
﻿using System;

abstract class MotorVehicle
{
    public string Name { get; set; }
    public short ManifactureYear { get; set; }
    public string RegistrationNumber { get; set; }
    public string Engine { get; set; }

    public void Drive(int speedKmH)
    {
        Console.WriteLine($"{Name} is driving with {speedKmH} km/h.");
    }
    public abstract short GetYears();
}
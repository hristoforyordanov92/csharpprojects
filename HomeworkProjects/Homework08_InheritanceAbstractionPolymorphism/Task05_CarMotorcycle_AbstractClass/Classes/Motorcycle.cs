﻿using System;

class Motorcycle : MotorVehicle
{
    public override short GetYears()
    {
        Console.WriteLine($"The motorcycle is {DateTime.Now.Year - ManifactureYear} years old.");
        return (short)(DateTime.Now.Year - ManifactureYear);
    }
}
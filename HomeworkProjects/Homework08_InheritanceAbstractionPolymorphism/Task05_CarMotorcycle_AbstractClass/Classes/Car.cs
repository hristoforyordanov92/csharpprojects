﻿using System;

class Car : MotorVehicle
{
    public byte NumberOfWheels { get; set; }

    public override short GetYears()
    {
        Console.WriteLine($"The car is {DateTime.Now.Year - ManifactureYear} years old.");
        return (short)(DateTime.Now.Year - ManifactureYear);
    }

    public void StartTurbo()
    {
        Console.WriteLine($"{Name} has started its turbo!");
    }
}
﻿using System;

class SchoolRegister
{
    private string teacherName;
    private short published;
    private short numberOfStudents;
    private short numberOfPages;

    public string TeacherName
    {
        get { return teacherName; }
        set { teacherName = value; }
    }
    public short Published
    {
        get { return published; }
        set { published = value; }
    }
    public short NumberOfStudents
    {
        get { return numberOfStudents; }
        set { numberOfStudents = value; }
    }
    public short NumberOfPages
    {
        get { return numberOfPages; }
        set { numberOfPages = value; }
    }

    //I could make it a double return type, but task doesn't require it.
    public void PrintAnnualGrade(byte[] grades)
    {
        double sum = 0;
        for (int i = 0; i < grades.Length; i++)
        {
            sum += grades[i];
        }
        sum /= grades.Length;
        Console.WriteLine($"The avarage grade is {sum:F2}.");
    }
}
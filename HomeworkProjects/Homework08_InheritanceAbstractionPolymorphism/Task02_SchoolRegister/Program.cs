﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02_SchoolRegister
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] grades = new byte[] { 4, 5, 6, 3, 2, 5, 4, 6, 3 };
            SchoolRegister sc = new SchoolRegister()
            {
                NumberOfPages = 20,
                NumberOfStudents = 25,
                Published = 2017,
                TeacherName = "Ivan Ivanov"
            };

            sc.PrintAnnualGrade(grades);
        }
    }
}

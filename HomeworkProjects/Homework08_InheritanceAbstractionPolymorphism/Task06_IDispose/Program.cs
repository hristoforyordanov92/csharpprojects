﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06_IDispose
{
    class Program
    {
        static void Main(string[] args)
        {
            TestClass tc = new TestClass();

            tc.Dispose();

            Console.WriteLine(tc.X);
        }
    }
}

﻿using System;
using System.Collections;

namespace Task06_IComparer
{
    /*
     * KINDA
     * COPY-PASTED
     * CODE,
     * BUT
     * I
     * TRIED
     * TO
     * WRITE
     * IT
     * MYSELF
     */
    class Person : IComparable
    {
        private string name;
        private int age;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        //This is supposed to be the default sorting order
        public int CompareTo(object obj)
        {
            Person person = (Person)obj;
            return String.Compare(Name, person.Name);
        }

        //This is supposed to sort by name in a descending order
        private class SortNameDescendingClass : IComparer
        {
            public int Compare(object person1, object person2)
            {
                Person p1 = (Person)person1;
                Person p2 = (Person)person2;
                return String.Compare(p2.Name, p1.Name);
            }
        }
        public static IComparer SortNameDescending()
        {
            return new SortNameDescendingClass();
        }

        //This is supposed to sort by age in an ascending order
        private class SortAgeAscendingClass : IComparer
        {
            public int Compare(object person1, object person2)
            {
                Person p1 = (Person)person1;
                Person p2 = (Person)person2;

                if (p1.Age > p2.Age)
                {
                    return 1;
                }
                else if (p2.Age > p1.Age)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static IComparer SortAgeAscending()
        {
            return new SortAgeAscendingClass();
        }

        //This is supposed to sort by age in a descending order
        private class SortAgeDescendingClass : IComparer
        {
            public int Compare(object person1, object person2)
            {
                Person p1 = (Person)person1;
                Person p2 = (Person)person2;

                if (p1.Age > p2.Age)
                {
                    return -1;
                }
                else if (p2.Age > p1.Age)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static IComparer SortAgeDescending()
        {
            return new SortAgeDescendingClass();
        }
    }
}
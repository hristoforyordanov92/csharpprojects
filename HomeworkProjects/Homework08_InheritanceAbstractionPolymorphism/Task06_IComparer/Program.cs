﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06_IComparer
{
    class Program
    {
        static void Main(string[] args)
        {
            //IComparableExampleCar();
            IComparableExamplePerson();
        }

        //Kinda copy-pasted code, but i wrote it myself
        public static void IComparableExamplePerson()
        {
            Person[] people = new Person[5]
            {
                new Person("Forko", 25),
                new Person("Ivan", 30),
                new Person("Gergana", 35),
                new Person("Emil", 10),
                new Person("Hristo", 15)
            };

            Console.WriteLine("Unsorted");
            PrintArray(people);

            Array.Sort(people);

            Console.WriteLine("Sorted default");
            PrintArray(people);

            Array.Sort(people, Person.SortNameDescending());

            Console.WriteLine("Sorted Name Descending");
            PrintArray(people);

            Array.Sort(people, Person.SortAgeAscending());

            Console.WriteLine("Sorted Age Ascending");
            PrintArray(people);

            Array.Sort(people, Person.SortAgeDescending());

            Console.WriteLine("Sorted Age Descending");
            PrintArray(people);
        }

        //Print Person array method
        public static void PrintArray(Person[] array)
        {
            foreach (var person in array)
            {
                Console.WriteLine(person.Name + "\t\t" + person.Age);
            }
            Console.WriteLine();
        }

        //None of this code is mine
        public static void IComparableExampleCar()
        {
            // Create an arary of car objects.      
            Car[] arrayOfCars = new Car[6]
            {
            new Car("Ford",1992),
            new Car("Fiat",1988),
            new Car("Buick",1932),
            new Car("Ford",1932),
            new Car("Dodge",1999),
            new Car("Honda",1977)
            };

            // Write out a header for the output.
            Console.WriteLine("Array - Unsorted\n");

            foreach (Car c in arrayOfCars)
                Console.WriteLine(c.Make + "\t\t" + c.Year);

            // Demo IComparable by sorting array with "default" sort order.
            Array.Sort(arrayOfCars);
            Console.WriteLine("\nArray - Sorted by Make (Ascending - IComparable)\n");

            foreach (Car c in arrayOfCars)
                Console.WriteLine(c.Make + "\t\t" + c.Year);

            // Demo ascending sort of numeric value with IComparer.
            Array.Sort(arrayOfCars, Car.SortYearAscending());
            Console.WriteLine("\nArray - Sorted by Year (Ascending - IComparer)\n");

            foreach (Car c in arrayOfCars)
                Console.WriteLine(c.Make + "\t\t" + c.Year);

            // Demo descending sort of string value with IComparer.
            Array.Sort(arrayOfCars, Car.SortMakeDescending());
            Console.WriteLine("\nArray - Sorted by Make (Descending - IComparer)\n");

            foreach (Car c in arrayOfCars)
                Console.WriteLine(c.Make + "\t\t" + c.Year);

            // Demo descending sort of numeric value using IComparer.
            Array.Sort(arrayOfCars, Car.SortYearDescending());
            Console.WriteLine("\nArray - Sorted by Year (Descending - IComparer)\n");

            foreach (Car c in arrayOfCars)
                Console.WriteLine(c.Make + "\t\t" + c.Year);
        }
    }
}
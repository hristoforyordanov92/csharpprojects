﻿abstract class BookBase //Don't even try to instantiate it!
{
    private short published;
    private short numberOfPages;

    public short Published
    {
        get { return published; }
        set { published = value; }
    }
    public short NumberOfPages
    {
        get { return numberOfPages; }
        set { numberOfPages = value; }
    }
}
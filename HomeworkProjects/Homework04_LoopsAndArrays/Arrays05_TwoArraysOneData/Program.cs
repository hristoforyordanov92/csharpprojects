﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays05_TwoArraysOneData
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[10];
            int[] b = new int[10];

            Random rnd = new Random();

            //fill those arrays with some fake data
            for (int i = 0; i < 10; i++)
            {
                a[i] = rnd.Next(0, 20);
                b[i] = rnd.Next(0, 20);
            }

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < b.Length; j++)
                {
                    if (a[i].Equals(b[j]))
                    {
                        //choose your output method. the first one looks better imho
                        Console.WriteLine("a[{0}] = b[{1}] = {2}", i, j, a[i]);
                        //Console.WriteLine("Array \'a\' at index {0} is equals to array \'b\' at index {1}. Value is {2}", i, j, a[i]);
                    }
                }
            }
        }
    }
}

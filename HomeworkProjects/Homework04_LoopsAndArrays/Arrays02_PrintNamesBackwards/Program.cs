﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays02_PrintNamesBackwards
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Dr. Radeva", "Boiko Borisov", "Delqn Peevski", "Orhan Mudar", "Fiki" };
            string[] namesBackwards = new string[5];

            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine(names[i]);
            }
            Console.WriteLine();

            for (int i = 0; i < names.Length; i++)
            {
                namesBackwards[i] = names[names.Length - i - 1];
                Console.WriteLine(names[names.Length - i - 1]);
            }
            Console.WriteLine();

            ////A "for" loop to print the namesBackwards array, but the task doesn't require it :(
            ////uncomment for more fun!
            //Console.WriteLine();
            //for (int i = 0; i < namesBackwards.Length; i++)
            //{
            //    Console.WriteLine(namesBackwards[i]);
            //}
            //Console.WriteLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops01_Print1toN
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("N: ");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.Write("{0,4}", i);
                if (i % 20 == 0)
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine("\n");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops02_Print1toN_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("N: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Numbers that can be divided by 3 AND 7 will be missing from the table below.");
            for (int i = 1; i <= n; i++)
            {
                if ((i % 3 != 0) || (i % 7 != 0))
                {
                    Console.Write("{0,4}", i);
                }
                else
                {
                    Console.Write("    ");
                }

                if (i == n)
                {
                    Console.WriteLine(".\n");
                }

                if (i % 20 == 0)
                {
                    Console.WriteLine();
                }
            }
        }
    }
}

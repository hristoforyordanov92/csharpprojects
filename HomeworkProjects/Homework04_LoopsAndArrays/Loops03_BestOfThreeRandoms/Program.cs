﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops03_BestOfThreeRandoms
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;
            int max = Int32.MinValue;
            Random rnd = new Random();

            for (int i = 0; i < 3; i++)
            {
                number = rnd.Next(1, 100);
                Console.WriteLine($"Number {i + 1} = {number}");
                if (number > max)
                {
                    max = number;
                }
            }
            Console.WriteLine($"The highest number is {max}.");
            Console.WriteLine();

            ////Old solution WITHOUT a loop

            //int a, b, c, temp;
            //char letter = 'A';
            //Random rnd = new Random();
            //a = rnd.Next(0, 100);
            //b = rnd.Next(0, 100);
            //c = rnd.Next(0, 100);
            //Console.WriteLine("A = {0}", a);
            //Console.WriteLine("B = {0}", b);
            //Console.WriteLine("C = {0}", c);
            //temp = a;
            //if (b > temp)
            //{
            //    letter = 'B';
            //    temp = b;
            //}
            //if (c > temp)
            //{
            //    letter = 'C';
            //    temp = c;
            //}
            //Console.WriteLine("{0} is the highest number with a value of {1}.", letter, temp);
        }
    }
}

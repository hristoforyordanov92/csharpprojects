﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops05_Months
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number of the month: ");
            int a = int.Parse(Console.ReadLine());

            string position = "";
            string month = "";

            switch (a)
            {
                case 1:
                    month = "January";
                    position = "1st";
                    break;
                case 2:
                    month = "February";
                    position = "2nd";
                    break;
                case 3:
                    month = "March";
                    position = "3rd";
                    break;
                case 4:
                    month = "April";
                    position = "4th";
                    break;
                case 5:
                    month = "May";
                    position = "5th";
                    break;
                case 6:
                    month = "June";
                    position = "6th";
                    break;
                case 7:
                    month = "July";
                    position = "7th";
                    break;
                case 8:
                    month = "August";
                    position = "8th";
                    break;
                case 9:
                    month = "September";
                    position = "9th";
                    break;
                case 10:
                    month = "October";
                    position = "10th";
                    break;
                case 11:
                    month = "November";
                    position = "11th";
                    break;
                case 12:
                    month = "December";
                    position = "12th";
                    break;
            }

            Console.WriteLine("The {0} month is {1}.", position, month);
        }
    }
}

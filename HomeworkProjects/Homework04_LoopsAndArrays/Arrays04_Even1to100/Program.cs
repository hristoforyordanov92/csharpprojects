﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays04_Even1to100
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[50];
            int index = 0;
            for (int i = 1; i <= 100; i++)
            {
                if (i % 2 == 0)
                {
                    numbers[index] = i;
                    index++;
                }
            }

            for (int i = 0; i < numbers.Length; i++)
            {
                //stop printing numbers if the rest of the array is untouched by the loop above
                if (numbers[i] == 0)
                {
                    break;
                }

                if (i % 10 == 0)
                {
                    Console.WriteLine();
                }

                Console.Write("{0,4}", numbers[i]);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}

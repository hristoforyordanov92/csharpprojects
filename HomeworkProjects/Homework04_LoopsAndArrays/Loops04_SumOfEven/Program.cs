﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops04_SumOfEven
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            for (int i = 1; i <= 100; i++)
            {
                if (i % 2 == 0)
                {
                    sum += i;
                }
            }
            Console.WriteLine("The sum of all even numbers from 1 to 100 is {0}.", sum);
            Console.WriteLine();

            ////check if the program works correctly. not a really good check though :/
            //int sum2 = 0;
            //string str = "";
            //for (int i = 1; i <= 100; i++)
            //{
            //    if (i % 2 == 0)
            //    {
            //        str += i + " + ";
            //        sum2 += i;
            //    }
            //    if (i % 10 == 0)
            //    {
            //        str = str.Remove(str.Length - 3);
            //        str += " = " + sum2;
            //        Console.WriteLine(str);
            //        str = "";
            //        sum2 = 0;
            //    }
            //}
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays03_BoringArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[20];

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = i + 1;
                Console.Write("{0,3}", numbers[i]);
            }
            Console.WriteLine();

            //"Change the 10th element with the value of 0." 
            //Nice try tricking me into thinking the 10th element is numbers[10]. Maybe next time.
            numbers[9] = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write("{0,3}", numbers[i]);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}

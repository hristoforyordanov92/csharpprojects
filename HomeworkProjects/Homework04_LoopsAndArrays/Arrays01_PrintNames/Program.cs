﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays01_PrintNames
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Mara Otvarachkata", "Suzanita", "Bachi Kiko", "Goshko Hubaveca", "Jesus" };
            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine(names[i]);
            }
            Console.WriteLine();
        }
    }
}

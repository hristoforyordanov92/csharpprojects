﻿using System;
using System.IO;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        string sourceFile = @"source.txt";
        string outputFile = @"output.txt";

        StringBuilder builder = new StringBuilder();

        StreamReader reader = new StreamReader(sourceFile);
        StreamWriter writer = new StreamWriter(outputFile);

        int lineIndex = 1;

        using (reader)
        using (writer)
        {
            string lineContent = reader.ReadLine();
            while (lineContent != null)
            {
                builder.Append($"{lineIndex}: {lineContent}");
                writer.WriteLine(builder);

                lineContent = reader.ReadLine();
                builder.Clear();
                lineIndex++;
            }
        }
        //There is no guarantee that indexes were added, yet i have a message it's been done.
        //Please, no judge!
        Console.WriteLine("Line indexes have been added!");
    }
}
﻿using System;
using System.IO;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        StringBuilder firstBuilder = new StringBuilder();
        StringBuilder secondBuilder = new StringBuilder();

        int equalLines = 0;
        int differentLines = 0;

        string firstFilePath = @"file1.txt";
        string secondFilePath = @"file2.txt";

        StreamReader firstReader = new StreamReader(firstFilePath);
        StreamReader secondReader = new StreamReader(secondFilePath);

        int currentLine = 1;

        using (firstReader)
        using (secondReader)
        {
            string firstLine = firstReader.ReadLine();
            string secondLine = secondReader.ReadLine();

            while (firstLine != null && secondLine != null)
            {
                if (firstLine.Equals(secondLine))
                {
                    equalLines++;
                    firstBuilder.Append($"{currentLine} ");
                }
                else
                {
                    differentLines++;
                    secondBuilder.Append($"{currentLine} ");
                }

                firstLine = firstReader.ReadLine();
                secondLine = secondReader.ReadLine();
                currentLine++;
            }
        }

        Console.WriteLine($"Lines {firstBuilder}are equal. A total of {equalLines}.");
        Console.WriteLine($"Lines {secondBuilder}are different. A total of {differentLines}.");
        Console.WriteLine();
    }
}
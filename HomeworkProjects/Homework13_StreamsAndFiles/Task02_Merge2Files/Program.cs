﻿using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        string firstFilePath = @"file1.txt";
        string secondFilePath = @"file2.txt";
        string outputFilePath = @"output.txt";

        if (File.Exists(outputFilePath))
        {
            File.Delete(outputFilePath);
        }

        StreamReader reader = new StreamReader(firstFilePath);
        StreamWriter writer = new StreamWriter(outputFilePath, true);

        using (reader)
        using (writer)
        {
            writer.WriteLine("File 1 content:");
            ReadFromWriteTo(reader, firstFilePath, writer);

            writer.WriteLine();

            writer.WriteLine("File 2 content:");
            ReadFromWriteTo(reader, secondFilePath, writer);
        }
        //There is no guarantee that they were merged, yet i have a message it's been done.
        //Please, no judge!
        Console.WriteLine("Files have been merged together!");
    }

    static void ReadFromWriteTo(StreamReader reader, string readFromFilePath, StreamWriter writer)
    {
        reader = new StreamReader(readFromFilePath);

        string lineContent = reader.ReadLine();
        while (lineContent != null)
        {
            writer.WriteLine(lineContent);
            lineContent = reader.ReadLine();
        }
    }
}
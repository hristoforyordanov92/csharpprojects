﻿using System;
using System.Xml.Serialization;

namespace DataSerializer
{
    [Serializable]
    [XmlRoot("root")]
    public class CustomCollection<T>
    {
        [XmlArray("Collection")]
        [XmlArrayItem("Student")]
        public T[] Collection { get; set; }
    }
}
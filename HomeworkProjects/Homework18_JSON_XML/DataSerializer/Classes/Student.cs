﻿using System;
using System.Xml.Serialization;

namespace DataSerializer
{
    [Serializable]
    public class Student
    {
        [XmlElement("firstName")]
        public string FirstName { get; set; }
        [XmlElement("lastName")]
        public string LastName { get; set; }
        [XmlElement("age")]
        public byte Age { get; set; }
        [XmlElement("isMale")]
        public bool IsMale { get; set; }
        [XmlElement("description")]
        public string Description { get; set; }
        [XmlElement("courseGrade")]
        public sbyte CourseGrade { get; set; }

        public static void ColorfulStudentPrint(Student student)
        {
            string gender = "female";
            if (student.IsMale) { gender = "male"; }
            Console.Write(
                $"{student.FirstName,-12}" +
                $"{student.LastName,-12}" +
                $"{student.Age,-5}" +
                $"{gender,-8}");
            if (student.CourseGrade >= 90)
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else if (student.CourseGrade >= 70)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            Console.Write($"{student.CourseGrade + "%",-7}");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"{student.Description,-30}");
        }
    }
}
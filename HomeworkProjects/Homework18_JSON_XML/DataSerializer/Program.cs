﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;

namespace DataSerializer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            string filePath = @"..\..\";

            //Dummy data
            CustomCollection<Student> students = new CustomCollection<Student>()
            {
                Collection = new Student[] {
                    new Student(){FirstName = "Forko", LastName = "Forkov", Age = 15, IsMale = true,  Description = "", CourseGrade = 24},
                    new Student(){FirstName = "Ivcho", LastName = "Ivchev", Age = 16, IsMale = true,  Description = "", CourseGrade = 21}
                }
            };

            //Serialize the dummy data
            SerializeToJSON(filePath + "StudentsCollection.json", students);
            SerializeToXML(filePath + "StudentsCollection.xml", students);

            //Test the manually created json file
            string jsonPath = filePath + "Students.json";
            CustomCollection<Student> jsonExample = DeserializeFromJSON<CustomCollection<Student>>(jsonPath);

            ColorfulConsole.WriteLine("From JSON to Console:", ConsoleColor.Blue);
            ColorfulConsole.WriteLine($"{"First Name",-12}{"Last Name",-12}{"Age",-5}{"Sex",-8}{"Grade",-7}{"Description",-30}", ConsoleColor.White);
            foreach (var student in jsonExample.Collection)
            {
                Student.ColorfulStudentPrint(student);
            }

            Console.WriteLine();
            Console.WriteLine();

            //Test the manually created xml file (i had to recreate it using this program... sorry, it's not very manually created :( ...)
            string xmlPath = filePath + "Students.xml";
            CustomCollection<Student> xmlExample = DeserializeFromXML<CustomCollection<Student>>(xmlPath);

            ColorfulConsole.WriteLine("From XML to Console:", ConsoleColor.Red);
            ColorfulConsole.WriteLine($"{"First Name",-12}{"Last Name",-12}{"Age",-5}{"Sex",-8}{"Grade",-7}{"Description",-30}", ConsoleColor.White);
            foreach (var student in xmlExample.Collection)
            {
                Student.ColorfulStudentPrint(student);
            }

            Console.ReadKey();
            Console.WriteLine();
        }

        static void SerializeToJSON(string fullFilePath, object objectToSerialize)
        {
            var serializedData = JsonConvert.SerializeObject(objectToSerialize, Formatting.Indented);
            File.WriteAllText(fullFilePath, serializedData);
        }

        static T DeserializeFromJSON<T>(string fullFilePath)
        {
            var reader = File.ReadAllText(fullFilePath);
            T returnedObject = (T)JsonConvert.DeserializeObject(reader, typeof(T));

            return returnedObject;
        }

        static void SerializeToXML<T>(string fullFilePath, T objectToSerialize)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StreamWriter writer = new StreamWriter(fullFilePath))
            {
                serializer.Serialize(writer, objectToSerialize);
            }
        }

        static T DeserializeFromXML<T>(string fullFilePath)
        {
            T returnedObject;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StreamReader reader = new StreamReader(fullFilePath))
            {
                returnedObject = (T)serializer.Deserialize(reader);
            }

            return returnedObject;
        }
    }
}
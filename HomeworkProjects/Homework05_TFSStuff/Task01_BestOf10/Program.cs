﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01_BestOf10
{
    class Program
    {
        static void Main(string[] args)
        {
            int highestNumber;
            int[] number = new int[10];
            Random rnd = new Random();

            //some manual value assigning to avoid using two 'for' loops
            number[0] = rnd.Next(0, 100);
            highestNumber = number[0];
            Console.WriteLine("number[0] = {0}", number[0]);

            for (int i = 1; i < number.GetLength(0); i++)
            {
                number[i] = rnd.Next(0, 100);
                Console.WriteLine("number[{0}] = {1}", i, number[i]);
                if (highestNumber < number[i])
                {
                    highestNumber = number[i];
                }
            }
            Console.WriteLine("\nHighest number is {0}.\n", highestNumber);
        }
    }
}

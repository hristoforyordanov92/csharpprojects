﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankFramework;

namespace Mapper
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new BankEntities())
            {
                //UserRepository.Create(ctx,
                //    new User()
                //    {
                //        FirstName = "Test1",
                //        MiddleName = "Test2",
                //        LastName = "Test3",
                //        DateOfBirth = DateTime.Now
                //    });

                //BankAccountRepository.Create(ctx,
                //    new BankAccount()
                //    {
                //        Name = "TestName",
                //        IBAN = "TestIBAN",
                //        Amount = 200M,
                //        UserID = UserRepository.Read(ctx, "Test1").ID,
                //        IsCurrent = true
                //    });

                //BankAccountRepository.Create(ctx,
                //    new BankAccount()
                //    {
                //        Name = "TestName2",
                //        IBAN = "TestIBAN2",
                //        Amount = 400M,
                //        UserID = UserRepository.Read(ctx, "Test1").ID,
                //        IsCurrent = false
                //    });

                //User newUser = UserRepository.Read(ctx, "Test1");

                //newUser.FirstName = "Ihateentityframework";

                //UserRepository.Update(ctx, newUser);

                ////Both UserRepository and BankAccountRepository Delete() methods have an overloaded second function,
                ////to delete by UserID for convenience.

                //BankAccountRepository.Delete(ctx, 16);

                //UserRepository.Delete(ctx, ctx.Users.SingleOrDefault(x => x.FirstName == "Test1").ID);
            }
        }
    }
}
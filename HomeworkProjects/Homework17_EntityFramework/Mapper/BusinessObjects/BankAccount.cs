﻿namespace Mapper
{
    class BankAccount
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string IBAN { get; set; }
        public decimal Amount { get; set; }
        public int UserID { get; set; }
        public bool IsCurrent { get; set; }
    }
}
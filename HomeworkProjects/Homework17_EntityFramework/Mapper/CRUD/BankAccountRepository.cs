﻿using BankFramework;
using System.Linq;

namespace Mapper
{
    static class BankAccountRepository
    {
        public static void Create(BankEntities context, BankAccount bankAccount)
        {
            BankFramework.BankAccount newBankAccount = new BankFramework.BankAccount()
            {
                Name = bankAccount.Name,
                IBAN = bankAccount.IBAN,
                Amount = bankAccount.Amount,
                UserID = bankAccount.UserID,
                IsCurrent = bankAccount.IsCurrent
            };

            context.BankAccounts.Add(newBankAccount);
            context.SaveChanges();
        }

        /// <summary>
        /// Deletes a bank account by the bank account ID.
        /// </summary>
        /// <param name="context">The Entity Framework context.</param>
        /// <param name="id">The ID of the bank account.</param>
        public static void Delete(BankEntities context, int id)
        {
            context.BankAccounts.Remove(context.BankAccounts.SingleOrDefault(x => x.ID == id));
            context.SaveChanges();
        }
        /// <summary>
        /// Deletes a single bank account by the user ID.
        /// </summary>
        /// <param name="context">The Entity Framework context.</param>
        /// <param name="id">The ID of the user who owns the bank account.</param>
        /// <param name="byUserId">If set to 'True', it will use the ID for the ID of the owner.</param>
        public static void Delete(BankEntities context, int id, bool byUserId)
        {
            if (byUserId)
            {
                context.BankAccounts.Remove(context.BankAccounts.SingleOrDefault(x => x.UserID == id));
            }
            context.SaveChanges();
        }

        public static BankAccount Read(BankEntities context, int id)
        {
            BankFramework.BankAccount bankAccount = context.BankAccounts.SingleOrDefault(x => x.ID == id);

            BankAccount returnedBankAccount = new BankAccount()
            {
                ID = bankAccount.ID,
                Name = bankAccount.Name,
                IBAN = bankAccount.IBAN,
                Amount = bankAccount.Amount,
                UserID = bankAccount.UserID,
                IsCurrent = bankAccount.IsCurrent
            };

            return returnedBankAccount;
        }

        public static void Update(BankEntities context, BankAccount bankAccount)
        {
            BankFramework.BankAccount entityBankAccount = context.BankAccounts.SingleOrDefault(x => x.ID == bankAccount.ID);

            entityBankAccount.Name = bankAccount.Name;
            entityBankAccount.IBAN = bankAccount.IBAN;
            entityBankAccount.Amount = bankAccount.Amount;
            entityBankAccount.UserID = bankAccount.UserID;
            entityBankAccount.IsCurrent = bankAccount.IsCurrent;

            context.SaveChanges();
        }
    }
}
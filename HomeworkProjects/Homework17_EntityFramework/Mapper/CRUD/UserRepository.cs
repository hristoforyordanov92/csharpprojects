﻿using BankFramework;
using System.Linq;

namespace Mapper
{
    static class UserRepository
    {
        public static void Create(BankEntities context, User user)
        {
            BankFramework.User newUser = new BankFramework.User()
            {
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                DateOfBirth = user.DateOfBirth
            };

            context.Users.Add(newUser);
            context.SaveChanges();
        }

        public static void Delete(BankEntities context, int id)
        {
            var bankAccounts = context.BankAccounts.Where(x => x.UserID == id);
            foreach (var bankAccount in bankAccounts)
            {
                context.BankAccounts.Remove(bankAccount);
            }
            context.Users.Remove(context.Users.SingleOrDefault(x => x.ID == id));
            context.SaveChanges();
        }
        public static void Delete(BankEntities context, string firstName)
        {
            var userToDelete = context.Users.SingleOrDefault(x => x.FirstName == firstName);

            var bankAccounts = context.BankAccounts.Where(x => x.UserID == userToDelete.ID);
            foreach (var bankAccount in bankAccounts)
            {
                context.BankAccounts.Remove(bankAccount);
            }
            context.Users.Remove(userToDelete);
            context.SaveChanges();
        }

        public static User Read(BankEntities context, int id)
        {
            BankFramework.User user = context.Users.SingleOrDefault(x => x.ID == id);

            User returnedUser = new User()
            {
                ID = user.ID,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                DateOfBirth = user.DateOfBirth
            };

            return returnedUser;
        }
        public static User Read(BankEntities context, string firstName)
        {
            BankFramework.User user = context.Users.SingleOrDefault(x => x.FirstName == firstName);

            User returnedUser = new User()
            {
                ID = user.ID,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                DateOfBirth = user.DateOfBirth
            };

            return returnedUser;
        }

        public static void Update(BankEntities context, User user)
        {
            BankFramework.User entityUser = context.Users.SingleOrDefault(x => x.ID == user.ID);

            entityUser.FirstName = user.FirstName;
            entityUser.MiddleName = user.MiddleName;
            entityUser.LastName = user.LastName;
            entityUser.DateOfBirth = user.DateOfBirth;

            context.SaveChanges();
        }
    }
}
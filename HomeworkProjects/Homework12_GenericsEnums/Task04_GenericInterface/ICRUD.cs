﻿interface ICRUD<T> //not entirely sure what to do here, but i guess this can always be modified later
{
    void Create(T item);
    void Create(T item, int index);

    T Read(int index);

    void Update(T item);

    void Delete(T item);
    void Delete(int index);
}
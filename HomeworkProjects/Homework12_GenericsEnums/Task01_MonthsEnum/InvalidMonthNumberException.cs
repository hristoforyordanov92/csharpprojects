﻿using System;

class InvalidMonthNumberException : Exception
{
    public InvalidMonthNumberException() : base() { }
    public InvalidMonthNumberException(string message) : base(message) { }
}
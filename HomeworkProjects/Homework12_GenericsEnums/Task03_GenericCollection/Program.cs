﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Collection<Person> myFriends = new Collection<Person>(3);

        Console.WriteLine("Current amount of items: " + myFriends.ItemsCount);

        Console.WriteLine("Maximum amount of items: " + myFriends.MaximumItems);

        myFriends.AddItem(new Person("Milen", "Vasilev", 25));
        myFriends.AddItem(new Person("Blagovest", "Hristov", 25));
        myFriends.AddItem(new Person("Daniel", "Karashtranov", 23, 130));
        Console.WriteLine();

        myFriends.PrintItemAt(0);
        myFriends.PrintItemAt(2);
        Console.WriteLine();

        myFriends.PrintCollection();
        Console.WriteLine();

        Console.WriteLine("Current amount of items: " + myFriends.ItemsCount);
        Console.WriteLine();

        myFriends.RemoveItem(1);
        myFriends.PrintCollection();
        Console.WriteLine();
        Console.WriteLine("Current amount of items: " + myFriends.ItemsCount);
        Console.WriteLine();
    }
}
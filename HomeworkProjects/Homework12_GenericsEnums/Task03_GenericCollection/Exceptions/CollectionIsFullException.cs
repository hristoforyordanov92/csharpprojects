﻿using System;

class CollectionIsFullException : Exception
{
    public CollectionIsFullException() : base() { }
    public CollectionIsFullException(string message) : base(message) { }
}
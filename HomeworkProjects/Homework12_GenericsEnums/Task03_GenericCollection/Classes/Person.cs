﻿class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string FullName { get; set; }
    public int Age { get; set; }
    public int IQ { get; set; }

    public Person(string firstName, string lastName) : this(firstName, lastName, 0) { }
    public Person(string firstName, string lastName, int age) : this(firstName, lastName, age, 0) { }
    public Person(string firstName, string lastName, int age, int iq)
    {
        FirstName = firstName;
        LastName = lastName;
        FullName = firstName + " " + lastName;
        Age = age;
        IQ = iq;
    }

    public override string ToString()
    {
        string toStringResult = FullName + " is " + Age + " years old.";
        if (IQ > 0)
        {
            toStringResult = toStringResult.TrimEnd('.') + $" and has {IQ} IQ.";
        }
        return toStringResult;
    }
}
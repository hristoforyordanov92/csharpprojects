﻿using System;
using System.Collections.Generic;

public class Collection<T>
{
    private const int DEFAULT_MAXIMUM_ITEMS = 100;
    private readonly int maximumItems;

    private List<T> items;

    /// <summary>
    /// Gets the number of items currenctly in the collection.
    /// </summary>
    public int ItemsCount
    {
        get
        {
            return items.Count;
        }
    }

    /// <summary>
    /// Gets the maximum allowed items in the collection.
    /// </summary>
    public int MaximumItems
    {
        get
        {
            return maximumItems;
        }
    }

    /// <summary>
    /// Creates a collection of 100 items.
    /// </summary>
    public Collection() : this(DEFAULT_MAXIMUM_ITEMS) { }
    /// <summary>
    /// Creates a collection with a custom size.
    /// </summary>
    /// <param name="maximumItems">The maximum amount of items the collection can hold.</param>
    public Collection(int maximumItems)
    {
        this.maximumItems = maximumItems;
        items = new List<T>();
    }

    /// <summary>
    /// Adds an item to your collection.
    /// </summary>
    /// <param name="item"></param>
    /// <exception cref="CollectionIsFullException"></exception>
    public void AddItem(T item)
    {
        if (items.Count < MaximumItems)
        {
            items.Add(item);
        }
        else
        {
            throw new CollectionIsFullException("Cannot add new item, because the maximum number of items ({}) has been reached.");
        }
    }

    /// <summary>
    /// Removes and returns an item at a specific index from your collection.
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public T RemoveItem(int index)
    {
        T removedItem;
        if (index < items.Count && index >= 0)
        {
            removedItem = items[index];
            items.RemoveAt(index);
            return removedItem;
        }
        else
        {
            throw new ArgumentOutOfRangeException($"Item with the index of {index} does not exist.");
        }
    }

    /// <summary>
    /// Gets an item at a specific index from the collection.
    /// </summary>
    /// <param name="index">The index of the item in the collection.</param>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public T GetItemAt(int index)
    {
        if (index < items.Count && index >= 0)
        {
            return items[index];
        }
        else
        {
            throw new ArgumentOutOfRangeException($"Item with the index of {index} does not exist.");
        }
    }

    /// <summary>
    /// Prints the item in the console.
    /// </summary>
    /// <param name="index">The index of the item in the collection.</param>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public void PrintItemAt(int index)
    {
        if (index < items.Count && index >= 0)
        {
            Console.WriteLine($"Item[{index}] = {items[index].ToString()}");
        }
        else
        {
            throw new ArgumentOutOfRangeException($"Item with the index of {index} does not exist.");
        }
    }

    /// <summary>
    /// Returns an array of your type with all items in the collection.
    /// </summary>
    public T[] GetAllItems()
    {
        T[] listOfItems = new T[items.Count];
        int i = 0;
        foreach (T item in items)
        {
            listOfItems[i++] = item;
        }
        return listOfItems;
    }

    /// <summary>
    /// Prints all items from the collection in the console.
    /// </summary>
    public void PrintCollection()
    {
        for (int i = 0; i < items.Count; i++)
        {
            Console.WriteLine($"Item[{i}] = {items[i].ToString()}");
        }
    }
}
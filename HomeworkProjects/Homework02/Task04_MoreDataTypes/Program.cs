﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04_MoreDataTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            short num1 = -180;
            sbyte num2 = 5;
            long num3 = 9834289023490L;
            float num4 = 7.7f;
            double num5 = 9.802349004d;
            short theNineties = 1999;
            long cleverNumber = 123456789123456789L;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02_StringMadness
{
    class Program
    {
        static void Main(string[] args)
        {
            string myStr = "Hello World";
            Console.WriteLine("myStr = \"{0}\"\n", myStr);

            //2.1
            Console.WriteLine("2.1");
            string hello = myStr.Substring(0, 5);
            string world = myStr.Substring(6, 5);
            Console.WriteLine("First string: {0}\nSecond string: {1}\n", hello, world);

            //2.2
            Console.WriteLine("2.2");
            sbyte length = 10;
            bool equalsToLength = (myStr.Length == length);
            Console.WriteLine("\"{0}\" is {1} symbols long - {2} (it is {3} symbols long)\n", myStr, length, equalsToLength, myStr.Length);

            //2.3
            Console.WriteLine("2.3");
            Console.WriteLine("The index of \'W\' is " + myStr.IndexOf('W') + "\n");

            //2.4
            Console.WriteLine("2.4");
            Console.WriteLine(myStr.ToUpper() + "\n");

            //2.5
            Console.WriteLine("2.5");
            Console.WriteLine("myStr = \"{0}\"\n", myStr);

            //2.6
            Console.WriteLine("2.6");
            myStr = myStr.Replace("World", "Programming");
            Console.WriteLine("myStr = \"{0}\"\n", myStr);
        }
    }
}
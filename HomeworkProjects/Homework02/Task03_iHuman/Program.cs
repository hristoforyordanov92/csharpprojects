﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03_iHuman
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Hristofor Yordanov";
            byte age = 25; //yes, byte, because i'm being optimistic!
            char gender = 'm'; //not genderfluid-friendly
            decimal bankAccountAmount = 9999999999999.999999999m; //not an actual amount!
            char firstLetterOfName = 'H';
            string eyeColor = "blue";
        }
    }
}

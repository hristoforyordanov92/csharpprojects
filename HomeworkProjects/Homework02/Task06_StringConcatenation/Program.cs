﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06_StringConcatenation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the first string: ");
            string firstString = Console.ReadLine();

            Console.Write("Enter the second string: ");
            string secondString = Console.ReadLine();

            string resultString = firstString + "_" + secondString;
            Console.WriteLine("The concatenated string is: {0}", resultString);
        }
    }
}

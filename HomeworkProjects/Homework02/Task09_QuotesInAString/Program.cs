﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task09_QuotesInAString
{
    class Program
    {
        static void Main(string[] args)
        {
            string myStr = "I will read the \"Harry Potter\" book collection.";
            Console.WriteLine(myStr);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task07_RectangleArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the rectangle's side A: ");
            double sideA = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter the rectangle's side B: ");
            double sideB = Convert.ToDouble(Console.ReadLine());

            double result = (sideA * sideB);

            Console.WriteLine("The area of the rectangle is {0} square units.", result);
        }
    }
}

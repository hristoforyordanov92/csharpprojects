﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10_SumOfFour
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Multiple integers
            //int num1, num2, num3, num4, result;
            //Console.Write("Enter first number: ");
            //num1 = Convert.ToInt32(Console.ReadLine());
            //Console.Write("Enter second number: ");
            //num2 = Convert.ToInt32(Console.ReadLine());
            //Console.Write("Enter third number: ");
            //num3 = Convert.ToInt32(Console.ReadLine());
            //Console.Write("Enter fourth number: ");
            //num4 = Convert.ToInt32(Console.ReadLine());
            //result = num1 + num2 + num3 + num4;
            //Console.WriteLine("The sum of the four numbers is {0}.", result);

            //Single integer
            int result = 0;
            Console.Write("Enter first number: ");
            result += Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter second number: ");
            result += Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter third number: ");
            result += Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter fourth number: ");
            result += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("The sum of the four numbers is {0}.", result);
        }
    }
}

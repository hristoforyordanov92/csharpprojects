﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task05_TriangleArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the triangle's side 'a': ");
            int side = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the side's height 'ha': ");
            int height = Convert.ToInt32(Console.ReadLine());

            double result = (double)(side * height) / 2;
            Console.WriteLine("\n\'S\' - triangle's area\n\nS = (a * ha) / 2");
            Console.WriteLine("S = ({0} * {1}) / 2", side, height);
            Console.WriteLine("S = {0} / 2", result * 2);
            Console.WriteLine("S = {0:F2} square units\n", result);
        }
    }
}

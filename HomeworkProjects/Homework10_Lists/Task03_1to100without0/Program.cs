﻿using System;
using System.Collections.Generic;

namespace Task03_1to100without0
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int>();

            for (int i = 1; i < 101; i++)
            {
                numbers.Add(i);
            }

            for (int i = 0; i < numbers.Count; i++)
            {
                if (numbers[i] % 10 == 0)
                {
                    numbers.RemoveAt(i);
                }
            }

            for (int i = 0; i < numbers.Count; i++)
            {
                if (i % 20 == 0)
                {
                    Console.WriteLine();
                }
                Console.Write($"{numbers[i],3}");
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
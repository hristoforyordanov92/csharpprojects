﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Random rnd = new Random();

        LinkedList<Car> carList = new LinkedList<Car>();

        carList.AddLast(new Car("Opel", "Astra", 4, rnd));
        carList.AddLast(new Car("BMW", "M5", 2, rnd));
        carList.AddLast(new Car("Audi", "A8", 4, rnd));
        carList.AddLast(new Car("Ford", "Mondeo", 4, rnd));
        carList.AddLast(new Car("Skoda", "Octavia", 4, rnd));

        foreach (var car in carList)
        {
            car.PrintCarInfo();
        }
    }
}
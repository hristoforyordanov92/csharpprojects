﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Random rnd = new Random();

        List<Car> carList = new List<Car>();

        carList.Add(new Car("Opel", "Astra", 4, rnd));
        carList.Add(new Car("BMW", "M5", 2, rnd));
        carList.Add(new Car("Audi", "A8", 4, rnd));
        carList.Add(new Car("Ford", "Mondeo", 4, rnd));
        carList.Add(new Car("Skoda", "Octavia", 4, rnd));

        foreach (var car in carList)
        {
            car.PrintCarInfo();
        }

        Console.WriteLine();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Car
{
    public string Brand { get; set; }
    public string Model { get; set; }
    public string RegistrationNumber { get; set; }
    public int NumberOfSeats { get; set; }

    public Car(string brand, string model, int numberOfSeats, Random rnd)
    {
        Brand = brand;
        Model = model;
        RegistrationNumber = RegNumberGenerator(rnd);
        NumberOfSeats = numberOfSeats;
    }

    private string RegNumberGenerator(Random rnd)
    {
        string regNumber = "";

        regNumber += (char)(rnd.Next(65, 90)) + "" + (char)(rnd.Next(65, 90)) + " ";
        for (int i = 0; i < 4; i++)
        {
            regNumber += rnd.Next(0, 9);
        }
        regNumber += " " + (char)(rnd.Next(65, 90)) + "" + (char)(rnd.Next(65, 90));

        return regNumber;
    }

    public void PrintCarInfo()
    {
        Console.WriteLine($"{Brand} {Model} with registration number {RegistrationNumber} has {NumberOfSeats} seats.");
    }
}
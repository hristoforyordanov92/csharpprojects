﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03_NumberEqualityMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the first integer: ");
            int x = int.Parse(Console.ReadLine());

            Console.Write("Enter the second integer: ");
            int y = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine($"{x} == {y} -> {IntegerEquality(x, y)}");

            Console.WriteLine();
        }

        static bool IntegerEquality(int x, int y)
        {
            if (x == y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

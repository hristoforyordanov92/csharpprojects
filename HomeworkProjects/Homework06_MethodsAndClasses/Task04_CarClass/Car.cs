﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
    class Car
    {
        private string _brand;
        private string _model;
        private string _registrationNumber;
        private string _color;
        private byte _years;
        private sbyte _seatsNumber;
        private bool _isDiesel;

        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
        public string RegistrationNumber
        {
            get { return _registrationNumber; }
            set { _registrationNumber = value; }
        }
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        public byte Years
        {
            get
            {
                return _years;
            }
            set
            {
                if (value >= 0)
                {
                    _years = value;
                }
                else
                {
                    Console.WriteLine("Invalid car age!");
                    _years = 0;
                }
            }
        }
        public sbyte SeatsNumber
        {
            get
            {
                return _seatsNumber;
            }
            set
            {
                if (value >= 1)
                {
                    _seatsNumber = value;
                }
                else
                {
                    Console.WriteLine("Invalid number of seats! Must have atleast 1 seat.");
                    _seatsNumber = 1;
                }
            }
        }
        public bool IsDiesel
        {
            get { return _isDiesel; }
            set { _isDiesel = value; }
        }

        public Car(string brand, string model, string registrationNumber, string color, byte years, sbyte seatsNumber, bool isDiesel)
        {
            Brand = brand;
            Model = model;
            RegistrationNumber = registrationNumber;
            Color = color;
            Years = years;
            SeatsNumber = seatsNumber;
            IsDiesel = IsDiesel;
        }

        public void GetCarInfo()
        {
            Console.WriteLine($"Brand: {Brand}");
            Console.WriteLine($"Model: {Model}");
            Console.WriteLine($"Registration number: {RegistrationNumber}");
            Console.WriteLine($"Color: {Color}");
            Console.WriteLine($"Car age: {Years}");
            Console.WriteLine($"Number of seats: {SeatsNumber}");
            Console.WriteLine($"Is diesel: {IsDiesel}");
            Console.WriteLine();
        }
    }
}

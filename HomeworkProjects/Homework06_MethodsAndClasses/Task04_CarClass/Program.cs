﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vehicles;

namespace Task04_CarClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car("Opel", "Astra", "ВТ 2301 ВС", "Blue", 24, 4, false);
            Car car2 = new Car("BMW", "5", "СА 2673 ЕС", "Black/Red", 1, 4, false);

            car1.GetCarInfo();
            car2.GetCarInfo();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task05_MotorcycleClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Motorcycle moto1 = new Motorcycle
            {
                Model = "Honda Hornet CB 600 F",
                Color = "White/Red",
                ProductionYear = 2003,
                MaxSpeed = 220
            };

            Motorcycle moto2 = new Motorcycle
            {
                Model = "Honda CBR 1000 Fireblade",
                Color = "White/Black",
                ProductionYear = 2008,
                MaxSpeed = 300
            };

            Motorcycle moto3 = new Motorcycle
            {
                Model = "Simson SR1",
                Color = "Black",
                ProductionYear = 1955,
                MaxSpeed = 50
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task05_MotorcycleClass
{
    class Motorcycle
    {
        public string Model { get; set; }
        public string Color { get; set; }
        public short ProductionYear { get; set; }
        public short MaxSpeed { get; set; }
    }
}

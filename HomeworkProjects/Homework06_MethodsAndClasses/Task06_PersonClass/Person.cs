﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06_PersonClass
{
    public class Person
    {
        private string _name;
        private string _pin; //PIN - Personal Identification Number (EGN)
        private short _yearOfBirth;
        private sbyte _monthOfBirth;
        private sbyte _dayOfBirth;
        private char _sex;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        //This long property actually tries to determinate the data of birth and sex based on the PIN (EGN)
        public string Pin
        {
            get { return _pin; }
            set
            {
                if (value.Length != 10)
                {
                    Console.WriteLine("Invalid PIN number! Must be exactly 10 numbers long.");
                    _pin = "0000000000";
                }
                else
                {
                    _pin = value;

                    DayOfBirth = sbyte.Parse(value.Substring(4, 2));

                    sbyte _pinMonth = sbyte.Parse(value.Substring(2, 2));
                    if (_pinMonth - 40 > 0)
                    {
                        MonthOfBirth = (sbyte)(_pinMonth - 40);
                    }
                    else if (_pinMonth - 20 > 0)
                    {
                        MonthOfBirth = (sbyte)(_pinMonth - 20);
                    }
                    else
                    {
                        MonthOfBirth = _pinMonth;
                    }

                    short _pinYear = (short)(1900 + short.Parse(value.Substring(0, 2)));
                    if (_pinMonth - 40 > 0)
                    {
                        _pinYear += 100;
                    }
                    else if (_pinMonth - 20 > 0)
                    {
                        _pinYear -= 100;
                    }
                    YearOfBirth = _pinYear;

                    sbyte _pinSex = sbyte.Parse(value.Substring(8, 1));
                    if (_pinSex % 2 == 0)
                    {
                        Sex = 'M';
                    }
                    else
                    {
                        Sex = 'F';
                    }
                }
            }
        }
        public short YearOfBirth
        {
            get { return _yearOfBirth; }
            set { _yearOfBirth = value; }
        }
        public sbyte MonthOfBirth
        {
            get { return _monthOfBirth; }
            set
            {
                if (value < 1 || value > 12)
                {
                    Console.WriteLine($"Invalid month! Must be from 1 to 12. ({Name}, {Pin})");
                }
                else
                {
                    _monthOfBirth = value;
                }
            }
        }
        public sbyte DayOfBirth
        {
            get { return _dayOfBirth; }
            set
            {
                if (value < 1 || value > 31)
                {
                    Console.WriteLine("Wrong day input! Day of birth must be from 1 to 31.");
                }
                else
                {
                    _dayOfBirth = value;
                }
            }
        }
        public char Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }

        public void GetPersonalInfo()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"PIN: {Pin}");
            Console.WriteLine($"Born: {DayOfBirth}.{MonthOfBirth}.{YearOfBirth,-7}Sex: {Sex}");
            Console.WriteLine();
        }
    }
}

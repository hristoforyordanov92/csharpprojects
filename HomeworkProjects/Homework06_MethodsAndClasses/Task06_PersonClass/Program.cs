﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06_PersonClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] people = new Person[10];

            for (int i = 0; i < people.GetLength(0); i++)
            {
                people[i] = new Person();
            }

            people[0].Name = "Ivan";
            people[0].Pin = "1231141283";

            people[1].Name = "Ivanka";
            people[1].Pin = "4404244434";

            people[2].Name = "Gergan";
            people[2].Pin = "5525055565";

            people[3].Name = "Gerganka";
            people[3].Pin = "6606066656";

            people[4].Name = "Stoqn";
            people[4].Pin = "0050147707";

            people[5].Name = "Stoqnka";
            people[5].Pin = "8841288878";

            people[6].Name = "Petran";
            people[6].Pin = "9929299989";

            people[7].Name = "Petranka";
            people[7].Pin = "1111111151";

            people[8].Name = "Deqn";
            people[8].Pin = "2212122222";

            people[9].Name = "Deqnka";
            people[9].Pin = "3352133333";

            for (int i = 0; i < people.GetLength(0); i++)
            {
                //Console.WriteLine($"NAME: {people[i].Name,-12}PIN: {people[i].Pin}");
                people[i].GetPersonalInfo();
            }

            Console.WriteLine();
        }
    }
}

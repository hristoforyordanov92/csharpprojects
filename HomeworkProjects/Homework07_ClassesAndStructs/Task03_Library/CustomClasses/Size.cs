﻿class Size
{
    public double Width { get; set; }
    public double Length { get; set; }
    public double Height { get; set; }
}
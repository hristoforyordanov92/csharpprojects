﻿class Library
{
    public Location location;
    public Size size;
    public int StartingYear { get; set; }

    public Library()
    {
        location = new Location();
        size = new Size();
    }
}
﻿class Program
{
    static void Main(string[] args)
    {
        Flower flower = new Flower
        {
            Name = "Flowy",
            Color = "blue",
            Height = 30
        };

        flower.Bloom();
    }
}
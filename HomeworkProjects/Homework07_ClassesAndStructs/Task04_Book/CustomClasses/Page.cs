﻿class Page
{
    public string Content { get; set; }
    public int Index { get; set; }

    public Page() { }
    public Page(int index)
    {
        Index = index;
    }
}
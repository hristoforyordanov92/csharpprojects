﻿using System;

class Book
{
    public string Title { get; set; }
    public int YearOfPublishing { get; set; }
    public string Author { get; set; }
    public int NumberOfPages { get; set; }
    public Page[] pages;

    public Book(string title, int yearOfProduction, string author, int numberOfPages)
    {
        Title = title;
        YearOfPublishing = yearOfProduction;
        Author = author;
        NumberOfPages = numberOfPages;
        pages = new Page[numberOfPages];
        for (int i = 0; i < pages.Length; i++)
        {
            pages[i] = new Page(i + 1);
        }
    }

    public void Print(int indexOfPage)
    {
        Console.WriteLine($"This method will print page with index {pages[indexOfPage - 1].Index}.");
    }
}
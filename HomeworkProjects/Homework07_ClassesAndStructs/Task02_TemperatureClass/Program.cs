﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine(Temperature.CelsiusToFahrenheit(35));
        Console.WriteLine(Temperature.FahrenheitToCelsius(75));
    }
}
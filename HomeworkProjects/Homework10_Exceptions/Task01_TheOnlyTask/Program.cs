﻿using System;

class Program
{
    static void Main(string[] args)
    {
        while (true)
        {
            try
            {
                Console.Write("Enter first number: ");
                int firstNumber = int.Parse(Console.ReadLine());

                Console.Write("Enter second number: ");
                int secondNumber = int.Parse(Console.ReadLine());

                SumNumbers(firstNumber, secondNumber);
                //break; //Commented it out so the application is tester-friendly! A.K.A. endless fun!
            }
            catch (EqualNumbersException ex)
            {
                Console.Write(ex.Message);
                Console.WriteLine(" Please, enter new numbers!\n");
            }
            catch (NegativeNumberException ex)
            {
                Console.Write(ex.Message);
                Console.WriteLine(" Please, enter new numbers!\n");
            }
            catch (FormatException ex)
            {
                Console.Write(ex.Message);
                Console.WriteLine(" Please, enter new numbers!\n");
            }
            catch (OverflowException ex)
            {
                Console.Write(ex.Message);
                Console.WriteLine(" Please, enter new numbers!\n");
            }
        }
    }

    static void SumNumbers(int firstNumber, int secondNumber)
    {
        if (firstNumber == secondNumber)
        {
            throw new EqualNumbersException("Error: Numbers must NOT be equal!");
        }
        else if (firstNumber < 0 || secondNumber < 0)
        {
            throw new NegativeNumberException("Error: Both numbers must be positive!");
        }
        else
        {
            Console.WriteLine($"{firstNumber} + {secondNumber} = {firstNumber + secondNumber}\n");
        }
    }
}
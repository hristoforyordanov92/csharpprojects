﻿using System;

class NegativeNumberException : Exception
{
    public NegativeNumberException() : base() { }
    public NegativeNumberException(string message) : base(message) { }
}
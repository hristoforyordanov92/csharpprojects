﻿using System;

class EqualNumbersException : Exception
{
    public EqualNumbersException() : base() { }
    public EqualNumbersException(string message) : base(message) { }
}
﻿class Fighter : Plane
{
    public int NumberOfRockets { get; set; }

    public Fighter(int maxSpeed, int maxAltitude) : base(maxSpeed, maxAltitude)
    {
        PlaneType = "Fighter plane";
        SpeedReached += MaxSpeedWarning;
        AltitudeReached += MaxAltitudeWarning;
    }
}
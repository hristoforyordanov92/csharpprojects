﻿using System;

class Program
{
    static void Main(string[] args)
    {
        var pass = new Passenger(400, 1400);
        var fight = new Fighter(800, 2400);

        pass.SetAltitude(1500);
        pass.SetSpeed(450);

        Console.WriteLine();

        fight.SetSpeed(900);
        fight.SetAltitude(3000);
    }
}
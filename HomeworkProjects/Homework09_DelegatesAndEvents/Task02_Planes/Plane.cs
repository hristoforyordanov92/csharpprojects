﻿using System;

abstract class Plane
{
    public delegate void PlaneEventHandler();

    public event PlaneEventHandler AltitudeReached;
    public event PlaneEventHandler SpeedReached;

    public string PlaneType { get; set; }
    public int CurrentSpeed { get; set; }
    public int MaximumSpeed { get; set; }
    public int CurrentAltitude { get; set; }
    public int MaximumAltitude { get; set; }

    public Plane(int maxSpeed, int maxAltitude)
    {
        MaximumSpeed = maxSpeed;
        MaximumAltitude = maxAltitude;
    }

    public void SetSpeed(int speed)
    {
        CurrentSpeed = speed;
        if (CurrentSpeed > MaximumSpeed)
        {
            OnSpeedReached();
        }
    }

    public void SetAltitude(int altitude)
    {
        CurrentAltitude = altitude;
        if (CurrentAltitude > MaximumAltitude)
        {
            OnAltitudeReached();
        }
    }

    public void OnSpeedReached()
    {
        SpeedReached?.Invoke();
    }

    public void OnAltitudeReached()
    {
        AltitudeReached?.Invoke();
    }

    public void MaxSpeedWarning()
    {
        Console.WriteLine($"{PlaneType} reached maximum speed! Must not exceed {MaximumSpeed} km/h. Current speed: {CurrentSpeed} km/h.");
    }

    public void MaxAltitudeWarning()
    {
        Console.WriteLine($"{PlaneType} reached maximum altitude! Must not exceed {MaximumAltitude} meters of altitude. Current altitude: {CurrentAltitude} meters.");
    }
}
﻿class Passenger : Plane
{
    public int NumberOfPassengers { get; set; }

    public Passenger(int maxSpeed, int maxAltitude) : base(maxSpeed, maxAltitude)
    {
        PlaneType = "Passenger plane";
        SpeedReached += MaxSpeedWarning;
        AltitudeReached += MaxAltitudeWarning;
    }
}
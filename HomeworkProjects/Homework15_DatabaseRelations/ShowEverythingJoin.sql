--SELECT * FROM [User] AS u

SELECT u.FirstName, u.MiddleName, u.LastName, c.Name, a.StreetName, 
		cc.CardNumber, cc.ExpirationDate, cc.CVC, o.[Date], o.[Status], it.Name, it.Price FROM [User] AS u
FULL JOIN [Address] AS a ON u.ID = a.ID
FULL JOIN [City] AS c ON a.CityID = c.ID
FULL JOIN [CreditCard] AS cc ON cc.OwnerID = u.ID
FULL JOIN [Order] AS o ON o.OrdererID = u.ID
FULL JOIN [ItemInOrder] AS i ON i.OrderID = o.ID
FULL JOIN [Item] AS it ON it.ID = i.ItemID
--Just for testing/fun
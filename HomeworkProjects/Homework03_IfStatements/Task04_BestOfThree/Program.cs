﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04_BestOfThree
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the first number: ");
            int a = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the second number: ");
            int b = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the third number: ");
            int c = Convert.ToInt32(Console.ReadLine());

            if (a > b)
            {
                if (a > c)
                {
                    Console.WriteLine(a + " is the greatest number.");
                }
                else
                {
                    Console.WriteLine(c + " is the greatest number.");
                }
            }
            else
            {
                if (b > c)
                {
                    Console.WriteLine(b + " is the greatest number.");
                }
                else
                {
                    Console.WriteLine(c + " is the greatest number.");
                }
            }
        }
    }
}

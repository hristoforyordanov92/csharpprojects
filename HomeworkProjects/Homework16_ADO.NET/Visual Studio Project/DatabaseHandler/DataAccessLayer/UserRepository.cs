﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class UserRepository : ICRUD<User>
{
    public void Create(User user)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_CreateUser", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@FirstName", SqlDbType.VarChar, 20);
            command.Parameters.Add("@MiddleName", SqlDbType.VarChar, 20);
            command.Parameters.Add("@LastName", SqlDbType.VarChar, 20);
            command.Parameters.Add("@BirthDate", SqlDbType.Date);

            command.Parameters["@FirstName"].Value = user.FirstName;
            command.Parameters["@MiddleName"].Value = user.MiddleName;
            command.Parameters["@LastName"].Value = user.LastName;
            command.Parameters["@BirthDate"].Value = user.BirthDate;

            connection.Open();

            Console.WriteLine(command.ExecuteNonQuery() + " rows has been affected.");
        }
    }

    public List<User> ReadAll()
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_ReadAllUsers", connection))
        {
            List<User> listOfUsers = new List<User>();

            command.CommandType = CommandType.StoredProcedure;

            connection.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    User newUser = new User()
                    {
                        ID = int.Parse(reader["ID"].ToString()),
                        FirstName = reader["FirstName"].ToString(),
                        MiddleName = reader["MiddleName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        BirthDate = reader["BirthDate"].ToString()
                    };
                    listOfUsers.Add(newUser);
                }
            }

            return listOfUsers;
        }
    }

    public User ReadById(int id)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_ReadUserById", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@ID", SqlDbType.Int);

            command.Parameters["@ID"].Value = id;

            connection.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();

                User newUser = new User()
                {
                    ID = int.Parse(reader["ID"].ToString()),
                    FirstName = reader["FirstName"].ToString(),
                    MiddleName = reader["MiddleName"].ToString(),
                    LastName = reader["LastName"].ToString(),
                    BirthDate = reader["BirthDate"].ToString()
                };

                return newUser;
            }
        }
    }

    public void Update(User user)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_UpdateUserById", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@ID", SqlDbType.Int);
            command.Parameters.Add("@FirstName", SqlDbType.VarChar, 20);
            command.Parameters.Add("@MiddleName", SqlDbType.VarChar, 20);
            command.Parameters.Add("@LastName", SqlDbType.VarChar, 20);
            command.Parameters.Add("@BirthDate", SqlDbType.Date);

            command.Parameters["@ID"].Value = user.ID;
            command.Parameters["@FirstName"].Value = user.FirstName;
            command.Parameters["@MiddleName"].Value = user.MiddleName;
            command.Parameters["@LastName"].Value = user.LastName;
            command.Parameters["@BirthDate"].Value = user.BirthDate;

            connection.Open();

            Console.WriteLine(command.ExecuteNonQuery() + " rows has been affected.");
        }
    }

    public void Delete(int id)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_DeleteUserById", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@ID", SqlDbType.Int);
            
            command.Parameters["@ID"].Value = id;
            
            connection.Open();

            Console.WriteLine(command.ExecuteNonQuery() + " rows has been affected.");
        }
    }
}
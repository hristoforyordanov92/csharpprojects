﻿using System.Collections.Generic;

interface ICRUD<T>
{
    void Create(T item);
    T ReadById(int id);
    List<T> ReadAll();
    void Update(T item);
    void Delete(int i);
}
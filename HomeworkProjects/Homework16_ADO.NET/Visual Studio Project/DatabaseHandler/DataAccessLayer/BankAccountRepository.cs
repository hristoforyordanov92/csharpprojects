﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class BankAccountRepository : ICRUD<BankAccount>
{
    public void Create(BankAccount bankAccount)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_CreateBankAccount", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@Name", SqlDbType.VarChar, 30);
            command.Parameters.Add("@IBAN", SqlDbType.VarChar, 20);
            command.Parameters.Add("@Amount", SqlDbType.Money);
            command.Parameters.Add("@UserID", SqlDbType.Int);
            command.Parameters.Add("@IsCurrentAccount", SqlDbType.Bit);

            command.Parameters["@Name"].Value = bankAccount.Name;
            command.Parameters["@IBAN"].Value = bankAccount.IBAN;
            command.Parameters["@Amount"].Value = bankAccount.Amount;
            command.Parameters["@UserID"].Value = bankAccount.Owner.ID;
            command.Parameters["@IsCurrentAccount"].Value = bankAccount.IsCurrentAccount;

            connection.Open();

            Console.WriteLine(command.ExecuteNonQuery() + " rows has been affected.");
        }
    }

    public List<BankAccount> ReadAll()
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_ReadAllBankAccounts", connection))
        {
            List<BankAccount> listOfBankAccounts = new List<BankAccount>();
            UserRepository userRepo = new UserRepository();

            command.CommandType = CommandType.StoredProcedure;

            connection.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    BankAccount bankAccount = new BankAccount()
                    {
                        ID = int.Parse(reader["ID"].ToString()),
                        Name = reader["Name"].ToString(),
                        IBAN = reader["IBAN"].ToString(),
                        Amount = decimal.Parse(reader["Amount"].ToString()),
                        Owner = userRepo.ReadById(int.Parse(reader["UserID"].ToString())),
                        IsCurrentAccount = bool.Parse(reader["IsCurrentAccount"].ToString())
                    };
                    listOfBankAccounts.Add(bankAccount);
                }
            }

            return listOfBankAccounts;
        }
    }

    public BankAccount ReadById(int id)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_ReadBankAccountById", connection))
        {
            UserRepository userRepo = new UserRepository();

            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@ID", SqlDbType.Int);

            command.Parameters["@ID"].Value = id;

            connection.Open();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                reader.Read();

                BankAccount newUser = new BankAccount()
                {
                    ID = int.Parse(reader["ID"].ToString()),
                    Name = reader["Name"].ToString(),
                    IBAN = reader["IBAN"].ToString(),
                    Amount = decimal.Parse(reader["Amount"].ToString()),
                    Owner = userRepo.ReadById(int.Parse(reader["UserID"].ToString())),
                    IsCurrentAccount = bool.Parse(reader["IsCurrentAccount"].ToString())
                };

                return newUser;
            }
        }
    }

    public void Update(BankAccount bankAccount)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_UpdateBankAccountById", connection))
        {
            UserRepository userRepo = new UserRepository();

            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@ID", SqlDbType.Int);
            command.Parameters.Add("@Name", SqlDbType.VarChar, 30);
            command.Parameters.Add("@IBAN", SqlDbType.VarChar, 20);
            command.Parameters.Add("@Amount", SqlDbType.Money);
            command.Parameters.Add("@UserID", SqlDbType.Int);
            command.Parameters.Add("@IsCurrentAccount", SqlDbType.Bit);

            command.Parameters["@ID"].Value = bankAccount.ID;
            command.Parameters["@Name"].Value = bankAccount.Name;
            command.Parameters["@IBAN"].Value = bankAccount.IBAN;
            command.Parameters["@Amount"].Value = bankAccount.Amount;
            command.Parameters["@UserID"].Value = bankAccount.Owner.ID;
            command.Parameters["@IsCurrentAccount"].Value = bankAccount.IsCurrentAccount;

            connection.Open();

            Console.WriteLine(command.ExecuteNonQuery() + " rows has been affected.");
        }
    }

    public void Delete(int id)
    {
        using (SqlConnection connection = new SqlConnection(DatabaseHandler.connectionString))
        using (SqlCommand command = new SqlCommand("usp_DeleteBankAccountById", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@ID", SqlDbType.Int);

            command.Parameters["@ID"].Value = id;

            connection.Open();

            Console.WriteLine(command.ExecuteNonQuery() + " rows has been affected.");
        }
    }
}
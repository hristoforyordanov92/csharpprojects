﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class DatabaseHandler
{
    public static string connectionString;

    static void Main(string[] args)
    {
        string server = "";
        string initialCatalog = "";
        bool integratedSecurity = true;

        ChooseServerDialog(ref server);
        ChooseInitialCatalogDialog(ref initialCatalog);

        connectionString = $"Data Source={server};Initial Catalog={initialCatalog};Integrated Security={(integratedSecurity.ToString()).ToLower()}";

        Console.WriteLine("\tConnection String=");
        Console.WriteLine(connectionString);
        Console.WriteLine();

        UserRepository userRepo = new UserRepository();
        BankAccountRepository bankRepo = new BankAccountRepository();

        ShowAllUsers(userRepo);
        Console.WriteLine();

        userRepo.Create(new User("Forko", "Forkov", "Forkovov", @"1992-05-14"));
        Console.WriteLine();
        ShowAllUsers(userRepo);
        Console.WriteLine();

        Console.Write("Edit row ID: ");
        User newUser = userRepo.ReadById(int.Parse(Console.ReadLine()));
        newUser.FirstName = "Hristoforko";

        userRepo.Update(newUser);
        Console.WriteLine();
        ShowAllUsers(userRepo);
        Console.WriteLine();

        Console.Write("Delete user with ID: ");
        userRepo.Delete(int.Parse(Console.ReadLine()));
        Console.WriteLine();
        ShowAllUsers(userRepo);
        Console.WriteLine();

        ShowAllBankAccounts(bankRepo);
        Console.WriteLine();

        Console.Write("Create new bank accout for user with ID: ");
        bankRepo.Create(new BankAccount("NewAcc", "DIUWBQHIDUNWQDWQ", 2000M, true, userRepo.ReadById(int.Parse(Console.ReadLine()))));
        Console.WriteLine();
        ShowAllBankAccounts(bankRepo);
        Console.WriteLine();

        Console.Write("Edit row ID: ");
        BankAccount newAcc = bankRepo.ReadById(int.Parse(Console.ReadLine()));
        newAcc.Name = "NewAccNameChanged";

        bankRepo.Update(newAcc);
        Console.WriteLine();
        ShowAllBankAccounts(bankRepo);
        Console.WriteLine();

        Console.Write("Delete bank account with ID: ");
        bankRepo.Delete(int.Parse(Console.ReadLine()));
        Console.WriteLine();
        ShowAllBankAccounts(bankRepo);
        Console.WriteLine();
    }

    public static void ShowAllUsers(UserRepository userRepo)
    {
        List<User> list = userRepo.ReadAll();

        Console.WriteLine("Users:");
        foreach (var item in list)
        {
            Console.WriteLine(item);
        }
    }

    public static void ShowAllBankAccounts(BankAccountRepository bankRepo)
    {
        List<BankAccount> list = bankRepo.ReadAll();

        Console.WriteLine("Bank accounts:");
        foreach (var item in list)
        {
            Console.WriteLine(item);
        }
    }

    public static void ChooseServerDialog(ref string server)
    {
        Console.Write(
            $"Choose server:\n" +
            $"-Enter \'1\' OR an empty string to choose (local)\n" +
            $"-Enter the name of the server you wish to connect to (.\\ will be included automatically)\n");
        server = Console.ReadLine();
        if (server == "" || server == "1")
        {
            server = "(local)";
        }
        else
        {
            server = @".\" + server;
        }
        Console.WriteLine();
    }

    public static void ChooseInitialCatalogDialog(ref string initialCatalog)
    {
        Console.Write(
            $"Choose database:\n" +
            $"-Enter \'1\' OR an empty string to choose SmallBank (this is a test/default database)\n" +
            $"-Enter the name of the database you wish to access\n");
        initialCatalog = Console.ReadLine();
        if (initialCatalog == "1" || initialCatalog == "")
        {
            initialCatalog = "SmallBank";
        }
        Console.WriteLine();
    }
}
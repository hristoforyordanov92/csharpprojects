﻿public class User
{
    public int ID { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string BirthDate { get; set; }

    public User()
    {

    }

    public User(string firstName, string middleName, string lastName, string birthDate)
    {
        FirstName = firstName;
        MiddleName = middleName;
        LastName = lastName;
        BirthDate = birthDate;
    }

    public override string ToString()
    {
        return $"{ID} {FirstName} {MiddleName} {LastName} {BirthDate}";
    }
}
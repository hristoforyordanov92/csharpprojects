﻿public class BankAccount
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string IBAN { get; set; }
    public decimal Amount { get; set; }
    public bool IsCurrentAccount { get; set; }
    public User Owner { get; set; }

    public BankAccount()
    {

    }

    public BankAccount(string name, string IBAN, decimal amount, bool isCurrentAccount, User owner)
    {
        Name = name;
        this.IBAN = IBAN;
        Amount = amount;
        IsCurrentAccount = isCurrentAccount;
        Owner = owner;
    }

    public override string ToString()
    {
        return $"{ID} {Name} {IBAN} {Amount} {IsCurrentAccount} {Owner.ToString()}";
    }
}
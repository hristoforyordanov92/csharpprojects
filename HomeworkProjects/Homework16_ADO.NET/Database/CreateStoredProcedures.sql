--Create all Stored Procedures

--User table stored procedures
CREATE PROCEDURE usp_CreateUser
	@FirstName AS varchar(20),
	@MiddleName AS varchar(20),
	@LastName AS varchar(20),
	@BirthDate AS date
AS
BEGIN
	INSERT INTO [User]
	VALUES (@FirstName, @MiddleName, @LastName, @BirthDate);
END
GO

CREATE PROCEDURE usp_ReadUserById
	@ID as int
AS
BEGIN
	SELECT * FROM [User]
	WHERE ID = @ID;
END
GO

CREATE PROCEDURE usp_ReadAllUsers
AS
BEGIN
	SELECT * FROM [User]
END
GO

CREATE PROCEDURE usp_UpdateUserById
	@ID int,
	@FirstName AS varchar(20),
	@MiddleName AS varchar(20),
	@LastName AS varchar(20),
	@BirthDate AS date
AS
BEGIN
	UPDATE [User]
	SET FirstName = @FirstName, 
		MiddleName = @MiddleName, 
		LastName = @LastName,
		BirthDate = @BirthDate
	WHERE ID = @ID;
END
GO

CREATE PROCEDURE usp_DeleteUserById
	@ID int
AS
BEGIN
	DELETE FROM [User]
	WHERE ID = @ID;
END
GO

--BankAccount table stored procedures
CREATE PROCEDURE usp_CreateBankAccount
	@Name varchar(30),
	@IBAN varchar(20),
	@Amount money,
	@UserID int,
	@IsCurrentAccount bit
AS
BEGIN
	INSERT INTO [BankAccount]
	VALUES (@Name, @IBAN, @Amount, @UserID, @IsCurrentAccount);
END
GO

CREATE PROCEDURE usp_ReadBankAccountById
	@ID int
AS
BEGIN
	SELECT * FROM [BankAccount]
	WHERE ID = @ID;
END
GO

CREATE PROCEDURE usp_ReadAllBankAccounts
AS
BEGIN
	SELECT * FROM [BankAccount]
END
GO

CREATE PROCEDURE usp_UpdateBankAccountById
	@ID int,
	@Name varchar(30),
	@IBAN varchar(20),
	@Amount money,
	@UserID int,
	@IsCurrentAccount bit
AS
BEGIN
	UPDATE [BankAccount]
	SET Name = @Name,
		IBAN = @IBAN,
		Amount = @Amount,
		UserID = @UserID,
		IsCurrentAccount = @IsCurrentAccount
	WHERE ID = @ID;
END
GO

CREATE PROCEDURE usp_DeleteBankAccountById
	@ID int
AS
BEGIN
	DELETE FROM [BankAccount]
	WHERE ID = @ID;
END
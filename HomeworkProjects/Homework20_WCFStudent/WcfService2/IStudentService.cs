﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService2
{
    [ServiceContract]
    public interface IStudentService
    {
        [OperationContract]
        void CreateStudent(DataContractStudent student);

        [OperationContract]
        DataContractStudent ReadStudent(int id);

        [OperationContract]
        List<DataContractStudent> ReadAllStudents();

        [OperationContract]
        void UpdateStudent(int id, DataContractStudent student);

        [OperationContract]
        void DeleteStudent(int id);
    }

    [DataContract]
    public class DataContractStudent
    {
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime BirthDate { get; set; }
        [DataMember]
        public string Faculty { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfService2
{
    class AnotherMapper
    {
        public static DataContractStudent ToDataContract(BusinessObjects.Student student)
        {
            DataContractStudent newStudent = new DataContractStudent()
            {
                FirstName = student.FirstName,
                LastName = student.LastName,
                BirthDate = student.BirthDate,
                Faculty = student.Faculty
            };

            return newStudent;
        }

        public static BusinessObjects.Student ToBusiness(DataContractStudent student)
        {
            BusinessObjects.Student newStudent = new BusinessObjects.Student()
            {
                FirstName = student.FirstName,
                LastName = student.LastName,
                BirthDate = student.BirthDate,
                Faculty = student.Faculty
            };

            return newStudent;
        }
    }
}
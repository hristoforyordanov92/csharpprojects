﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using StudentsDataAccessLayer;

namespace WcfService2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service2 : IStudentService
    {
        DataAccessLayer dal = new DataAccessLayer();

        public void CreateStudent(DataContractStudent student)
        {
            dal.Create(AnotherMapper.ToBusiness(student));
        }
        
        public DataContractStudent ReadStudent(int id)
        {
            return AnotherMapper.ToDataContract(dal.Read(id));
        }

        public List<DataContractStudent> ReadAllStudents()
        {
            var contractStudents = new List<DataContractStudent>();
            var databaseStudents = dal.ReadAll();

            foreach (var student in databaseStudents)
            {
                contractStudents.Add(AnotherMapper.ToDataContract(student));
            }

            return contractStudents;
        }

        public void UpdateStudent(int id, DataContractStudent student)
        {
            dal.Update(id, AnotherMapper.ToBusiness(student));
        }

        public void DeleteStudent(int id)
        {
            dal.Delete(id);
        }
    }
}

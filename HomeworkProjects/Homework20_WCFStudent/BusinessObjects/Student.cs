﻿using System;

namespace BusinessObjects
{
    public class Student
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Faculty { get; set; }
    }
}
﻿<%@ Page Title="Create" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="StudentWebApp.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <table class="nav-justified" style="height: 93px">
        <tr>
            <td style="width: 123px">
                <asp:Label ID="lblFirstName" runat="server" Text="First name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLastName" runat="server" Text="Last name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblBirthDate" runat="server" Text="Birth date:"></asp:Label>
            </td>
            <td>
                <asp:Calendar ID="birthDateCalendar" runat="server"></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblFaculty" runat="server" Text="Faculty: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtFaculty" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnSubmit" runat="server" OnClick="NewObject" Text="Submit"/>
            </td>
        </tr>
    </table>
    
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASPStudents.ServiceReference1;

namespace StudentWebApp
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void NewObject(object sender, EventArgs e)
        {
            StudentServiceClient client = new StudentServiceClient();

            client.CreateStudent(
                new DataContractStudent()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    BirthDate = DateTime.Now,
                    Faculty = txtFaculty.Text
                }
                );
        }
    }
}
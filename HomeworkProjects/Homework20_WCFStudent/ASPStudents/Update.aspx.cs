﻿using ASPStudents.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPStudents
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UpdateObject(object sender, EventArgs e)
        {
            StudentServiceClient client = new StudentServiceClient();

            client.UpdateStudent(
                int.Parse(txtID.Text),
                new DataContractStudent()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    BirthDate = DateTime.Now,
                    Faculty = txtFaculty.Text
                }
                );
        }
    }
}
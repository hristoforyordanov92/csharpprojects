﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASPStudents.ServiceReference1;

namespace ASPStudents
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Delete(object sender, EventArgs e)
        {
            StudentServiceClient client = new StudentServiceClient();

            client.DeleteStudent(int.Parse(txtID.Text));
        }
    }
}
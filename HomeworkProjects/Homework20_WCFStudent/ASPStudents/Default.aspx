﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ASPStudents._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="FirstName"  HeaderText="First Name" />
            <asp:BoundField DataField="LastName"  HeaderText="Last Name" />
            <asp:BoundField DataField="BirthDate"  HeaderText="Birth Date" />
            <asp:BoundField DataField="Faculty"  HeaderText="Faculty" />
        </Columns>
    </asp:GridView>

   

</asp:Content>

﻿<%@ Page Title="Delete" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="ASPStudents.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <table class="nav-justified" style="height: 93px">
        <tr>
            <td style="width: 123px">
                <asp:Label ID="lblID" runat="server" Text="ID:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" OnClick="Delete" Text="Submit" />
            </td>
        </tr>
    </table>

</asp:Content>

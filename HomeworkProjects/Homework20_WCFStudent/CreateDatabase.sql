CREATE DATABASE [StudentsHW20];
GO

CREATE TABLE [Students](
	[ID] int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[FirstName] varchar(20) NOT NULL,
	[LastName] varchar(20) NOT NULL,
	[BirthDate] date NOT NULL,
	[Faculty] varchar(20) NOT NULL
);
GO

INSERT INTO [Students] VALUES ('Hristofor', 'Yordanov', '1992-05-14', 'Programming');
INSERT INTO [Students] VALUES ('Ivan', 'Ivanov', '1995-02-11', 'Mathematics');
INSERT INTO [Students] VALUES ('Petar', 'Petrov', '1982-05-24', 'English');
INSERT INTO [Students] VALUES ('Stoyan', 'Stoyanov', '1991-09-12', 'Informatics');
INSERT INTO [Students] VALUES ('Iva', 'Ivova', '1992-11-14', 'Programming');
INSERT INTO [Students] VALUES ('Maria', 'Marinova', '1972-12-12', 'Singing');
INSERT INTO [Students] VALUES ('Stela', 'Stakova', '1990-01-01', 'Mathematics');
INSERT INTO [Students] VALUES ('Trichka', 'Trikova', '1988-07-02', 'Painting');
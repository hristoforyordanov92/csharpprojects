﻿namespace StudentsDataAccessLayer
{
    class ObjectMapper
    {
        public static StudentEntity.Student ToEntityStudent(BusinessObjects.Student businessStudent)
        {
            StudentEntity.Student entityStudent = new StudentEntity.Student()
            {
                ID = businessStudent.ID,
                FirstName = businessStudent.FirstName,
                LastName = businessStudent.LastName,
                BirthDate = businessStudent.BirthDate,
                Faculty = businessStudent.Faculty
            };

            return entityStudent;
        }

        public static BusinessObjects.Student ToBusinessStudent(StudentEntity.Student entityStudent)
        {
            BusinessObjects.Student businessStudent = new BusinessObjects.Student()
            {
                ID = entityStudent.ID,
                FirstName = entityStudent.FirstName,
                LastName = entityStudent.LastName,
                BirthDate = entityStudent.BirthDate,
                Faculty = entityStudent.Faculty
            };

            return businessStudent;
        }
    }
}
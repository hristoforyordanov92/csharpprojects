﻿using System.Collections.Generic;
using System.Linq;
using StudentEntity;

namespace StudentsDataAccessLayer
{
    public class DataAccessLayer : IDataAccessLayer<BusinessObjects.Student>
    {
        private StudentsHW20Entities entity = new StudentsHW20Entities();

        public void Create(BusinessObjects.Student item)
        {
            entity.Students.Add(ObjectMapper.ToEntityStudent(item));
            entity.SaveChanges();
        }

        public List<BusinessObjects.Student> ReadAll()
        {
            var entityList = entity.Students.ToList();
            var remappedList = new List<BusinessObjects.Student>();

            foreach (var student in entityList)
            {
                remappedList.Add(ObjectMapper.ToBusinessStudent(student));
            }

            return remappedList;
        }

        public BusinessObjects.Student Read(int id)
        {
            var entityStudent = entity.Students.FirstOrDefault(x => x.ID == id);
            var remappedStudent = ObjectMapper.ToBusinessStudent(entityStudent);

            return remappedStudent;
        }

        public void Update(int id, BusinessObjects.Student item)
        {
            var entityStudent = entity.Students.FirstOrDefault(x => x.ID == id);
            if (entityStudent != null)
            {
                var remappedBusinessStudent = ObjectMapper.ToEntityStudent(item);

                entityStudent.FirstName = remappedBusinessStudent.FirstName;
                entityStudent.LastName = remappedBusinessStudent.LastName;
                entityStudent.BirthDate = remappedBusinessStudent.BirthDate;
                entityStudent.Faculty = remappedBusinessStudent.Faculty;
                entity.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var student = entity.Students.FirstOrDefault(x => x.ID == id);
            entity.Students.Remove(student);
            entity.SaveChanges();
        }
    }
}
﻿using System.Collections.Generic;

namespace StudentsDataAccessLayer
{
    interface IDataAccessLayer<T>
    {
        void Create(T item);
        List<T> ReadAll();
        T Read(int id);
        void Update(int id, T item);
        void Delete(int id);
    }
}
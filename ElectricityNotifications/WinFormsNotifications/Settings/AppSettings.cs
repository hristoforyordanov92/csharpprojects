﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WinFormsNotifications
{
    public static class AppSettings
    {
        public static SettingsSerializable settings = new SettingsSerializable();

        public const string BASE_URL = "http://www.energo-pro-grid.bg/";

        public const int MINIMUM_INTERVAL = 10;
        public const int MAXIMUM_INTERVAL = 1000;
        public const int DEFAULT_INTERVAL = 30;

        public const int MINIMUM_DAYS = 0;
        public const int MAXIMUM_DAYS = 100;
        public const int DEFAULT_DAYS = 3;

        public static List<LinkItem> linkItemsList;

        static AppSettings()
        {
            CreateLinkItemsList();
            LoadSettings();
        }

public static void LoadSettings()
        {
            if (File.Exists("settings.dat"))
            {
                settings = DeserializeFromJSON<SettingsSerializable>("settings.dat");
            }
            else
            {
                settings = new SettingsSerializable();
            }
        }

        public static void SaveSettings()
        {
            SerializeToJSON("settings.dat", settings);
        }

        public static void CreateLinkItemsList()
        {
            linkItemsList = new List<LinkItem>
            {
                new LinkItem("Габрово", "bg/Oblast-Gabrovo"),
                new LinkItem("Велико Търново", "bg/Oblast-Veliko-Tarnovo"),
                new LinkItem("Русе", "bg/Oblast-Ruse"),
                new LinkItem("Разград", "bg/Oblast-Razgrad"),
                new LinkItem("Търговище", "bg/Oblast-Targovishte"),
                new LinkItem("Шумен", "bg/Oblast-Shumen"),
                new LinkItem("Силистра", "bg/Oblast-Silistra"),
                new LinkItem("Добрич", "bg/Oblast-Dobrich"),
                new LinkItem("Варна", "bg/ROC-Varna")
            };
        }
        private static void SerializeToJSON(string fullFilePath, object objectToSerialize)
        {
            var serializedData = JsonConvert.SerializeObject(objectToSerialize, Formatting.Indented);
            File.WriteAllText(fullFilePath, serializedData);
        }
        private static T DeserializeFromJSON<T>(string fullFilePath)
        {
            var reader = File.ReadAllText(fullFilePath);
            T returnedObject = (T)JsonConvert.DeserializeObject(reader, typeof(T));

            return returnedObject;
        }

    }
}

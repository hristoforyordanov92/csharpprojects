﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsNotifications
{
    public class SettingsSerializable
    {
        public int currentInterval = AppSettings.DEFAULT_INTERVAL;
        public int currentDays = AppSettings.DEFAULT_DAYS;
        public int currentSelectedArea = 0;
        public string[] keywords;
    }
}

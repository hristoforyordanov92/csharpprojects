﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsNotifications
{
    public class DateItem
    {
        public DateTime fromDate;
        public DateTime toDate;
        public List<DateTime> fromTime;
        public List<DateTime> toTime;
        public string text;

        public DateItem()
        {
            //fromDate = new List<DateTime>();
            //toDate = new List<DateTime>();
            fromTime = new List<DateTime>();
            toTime = new List<DateTime>();
        }

        public override string ToString()
        {
            return text;
        }
    }
}

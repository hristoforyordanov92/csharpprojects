﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsNotifications
{
    public class LinkItem
    {
        public string text;
        public string url;

        public LinkItem(string text, string url)
        {
            this.text = text;
            this.url = url;
        }

        public override string ToString()
        {
            return text;
        }
    }
}
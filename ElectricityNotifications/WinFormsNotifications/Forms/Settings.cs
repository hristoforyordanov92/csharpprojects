﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsNotifications
{
    public partial class Settings : Form
    {
        Form mainForm;



        public Settings(Form mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            LoadSettings();

        }

        private void LoadSettings()
        {
            textBoxInterval.Text = AppSettings.settings.currentInterval.ToString();
            textBoxDays.Text = AppSettings.settings.currentDays.ToString();
        }

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }


        private void SaveSettings()
        {
            if (!ChangeInterval())
            {
                return;
            }
            if (!ChangeDays())
            {
                return;
            }

            AppSettings.SaveSettings();
            Close();
        }

        private bool ChangeDays()
        {
            int parsedDays = 0;

            if (!int.TryParse(textBoxDays.Text, out parsedDays))
            {
                MessageBox.Show($"Въведеният брой дни е невалиден.",
                    "Невалиден брой дни!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AppSettings.settings.currentDays = AppSettings.DEFAULT_DAYS;
                return false;
            }
            else if (parsedDays < AppSettings.MINIMUM_DAYS)
            {
                MessageBox.Show($"Дните не могат да са по-малко от {AppSettings.MINIMUM_DAYS} дена.",
                    "Грешка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AppSettings.settings.currentDays = AppSettings.MINIMUM_DAYS;
                return false;
            }
            else if (parsedDays > AppSettings.MAXIMUM_DAYS)
            {
                MessageBox.Show($"Дните не могат да са повече от {AppSettings.MAXIMUM_DAYS} дена.",
                    "Грешка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AppSettings.settings.currentDays = AppSettings.MAXIMUM_DAYS;
                return false;
            }
            else
            {
                AppSettings.settings.currentDays = parsedDays;
            }

            textBoxDays.Text = AppSettings.settings.currentDays.ToString();
            return true;
        }

        private bool ChangeInterval()
        {
            int parsedInterval = 0;

            if (!int.TryParse(textBoxInterval.Text, out parsedInterval))
            {
                MessageBox.Show($"Въведеният интервал е невалиден.",
                    "Невалиден интервал!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AppSettings.settings.currentInterval = AppSettings.DEFAULT_INTERVAL;
                return false;
            }
            else if (parsedInterval < AppSettings.MINIMUM_INTERVAL)
            {
                MessageBox.Show($"Интервалът между отделните проверки не може да бъде по-кратък от {AppSettings.MINIMUM_INTERVAL} минути.",
                    "Грешка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AppSettings.settings.currentInterval = AppSettings.MINIMUM_INTERVAL;
                return false;
            }
            else if (parsedInterval > AppSettings.MAXIMUM_INTERVAL)
            {
                MessageBox.Show($"Интервалът между отделните проверки не може да бъде по-дълъг от {AppSettings.MAXIMUM_INTERVAL} минути.",
                    "Грешка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AppSettings.settings.currentInterval = AppSettings.MAXIMUM_INTERVAL;
                return false;
            }
            else
            {
                AppSettings.settings.currentInterval = parsedInterval;
            }

            textBoxInterval.Text = AppSettings.settings.currentInterval.ToString();
            return true;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsNotifications
{
    public partial class MainForm : Form
    {
        public const string BASE_URL = "http://www.energo-pro-grid.bg/";

        public List<DateItem> dateItemsList;

        public DateTime nextCheck = DateTime.Now;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AddLinkItems();

            LoadSettings();
            SetupNotificationIcon();



            timer1.Enabled = true;
        }

        private void LoadSettings()
        {
            Icon = Properties.Resources.greenbolt1;
            comboBoxArea.SelectedIndex = AppSettings.settings.currentSelectedArea;
            textBoxKeywords.Lines = AppSettings.settings.keywords;
        }

        private void AddLinkItems()
        {
            comboBoxArea.Items.Clear();
            foreach (var linkItem in AppSettings.linkItemsList)
            {
                comboBoxArea.Items.Add(linkItem);
            }
            if (comboBoxArea.Items.Count > 0)
            {
                comboBoxArea.SelectedIndex = 0;
            }
        }



        private void SetupNotificationIcon()
        {
            SetNotifyIconPositive();
        }

        private void ShowForm(object sender, EventArgs e)
        {
            this.Show();
        }

        private void SetNotifyIconPositive()
        {
            notifyIcon.Text = "Няма планирани прекъсвания на електрозахранването.";
            notifyIcon.Icon = Properties.Resources.greenbolt1;
            Icon = Properties.Resources.greenbolt1;
        }

        private void SetNotifyIconProblematic()
        {
            notifyIcon.Text = "Очакват се прекъсвания на електрозахранването.";
            notifyIcon.Icon = Properties.Resources.redbolt1;
            Icon = Properties.Resources.redbolt1;
        }

        private string GetHTML(string url)
        {
            using (var client = new WebClient())
            using (var stream = client.OpenRead(url))
            using (var textReader = new StreamReader(stream, Encoding.GetEncoding("windows-1251"), true))
            {
                return textReader.ReadToEnd();
            }
        }

        private void buttonGetHTML_Click(object sender, EventArgs e)
        {
            PerformActions();
            AppSettings.settings.currentSelectedArea = comboBoxArea.SelectedIndex;
            AppSettings.SaveSettings();
        }

        private void PerformActions()
        {
            GrabInformation();
            PerformCheck();
            nextCheck = nextCheck.AddMinutes(AppSettings.settings.currentInterval);
            if (richTextBox1.Text != "")
            {
                SetNotifyIconProblematic();
            }
            else
            {
                SetNotifyIconPositive();
            }
        }

        private void GrabInformation()
        {
            string newUrl = AppSettings.BASE_URL +
            ((LinkItem)comboBoxArea.SelectedItem).url;

            string fullHTMLstring = GetHTML(newUrl);
            string startingString = "<div id=\"div_tab_2\"";
            int indexOfStartingString = fullHTMLstring.IndexOf(startingString);
            fullHTMLstring = fullHTMLstring.Substring(indexOfStartingString);
            HtmlToDateItems(fullHTMLstring);
            richTextBox1.Text = "";
        }

        private void HtmlToDateItems(string html)
        {
            dateItemsList = new List<DateItem>();
            int indexOfParagraphStart = html.IndexOf("<p>");
            while (indexOfParagraphStart >= 0)
            {
                int indexOfParagraphEnd = html.IndexOf("</p>");

                DateItem newDateItem = new DateItem();

                GetDates(html, ref newDateItem);
                GetTimes(html, ref newDateItem);

                string dateItemText = html.Substring(indexOfParagraphStart + 3,
                   indexOfParagraphEnd - indexOfParagraphStart - 3);
                RemoveHtmlTags(ref dateItemText);
                newDateItem.text = dateItemText;

                dateItemsList.Add(newDateItem);

                html = html.Substring(indexOfParagraphEnd + 4);
                indexOfParagraphStart = html.IndexOf("<p>");
            }
        }

        private void GetDates(string input, ref DateItem dateItem)
        {
            string newString = input;
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();
            int indexOfYear;
            string date;

            indexOfYear = newString.IndexOf(" г. ");
            date = newString.Substring(indexOfYear - 10, indexOfYear - (indexOfYear - 10));
            fromDate = Convert.ToDateTime(date);

            newString = newString.Substring(indexOfYear + 3);
            indexOfYear = newString.IndexOf(" г. ");
            if (indexOfYear != -1)
            {
                date = newString.Substring(indexOfYear - 10, indexOfYear - (indexOfYear - 10));
                toDate = Convert.ToDateTime(date);
            }
            else
            {
                toDate = fromDate;
            }


            dateItem.fromDate = fromDate;
            dateItem.toDate = toDate;
        }

        private void GetTimes(string input, ref DateItem dateItem)
        {
            int indexOfStrong = input.IndexOf("<strong>");
            int indexOfLastString = input.IndexOf("</strong>");
            string newString = input.Substring(indexOfStrong, indexOfLastString - indexOfStrong);

            int indexOfFromTime = newString.IndexOf(" от ");
            int indexOfToTime = newString.IndexOf(" до ");

            DateTime fromTime;
            DateTime toTime;

            while (indexOfFromTime != -1)
            {
                string fromTimeString = newString.Substring(indexOfFromTime + 4, 6);
                fromTimeString = fromTimeString.Replace(" ", "");

                string toTimeString = newString.Substring(indexOfToTime + 4, 6);
                toTimeString = toTimeString.Replace(" ", "");

                fromTime = Convert.ToDateTime(fromTimeString);
                toTime = Convert.ToDateTime(toTimeString);

                dateItem.fromTime.Add(fromTime);
                dateItem.toTime.Add(toTime);

                newString = newString.Substring(indexOfToTime + 8);

                indexOfFromTime = newString.IndexOf(" от ");
                indexOfToTime = newString.IndexOf(" до ");
            }
        }

        private void RemoveHtmlTags(ref string input)
        {
            input = input.Replace("<strong>", "");
            input = input.Replace("\r\n", "");
            input = input.Replace("</strong>", "");
            input = input.Replace("<p>", "");
            input = input.Replace("</p>", "");
            input = input.Replace("&#34;", "\"");
            input = input.TrimEnd(' ');
        }

        private void buttonHideForm_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (nextCheck <= DateTime.Now)
            {
                PerformActions();
            }
        }

        private void PerformCheck()
        {

            foreach (var dateItem in dateItemsList)
            {
                if (textBoxKeywords.Text != "")
                {
                    bool found = false;
                    foreach (var line in textBoxKeywords.Lines)
                    {
                        if (dateItem.text.IndexOf(line) >= 0)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        continue;
                    }
                }
                int dateCalc = dateItem.fromDate.DayOfYear - DateTime.Now.DayOfYear;
                if ((dateCalc <= AppSettings.settings.currentDays && dateCalc >= 1) ||
                    (DateTime.Now.DayOfYear >= dateItem.fromDate.DayOfYear && DateTime.Now.DayOfYear < dateItem.toDate.DayOfYear))
                {
                    richTextBox1.Text += dateItem.text + "\n\n";

                }
                else if (DateTime.Now.DayOfYear == dateItem.toDate.DayOfYear)
                {
                    foreach (var time in dateItem.toTime)
                    {
                        if (DateTime.Now.TimeOfDay < time.TimeOfDay)
                        {
                            richTextBox1.Text += dateItem.text + "\n\n";
                            break;
                        }
                    }
                }
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var settingsForm = new Settings(this);
            settingsForm.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var aboutForm = new About();
            aboutForm.ShowDialog();
        }

        private void buttonSaveKeywords_Click(object sender, EventArgs e)
        {
            AppSettings.settings.keywords = textBoxKeywords.Lines;
            AppSettings.SaveSettings();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxKeywords.Lines = AppSettings.settings.keywords;
        }
    }
}
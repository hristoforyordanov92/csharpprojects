﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsNotifications
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
            richTextBox1.SelectionFont = new Font("Microsoft Sans Serif", 11.25f);

            richTextBox1.SelectionColor = Color.Black;

            richTextBox1.SelectedText = "Програмата е създадена от:\n";

            richTextBox1.SelectionFont = new Font("Microsoft Sans Serif", 14f, FontStyle.Bold);

            richTextBox1.SelectionColor = Color.Blue;

            richTextBox1.SelectedText = "Fork0 \n\n";

            richTextBox1.SelectionFont = new Font("Microsoft Sans Serif", 11.25f);

            richTextBox1.SelectionColor = Color.Black;

            richTextBox1.SelectedText = "За контакти при проблеми:\n";

            richTextBox1.SelectionFont = new Font("Microsoft Sans Serif", 14f, FontStyle.Bold);

            richTextBox1.SelectionColor = Color.Blue;

            richTextBox1.SelectedText = "hristofor.yordanov.92@gmail.com";
        }
    }
}

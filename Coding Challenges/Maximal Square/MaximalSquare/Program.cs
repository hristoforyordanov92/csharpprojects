﻿using System;

class Program
{
    static void Main(string[] args)
    {
        /*
        Have the function MaximalSquare(strArr) take the strArr parameter being passed which will be a 2D matrix of 0 and 1's,
        and determine the area of the largest square submatrix that contains all 1's.
        A square submatrix is one of equal width and height, and your program should return the area of the largest submatrix that contains only 1's.
        For example: 
        if strArr is ["11100", "11011", "01101", "10010"] then this looks like the following matrix:

        1 1 1 0 0
        1 1 0 1 1
        0 1 1 0 1
        1 0 0 1 0

        For the input above, you can see the 1's in the top left corner create the largest square submatrix of size 2x2,
        so your program should return the area which is 4.
        You can assume the input will not be empty.

        Example inputs:
        1.For the input("10100", "10111", "11111", "10010") the correct answer is 4.
        2.For the input("0111", "1111", "1111", "1111") the correct answer is 9.
        3.For the input("0111", "1101", "0111") the correct answer is 1.
        4.For the input("1111", "1111") the correct answer is 4.
        5.For the input("1111", "1111", "1111") the correct answer is 9.
        6.For the input("1111", "1101", "1111", "0111") the correct answer is 4.
        7.For the input("01001", "11111", "01011", "11011") the correct answer is 4.
        8.For the input("01001", "11111", "01011", "11111", "01111", "11111") the correct answer is 9.
        9.For the input("101101", "111111", "010111", "111111") the correct answer is 9.
        10.For the input("101101", "111111", "011111", "111111", "001111", "011111") the correct answer is 16.
        */

        Console.WriteLine(MaximalSquare("10100", "10111", "11111", "10010"));
        Console.WriteLine(MaximalSquare("0111", "1111", "1111", "1111"));
        Console.WriteLine(MaximalSquare("0111", "1101", "0111"));
        Console.WriteLine(MaximalSquare("1111", "1111"));
        Console.WriteLine(MaximalSquare("1111", "1111", "1111"));
        Console.WriteLine(MaximalSquare("1111", "1101", "1111", "0111"));
        Console.WriteLine(MaximalSquare("01001", "11111", "01011", "11011"));
        Console.WriteLine(MaximalSquare("01001", "11111", "01011", "11111", "01111", "11111"));
        Console.WriteLine(MaximalSquare("101101", "111111", "010111", "111111"));
        Console.WriteLine(MaximalSquare("101101", "111111", "011111", "111111", "001111", "011111"));
    }

    public static int MaximalSquare(params string[] stringArrayInput)
    {
        int maximumSideLength = 0;
        for (int currentRowIndex = 0; currentRowIndex < stringArrayInput.Length; currentRowIndex++)
        {
            string currentRow = stringArrayInput[currentRowIndex];
            int nextIndexToCheck = 0;
            int firstSubstringIndex = 0;
            int lastSubstringIndex = 0;
            while (nextIndexToCheck < currentRow.Length)
            {
                nextIndexToCheck = firstSubstringIndex = currentRow.IndexOf('1', nextIndexToCheck);
                if (firstSubstringIndex == -1)
                {
                    break;
                }

                lastSubstringIndex = currentRow.IndexOf('0', firstSubstringIndex);
                if (lastSubstringIndex == -1)
                {
                    lastSubstringIndex = currentRow.Length;
                }

                int rowsToCheckDownwards = lastSubstringIndex - firstSubstringIndex;
                string currentRowSubstring = currentRow.Substring(firstSubstringIndex, lastSubstringIndex - firstSubstringIndex);

                int currentSideLength = 0;

                for (int x = 0; x < rowsToCheckDownwards; x++)
                {
                    string nextRow;
                    try
                    {
                        nextRow = stringArrayInput[currentRowIndex + x].Substring(firstSubstringIndex, lastSubstringIndex - firstSubstringIndex);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        break;
                    }

                    if (currentRowSubstring != nextRow)
                    {
                        break;
                    }
                    currentSideLength++;
                }

                if (maximumSideLength < currentSideLength)
                {
                    maximumSideLength = currentSideLength;
                }
                nextIndexToCheck++;
            }
        }

        return maximumSideLength * maximumSideLength;
    }
}